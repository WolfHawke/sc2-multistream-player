<?php
/**
 * SolaShout Player Documentation
 * Localization file=> Turkish
 */

$LANGUAGE = array(
    'languageCode' => 'tr',
    'languageName' => 'Türkçe',
    'languageIsoName' => 'Turkish',
    'languageDirection' => 'ltr',
    'updated' => '2020-10-07',
    'title' => 'SolaShout Player Belgeleme',
    'nav_player' => 'Player',
    'nav_contents' => 'İçindekiler',
    'nav_language' => 'Dil',
    'copyright' => 'Telif Hakları',
    'rights' => 'Tüm haklar saklıdır.',
    'license' => 'Kullanım koşulları için [lisansı] görüntüleyiniz.',
    'current_ver' => 'Mevcut Sürüm',
    'doc_update' => 'Belgeleme Güncellemesi: rev.',
    'body' => '#Tanım
SolaShout Player SHOUTCast v2 streamleri için web tabanlı bir ses çalar yazılımıdır. Hafif ve tek başına ek bileşendir. Ön yüzü HTML, CSS ve JavaScript ile yazılmıştır. Arka yüz ise PHP-tabanlıdır. Her bir SolaShout oluşumu 1 ile 10 adet SHOUTCast v2 yayını sunabilir. Arabirim mobil kullanımı ön planda tutup basit olmak üzere tasarlanmıştır. Tek başına duran bir ek bileşim olarak tasarlanan bu yazılım, ayarlarını bir PHP dosyasında saklar. Veri tabanı bağlantısına gerek yoktur.

***Lütfen dikkat!** SolaShout Player özellikle SHOUTCast v2 yayınlarını sunmak üzere tasarlandı. Ne Icecast ne de SHOUTCast v1 yayınlarını sunabilir.*

#Sistem Gereksinimleri
- PHP 7.2 veya daha yenisi
- `~/player/config` dizini için yazma izinleri
- Yayımlanacak yayınlar için URL ve port bilgileri
- Yerelleştirme için `~/player/localization`, `~/player/docs/localization` ve `~/player/streamconfig/localization` dizinleri için yazma izinleri

#Kurulum
1. `player` dizinini web sunucunuza yükleyin.
2. Sunucunuzda `~/player/config`, `~/player/localization`, `~/player/docs/localization` ve`~/player/streamconfig/localization` altdizinlerinin yazma izinlerine sahip olduğunuzu kontrol edin.
3. Sunucunuzun `http://sunucum.com/player` konumunu web tarayıcınızda açın.
4. Kurulum sihirbazını kullanarak yayınlarınızı ayarlayın.
5. Yayınlarınızı dinleyın.

#Ayarlar
##Kurulum Sihirbazını Kullanarak Ayar Yapmak
SolaShout Player kurulum sihirbazı aracılığıyla ayarlanabilir. İlk kurulumu yaparken, bu etkileşimli yöntem kullanabileceğiniz tek yöntemdir. İlk kurulumu bitirdikten sonra, ayar ara yüzünü (bkz. aşağıda) kullanarak ayarlarınızı değiştirebilirsiniz.

Kurulum sihirbazının kullanımına detaylı anlatım gerekmez, çünkü her adımda ne yapıldığına dair detaylı bilgi içerir. Ancak dikkatinizi iki konuya çekmek istiyoruz:
1. Eğer SolaShout Player’ı tek bir sunucudan yayımlanan birkaç tane yayını sunmak üzere ayarlıyorsanız, kurulum sihirbazı otomatik olarak sunucudaki mevcut yayınların bir listesini çıkarır. SHOUTCast konfigürasyon dosyasından çıkan yayın adlarını görüntüleyecektir. Bunları değiştirmek isterseniz, bunu mevcut isimlere tıklayarak yapabilirsiniz. Sadece mevcut yayını olan streamler otomatik olarak eklenecektir. Ancak yayın dışı olan streamleri ekleme imkânı da size tanınacaktır.
2. Ayar arabirimi etkisiz hale getirilebilir. Buna ayrıca &ldquo;ara yüzü kilitlemek&rdquo; olarak hitap ederiz (bkz. Güvenlik). Ayar arabirimini kilitlerseniz, e-Posta adresini tanımlamayı önemle öneririz. Bir e-Posta adresi tanımlı olmazsa, ayarlar arabirimini etkili hale getirdiğinizde, kurulum sihirbazı yeniden çalışır ve ayarlarınızın hepsi uçar. Kurulum sihirbazında yazıldığı gibi, SolaShout Player&rsquo;a girdiğiniz e-Posta adresi hiçbir yere iletilmez ve sadece SolaShout Player&rsquıno; yüklü olduğu sunucuda saklanır. Kamuoyuna yönelik sayfaların hiçbiri e-Posta adresine erişemez. Ancak ayar arabirimi e-Posta adresi görebilir, ama tarayıcıya gönderilen HTML ve JavaScript kodunda bulunmaz. e-Posta adresin nasıl kullanıldığını öğrenmek için yazılımı incelemenizi öneririm.accordion

##Ayarlar Arabirimi Kullanarak Ayar Yapmak 
SolaShout Player `~/player/streamconfig` adresinden ayar yapmak için kullanabileceğiniz basit bir arabirim sunuar. Opsiyonların çoğunun ne işe yaradığını açıklayan bildirimler mouseu ? işareti üzerine getirildiğinde belirlenir. 

Ayarlar arabirimine erişimi bu sayfadan da kapatılabilir.

Ayarları kaydetmek için ya dipteki yeşil düğmeyi tıklayın ya da menüdeki Ayarlari kaydet öğesini tıklayın.

Ayarları kaydederken parolayı değiştirmek istemezseniz, parola alanlarını boş bırakınız.

##Ayarları settings.php Sayfasını Kullanarak Düzenlemek
Her detayı en incesine yönetmek isteyenler, `~/player/config/settings.php` dosyasını düzenleyerek ayarlarını yapabilir. Bu dosyada ayarları içieren bir PHP dizini bulunur. Dizindeki öğelerin hepsi gereklidir. Öğelerin ne anlama geldiğini öğrenmek ve bir örnek görmek için `~/player/config/example-settings.php` dosyasına bakınız.

`~/player/streamconfig` altdizinini silerseniz, SolaShout Player&rsquo;ın ayarını yapabilmek için kalan tek yöntem budur.

Parola karmasını oluşturmak için `~/player/` altdizninde komut isteminden `php genpwdhash.php [parola]` komutunu çalıştırn. 

##SolaShout Player&rsquo;ı Progressive Web App Olarak Sunmak
** İleride gelecek olan bir özellik. Daha çalışır durumda değil! **

SolaShout Player&rsquo;ı HTTPS ile güvenli kılınmış bir sunucudan sunarsanız, yazılımı kullanıcılarınıza bir Progressive Web App (PWA) olarak sunma özelliği olur. Bir PWA Chromium-tabanlı tarayaıclıarın veya mobil platforlarda işletim sistemine has bir uygulama olarak kurulabilir. Böylece dinleyicileriniz internet yayınlarınızı daha kolayca bulabilecekler. PWA özelliği etkin kılınırsa, SolaShout Player oluşumunuza özel bir isim tanımlamanızı öneririz. Böylece sizin oluşumunuz diğer oluşumlardan farklı olur. 

#Tema Özelleştirme
SolaShout Player beş adet tema ile gelir. SolaShout Player oluşumunuzu özelleştirmek için, mevcut temaların birisini kullanarak görünümü değiştirin. Playerın temel görünümünü değiştirmemenizi öneriyoruz. `~/player/css/themes/[tema adı]/` altdizninde bulunan `style.css` dosyasını düzenleyerek sadece renkleri, arka planı ve yazı tipini değiştirirseniz iyi olur. 

Özelleştirilmiş temanızı bitirdikkten sonra, kullanıcıların temayı seçme özelliğini devre dışı bırakıp playerı sadece bu temayı sunmak üzere ayarlayabilirsiniz.

#Yerelleştirme
SolaShout Player&rsquo;ın arabirimini herhangi bir dile yerelleştirebilmek için özel bir arabirim mevruttur. Arabirimi yerelleştirenin İngilizce bildiği ve çeviriyi o dilden yapacağı varsayılır. Çevrilmeyen metinler otomatikman İngilizcede görüntülenecektir. SolaShout Player ayrıca Arapça, Farsça veya İbranice gibi sağdan sola yazan dilleri görüntülemek için hazırdır.

Arabirime yeni bir dil eklemek için Dil Ekle düğmesini tıklayıp görüntülenen arabirimden İngilizcenin çevrileceği dili seçiniz.

Her bir yerelleştirme o dilin kendi ismi ile görüntülenir. Arkasında parantez içinde arabirimin görüntülendiği dil tanımı yer alır.

Tüm yerelleştirmeler görüntülenebilir ve İngilizce hariç tüm yereleştirmeler düzenlenebilir veya silinebilir. *İngilizce yerelleştirme dosyaları silinirse SolaShout Player çalışmaz hale gelir!*

Her bir metin arabirimin neresinde yer aldığına dair bir tanıma sahiptir. Bu tanım ayrıca metinde geçmesi gereken özel karakterleri tanımlar. Bu tanım dil görüntüleme veya dil düzenle arabirimlerinde bir metne tıklandığında görüntülenir.

Düzenleme arabirimi açılınca çevrilmemiş metinler varsa, arabirim otomatik olarak ilk çevrilmemiş öğeyi görüntüler.

Çok uzun metinler çevrilecekse, çeviri penceresi tüm görsel alanı kaplamak üzere büyütülebilir.

Dil düzenle arayüzü şu kısayol tuş kombinasyonlarını içerir:

Tuş kombinasyonu... | Eylem
------------------- | -----
Ctrl + Shift + Enter | Listedeki sonraki metne geç
Ctrl + Shift + ↑ | Listedeki önceki metne geç
Ctrl + Shift + E | Çeviri alanını büyüt veya küçült
Ctrl + Shift + Q | Çeviri arabirimini kapat
Ctrl + Shift + S | Kayıtlı olmayan tüm metinleri kaydet
Ctrl + Shift + V | Orijinal (İngilizce) metni çeviri kutusuna yapıştır

|| Yerelleştirme metinleri güzelleştirmek için [Github tadındaki Markdown](https://guides.github.com/features/mastering-markdown/) kodlarını kullanabilirsiniz. Kaynak dildeki güzelleştirmeleri ve vurguları kullanmayı öneririz.

Sıkça kullanılan Markdown kodları:
> `**yazı**` - kalın yazı||
> `*yazı*` - italik yazı||
> `***yazı***` - kalın ve italik yazı

Sayılı liste:
> `1. Öğe`||
> `2. Öğe`||
> `3. Öğe`

Madde imli lists:
>`- Öğe`||
>`- Öğe`

SolaShout Player çeviri arayüzü bu komutlara `||` komutunu ekler. Bu komut metni yeni bir satıra (`<br>`) taşır. Bu komut orijinal (İngilizce) metinde bulunursa, bunu çeviriye taşımanızı rica ederiz.

*Otomatik olarak kaydet* şalteri açık olursa (varsayılarak açıktır), kaydedilmemiş satırlar her 60 saniyede bir kaydedilir.

Yerelleştirme işleminin tamamladığınızda, `[dil kodu].php` dosyasını solashoutplayer[at]hawke-ai.com adresine göndermenizi veya [Bitbucket deposuna](https://bitbucket.org/WolfHawke/sc2-multistream-player/) eklemenizi rica ederiz. Böylece çevirdiğiniz arabirimden başka kullanıcılar da yararlanabilir.

#Güvenlik
SolaShout Player yönetimi basit olmak üzere tasarlandı. Ayarlar ve yereleştirmeler PHP dizinlerini içeren metin dosyalara yazılır. Bu dosyalar hem ön hem de arka uç tarafından kullanılır. Buradaki temel fikir, yazılımın sunucuya olan erişimi olabildiğince kadar engellemektir. SolaShout Player son kullanıcıyı düşünerek hiçbir takip yazılımını içermez. İnternet yayınlarını sunanların bağlantıları ya SHOUTCast yazılımı üzerinden ya da web sunucunun mevcut kütüklerini kullanarak takip edecekleri varsayılır. Üçüncü parti takip yazılımını kullanmayı hedefliyorsanız, bunu ekleme kodlama yeteneğine sahip olup bunu kendiniz ekleyeceğiniz varsayılır.

SolaShout Player her açıldığında `~/player/config/settings.php` doğrular. Böyle herhangi geçersiz bir ayarın kullanılmasını önler. Suncunuzun güvenliğini sağlamak için `~/player/config/settings.php` dosyasında herhangi bir hata bulunursa SolaShout Player açılmaz (bkz. Sorun Giderme). 

##SolaShout Player Ayarlar Arabirimini Devre Dışı Bırakmak
SolaShout Player ayarlar arabirimi yöneticileri tarafından devre dışı bırakılabilmek (&ldquo;kilitlemek&rdquo;) üzere tasarlanmıştır. Bu ya kurulum sihirbazı ile ya da ayarlar arabirimindeki ayar ile gerçekleştirileilir.

Güvenliği arttırmak için `~/config/settings.php` dosyasının yazma izinleri sıfırlanabilir ve `streamconfig` altdizini silenbilir veya erişimi engellenebilir.

##SolaShout Player Ayarlar Arabirimini Etkinleştirmek
SolaShout Player&rsquo;ın ayarlar arabirimini devre dışı bırakırsanız, ayarlar arabirimi ve/veya kurulum sihirbazını etkinleştirebilmek için bir anahtar kodu sağlanacaktır. Bu arabirimlerine ulaşabilmek için `http://sizindomain.com/player/streamconfig/?unlock-code=[anahtar kodu]` adresine gidiniz.

İlk kurulumda bir e-Posta adresi girmiş bulunuyorsanız, arabirim kilidini açınca ayarlar arabirimi görüntülenecek. Buradan anahtar kodunu geçici bir parola olarak kullanarak giriş sağlayabilirsiniz. Bir sonraki adımda yeni bir parolayı oluşturmanız gerekecektir.

İlk kurulumda bir e-Posta adresi girmediyseniz, ilk açılıştaki olduğu gibi kurulum sihirbazı çalıştırılacaktır ve mevcut ayarlarınız silinecektir.

Sunucuya erişiminiz varsa, `~/player/config/lockdown.txt` dosyasını silebilirsiniz. Yukarıda yazıldığı gibi e-Posta adresi tanımlamadıysanız kurulum sihirbazı çalıştırılır.  `~/player/config/settings.php` dosyasında bir e-Posta adresi tanımlı olursa, o adresi ve anahtar kodunu kullanarak ayarlar arabirimine giriş sağlayabilirsiniz. 

##HTTPS, Ayar Arabirimi ve Sesli Yayın
SolaShout Player ayar yaparken HTTPS güvenliği ile çalışmayı tercih eder. Ayar sırf HTTP üzerinden yapılınca, bazı ayarlara (örn. PWA olarak çalıştırmak) erişim olmaz. Ayarlar HTTP ile yaılabilir, ama güvenli olmayan bu bağlantıdan kaynaklanan e-Posta adresi, parola ve giriş bilgilerinin herkese açık olacağını hatırlamak gerek.

Google Chrome sürüm 81&rsquo;den itibaren o tarayıcı artık HTTPS ile güvenli kılınan sayfalardan HTTP bağlantılara erişimi engellemiştir (Firefox her halde buna yakın zamanda katılacatır). Chrome ve Chromium-tabanlı diğer tarayıcılar (örn. Opera, Microsoft Edge, Brave ve Vivaldi) kullanılan en popüler yazılımlardır. Bundan dolayı SHOUTCast yayınınızı HTTPS üzerinden sunmanız özellikle önerilir.

Yayınlarınız sadece HTTP ile sunulabilirse, onların SolaShout Player tarafından düzgünce yayımlanmalarını sağlamak için şu uygulamalar başvurabilirsiniz:

1. İlk ayarlarınızı HTTPS bağlantısı üzerinden yapın. Ayarları tamamlayınca yazılımı kilitleyin.
2. Ayar tamam olunca, SolaShout Player&rsquo;ı `http://siteniz.com/player` adresinden sunun.

Sunucunuza erişiminiz varsa, certbot ve stunnel gibi yazılımları kullanarak yayınlarınızı HTTPS ile şifreleyebilirsiniz. Yoksa proxy sunucu kullanarak yayını HTTPS üzerinden sunabilirsiniz.

#Hata Giderme
##Yayınım Çalmıyor
Şunları kontrol edin:

1. Chrome veya Chromium-tabanlı tarayıcı kullanıyor musunuz? HTTPS ile yayımlanan SolaShout Player&rsquo;deki SHOUTCast yayınları HTTP üzerinden mi sunuluyor? Bu durumda ya SolaShout Player&rquo;ı HTTP bağlantısına taşıyın veya SHOUTCast yayınlarını HTTPS bağlantısına taşıyın.
2. Reklam önleyici yazılım (adblocker) tarayıcınızda etkin mi? Bazı reklam önleyici yazılımlar yayının çalmasını önler. Yazılımı etkisiz hale getirin ve sayfayı güncelleyin.
3. Yayınız faaliyette mi? SHOUTCast yayını sunan suncucu URL&rsquo;ni açıp kontrol edin.

##Yayın Firefox ve Safari&rsquo;de sorunsuz çalışıyor, ama Chrome&rsquo;da çalışmıyor.
HTTPS ile yayımlanan SolaShout Player&rsquo;deki SHOUTCast yayınları HTTP üzerinden mi sunuluyor? Bu durumda ya SolaShout Player&rquo;ı HTTP bağlantısına taşıyın veya SHOUTCast yayınlarını HTTPS bağlantısına taşıyın.

##SolaShout Player&rsquo;in ayarlar arabirimine anahtar kodu ile giremiyorum.
1. URL&rsquo;yi ve anahtar kodunu doğru girdiğinizi kontrol edin.
2. &ldquo;SolaShout Player Ayarlar Arabirimini Etkinleştirmek&rdquo; bölümünde belirtildiği gibi  `~/player/config/lockdown.txt` dosyasını siliniz.

##settings.php dosyası hatalı mesajı geliyor
Ayarları düzeltmek için kurulum sihirbazını tekrar çalıştırın. Sunucuya erişebilirseniz hatayı bulmak için `~/player/config/settings.php` dosyasına bakın. `config` altdizinindeki `examples-settings.php` dosyası ayarların nasıl görünmesi gerektiği hakkında detaylar içerir. `streamconfig` altdizinini sildiyseniz, altdizini geri yükleyin veya player çalışmaya başlayıncaya kadar `settings.php` dosyasını düzeltin.

##Diğer sorunlar
Başka bir sorun varsa, SolaShout Player geliştiricilerine solashoutplayer[at]hawke-ai.com adresinden veya [Bitbucket deposuna](https://bitbucket.org/WolfHawke/sc2-multistream-player/issues) sorununuzu bildirin.

#Lisans
Hawke AI deposunda sunulan SolaShout Player Creative Commons Alıntı-GayriTicari-Türetilemez 4.0 Uluslararası (CC BY-NC-ND 4.0) lisansı altında sunulmaktadır (tam metin için bkz. `LICENSE.md` veya altbilgideki lisans linkini tıklayınız).

Aşağıdaki şartlar altında eseri her ortam veya formatta kopyalayabilir ve yeniden dağıtabilirsiniz. Uygun referans vermeli, lisansa bağlantı sağlamalı ve değişiklik yapıldıysa bilgi vermelisiniz. Bunları uygun bir şekilde yerine getirebilirsiniz fakat bu, lisans sahibinin sizi ve kullanım şeklinizi onayladığını göstermez. Bu yazılımı ticari amaçlarla kullanamazsınız. Eğer yazılımı karıştırdınız, aktardınız ya da materyalin üzerine inşa ettiyseniz, değiştirilmiş materyali dağıtamazsınız.

SolaShout Player&rsquo;ı ticari amaç için kullanmak isterseniz (bunlarla sınırlamamış olarak örn. reklam veya abonelik alarak bir yayını dinletmek), Envato CodeCanyon&rsquo;dan Normal veya Genişletilmiş bir lisans temin edebilirsiniz."

SolaShout Player JQuery ve FontAwesome yazılımlarını içerir. Bunların kendilerine has bir lisansı var. İzin ile kullanılmıştır.

SolaShout Player ayrıca [MIT Lisansı](https://mit-license.org/) kapsamında şu yazılımları içerir:
- Bootstrap
- JQuery.cookie
- JQuery.scrollTo
- Parsedown
',
);
