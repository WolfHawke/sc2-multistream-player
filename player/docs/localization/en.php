<?php
/**
 * SolaShout Player Documentation
 * Localization file => English
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */
$LANGUAGE = array(
    'languageCode' => 'en',
    'languageName'=> 'English',
    'languageIsoName'=> 'English',
    'languageDirection' => 'ltr',
    'updated' => '2020-10-07',
    'title' => 'SolaShout Player Documentation',
    'nav_player' => 'Player',
    'nav_contents' => 'Contents',
    'nav_language' => 'Language',
    'copyright' => 'Copyright',
    'rights' => 'All Rights reserved.',
    'license' => 'See [License] for usage details.',
    'current_ver' => 'Current Version',
    'doc_update' => 'Documentation Update: rev.',
    'body' => '#Description
SolaShout Player is a light-weight standalone web-based audio player for SHOUTCast v2 streams. The front end is written using HTML, CSS, and JavaScript. The back end is written in PHP. Each SolaShout Player instance can host up to 10 SHOUTCast v2 streams. The interface is simple and intuitive and designed with mobile use in mind. The application is designed to be completely stand-alone and stores its settings in a single PHP file. No database connection is required.

***Please note:** SolaShout Player will not play Icecast or SHOUTCast v1 streams. It is specifically designed to only play SHOUTCast v2 streams.*

#System Requirements
- PHP 7.2 or higher
- Write access to `~/player/config` directory during initial set up
- URL and port for the streams that will be broadcast
- For localization write access to `~/player/localization`, `~/player/docs/localization` and `~/player/streamconfig/localization` directories

#Installation
1. Upload the player folder to your web server.
2. Check that your server has write access to `~/player/config`, `~/player/localization`, `~/player/docs/localization` and `~/player/streamconfig/localization` directories.
3. Open `http://yourserver.com/player` in your web browser.
4. Configure your streams using the wizard.
5. Listen to your streams.

#Configuration
##Configuring SolaShout Player using the Wizard
SolaShout Player can be configured using a wizard. When initially installing SolaShout Player, this is the only interactive way to configure the player. Once the initial setup is complete, it is possible to use the configuration interface (see below) to change your settings.

Stepping through the wizard should be self-explanatory, as each screen explains what is going to happen, including small information tidbits regarding what each setting does. Two points should be emphasized.
1. If SolaShout Player is set up to broadcast a series of streams that are hosted on a single SHOUTCast v2 server, the wizard will automatically detect any stream already present on that server. It will display the stream names that it receives from the SHOUTCast configuration file. If you wish to alter those names, you can do so by clicking on the name and changing it before moving on to the next step. Any streams that are down will not be automatically selected for broadcasting, but you can add them anyway.
2. The configuration interface can be disabled with the option to re-enable it. This is called “locking out” the interface (see section on Security below). It is highly recommended to define an e-mail address if you chose to lock out the interface. Otherwise once unlocking it, you will be required to run through the entire setup wizard again. As mentioned in the wizard, the e-mail address entered is not transmitted in any way and is only stored on the server that SolaShout Player is run from. It is not accessed by the public-facing pages, only by the configuration interface, and is not displayed anywhere in the rendered HTML or JavaScript. Feel free to examine the code to see how the e-mail address is used.

##Configuring SolaShout Player using the Configuration Interface
SolaShout Player provides a configuration interface at `~/player/streamconfig` for easy modification of the stream data and player behavior. Most of the options have explanations in the interface, which can be accessed by hovering over the `?` symbol next to each item.

The configuration interface can be locked out from this page as well.

The settings can be saved by either clicking the green Save Settings button at the bottom or the Save Settings menu item on the navigation bar.

If you do not wish to change the password when you modify the settings of your instance of SolaShout Player, simply leave those fields blank.

##Configuring SolaShout Player via settings.php.
For those who like to get down and dirty, SolaShout Player can be configured by editing the `~/player/config/settings.php` file, which contains a PHP array with the various settings. All of the array items are required. An example can be found under `~/player/config/example-settings.php`.

If you choose to remove the `~/player/streamconfig` folder and its contents, this will be the only means for configuring SolaShout Player.

You can generate the password hash by running `php genpwdhash.php [your password]` script in the `~/player/` directory.

##Serving SolaShout Player as a Progressive Web App
** Coming soon, not yet in production **

If you run SolaShout Player on a HTTPS-secured server, you will have the option to provide it to your users as a Progressive Web App (PWA). PWAs can be installed on machines running Chromium-based browsers or mobile devices, and thus will act like native applications. This can keep your internet audio streams front and center with your listeners. If the PWA function is enabled, you can designate a separate title for the SolaShout Player to differentiate it from other SolaShout Player instances.

#Theme Customization
SolaShout Player comes with five built-in themes. To customize the look of SolaShout Player, you will need to base your custom theme on one of the existing five themes. It is recommended to leave the base look-and-feel of the player alone and only customize colors, font styles and background using one of the existing `style.css` files under `~/player/css/themes/[theme name]/`.

Once you have created your custom theme, you can configure the player to only display that theme by disabling the option to allow users to select the theme.

#Localization
SolaShout Player provides a full localization interface that will allow you to localize every portion of the application. When localizing the interface, it is expected that the translator knows English and will work from the English strings in the interface. SolaShout Player is designed so that any strings not localized will display in English. SolaShout Player is also ready for localization for right-to-left languages like Arabic, Persian or Hebrew.

New localizations can be created by using the Add Language button. You can then select the new localization you wish to add from the list provided.

Each localization is displayed using its native name followed by its name in the localization that is currently employed.

All localizations can be viewed. Any localization except for English can be edited or deleted. *SolaShout Player will not work if the English localization files are deleted!*

Each string has a description as to where in the interface it is and what purpose it serves. The description will also include any special characters or formatting the should be included in the translation of the string. These can be displayed by selecting the string in the View Language or Edit Language interfaces.

If there are untranslated strings in the localization, the interface will immedately skip to the first untranslated string upon opening.

The translation box can be expanded to fill the work area when translating very long strings, like the documentation.

The following keyboard shortcuts are available when using the Edit Language interface:

Key Combination  | Action
---------------- | ------
Ctrl + Shift + Enter | Move to next string in list
Ctrl + Shift + ↑ | Move to previous string in list
Ctrl + Shift + E | Expand or contract translation box
Ctrl + Shift + Q | Close translation interface
Ctrl + Shift + S | Save all unsaved strings
Ctrl + Shift + V | Copy original (English-language) text to translation box

|| You can use [Github-flavored Markdown](https://guides.github.com/features/mastering-markdown/) to beautify the localizations. In certain places of the localization, the source strings will have this markup included. It is recommended to follow suit in the language you are localizing the interface to.

Some commonly used Markdown commands:
> `**word**` for bold||
> `*word*` for italic||
> `***word***` for bold italic

Numbered lists:
> `1. Item`||
> `2. Item`||
> `3. Item`

Bulleted lists:
>`- Item`||
>`- Item`

One addition that the SolaShout Player localization interface provides is `||` for wrapping a line. This will cause a line-wrap (`<br>`)within the string translated. Please follow any instances of this in the original (English) strings.

If the Autosave switch is flipped (it is on by default) all unsaved strings will automatically be saved every 60 seconds.

When you are finished with your localization, you can submit it to be incorporated in the full release of SolaShout Player by e-mailing the [language code].php files to solashoutplayer[at]hawke-ai.com or submit it them to the [Bitbucket repository](https://bitbucket.org/WolfHawke/sc2-multistream-player/).

# Security
SolaShout Player is designed to keep management simple. It writes its settings and localizations to text files containing PHP arrays that can be read by both the front and back ends. The idea behind this is to keep access to the host system strictly limited. SolaShout Player is designed to be friendly to the end user with no tracking capabilities built into the player. It is assumed that the hosts using SolaShout Player will do their user tracking through their web server or their SHOUTCast server. If you wish to add third-party tracking capabilities to SolaShout Player, you will have to do so yourself and provide the necessary warnings.

SolaShout validates `~/player/config/settings.php` every time it starts to make sure that no invalid settings are being passed along. If there are any errors in the structure of `settings.php`, SolaShout Player will not run (see the troubleshooting section below) to make sure your server stays secure.

## Disabling the SolaShout Player Configuration Interface
SolaShout Player is designed so that administrators completely disable (“lock down” or “lock out”) any access to the configuration interface. This is configured through the wizard upon initial set up or using the appropriate option in the configuration interface.

For extra security, the permissions for `~/config/settings.php` can be set to read-only and the `streamconfig` folder can be removed or set to no access.

## Unlocking SolaShout Player for Configuration
If you choose to lock down SolaShout Player, you will be provided with a code that will allow you to unlock the configuration interface and/or wizard. You can access either the configuration interface or the wizard by navigating to `http://yourdomain.com/player/streamconfig/?unlock-code=[your code]`.

If you set up an e-mail address when first running the wizard, then you will be taken to the configuration interface and you will be able to log in with the unlock code as your password. You will then be prompted to create a new password.

If you did not set up an e-mail address when running the wizard, you will be presented with the wizard and will have to set up SolaShout Player as if you were running it for the first time.

Alternatively, if you have access to the server, you can delete `~/player/config/lockdown.txt` and the wizard will run again if you have not defined an e-mail address. If you have configured a proper e-mail address in `~/player/config/settings.php`, you can use that and your unlock code as a password to log into the configuration interface.

## HTTPS, Configuration and Audio Streaming
SolaShout Player prefers to be run under HTTPS for configuration purposes. Certain options, such as being run as Progressive Web App, are not available if you run the player on plain HTTP. It will work when configured over HTTP, but you will be faced with the classic security issues of your e-mail, password and configuration data being sent in plaintext over the internet.

With the release of Google Chrome v.81, that browser will no longer allow for HTTPS-secured sites to open non-HTTPS-secured resources (Firefox is likely soon to follow suit). Chrome and its Chromium-based derivatives (e.g. Opera, Microsoft Edge, Brave Browser or Vivaldi) are the most popular browsers in use. Thus, it is highly recommended that your SHOUTCast streams be available over HTTPS as well.

If the streams you wish to broadcast are only available over HTTP, you can use the following method to ensure that SolaShout Player will stream them:

1. Do your initial configuration of SolaShout Player using an HTTPS connection. Lock down the player when you’re finished.
2. Once configuration is complete, serve SolaShout Player using `http://yoursite.com/player`

If you have access to your own server, you can add HTTPS encryption to the streams by relaying them over your server using stunnel or a similar utility. Alternatively, use a proxy server to provide the streams via HTTPS.

#Troubleshooting
##My stream won’t play
Check for the following:

1. Are you running Chrome or a Chromium-based browser with SolaShout Player running over HTTPS and the streams over HTTP? If so, either move your instance of SolaShout Player to HTTP or move your streams to HTTPS.
2. Is there advertisement blocker software running in your browser? Sometimes this can prevent audio from playing properly. Disable your advertisement blocker and try again.
3. Is your stream up? Visit your SHOUTCast stream’s host URL to determine if the stream is up.

##My stream plays fine in Firefox and Safari, but not on Chrome
Are you running Chrome or a Chromium-based browser with SolaShout Player running over HTTPS and the streams over HTTP? If so, either move your instance of SolaShout Player to HTTP or move your streams to HTTPS.

##I can’t unlock SolaShout Player
1. Check to see if you properly entered the URL with the unlock code.
2. Follow the instructions to delete `~/player/config/lockdown.txt` detailed above.

## SolaShout Player says that settings.php is invalid
Rerun the wizard to reconstitute the settings or, if you have access to the server, look at `~/player/config/settings.php` to see where the error might lie. You can reference `examples-settings.php` in the `config` folder. If you have removed the `streamconfig` folder, you can either re-upload it or edit `settings.php` to make the player work again.

##Other Issues
For other issues, contact the creator of SolaShout Player at solashoutplayer[at]hawke-ai.com or open an [Issue ticket](https://bitbucket.org/WolfHawke/sc2-multistream-player/issues) at the Bitbucket repository.

#License
SolaShout Player hosted at Hawke AI is released under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0) License (see `LICENSE.md` or click the license link in the footer for the full text).

You are free to copy and redistribute the material in any medium or format, providing the following: You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. You may not use the material for commercial purposes. If you remix, transform, or build upon the material, you may not distribute the modified material.

If you wish to use SolaShout Player for commercial purposes, you can purchase a Regular or Extended License at CodeCanyon by Envato.

SolaShout Player uses JQuery and FontAwesome, each of which have their own licenses. These are used by Permission.

SolaShout Player includes the following libraries that are used under the [MIT license](https://mit-license.org/):
- Bootstrap
- JQuery.cookie
- JQuery.scrollTo
- Parsedown
'
);

?>
