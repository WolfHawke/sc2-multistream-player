/**
 * SolaShout Player Documentation Navigation JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* docs object */
function DocNav() {
    /* properties */
    this.htmlTpl = "<a class=\"dropdown-item waves-effect waves-light\" href=\"#%href%\" onclick=\"docNav.goto('#%loc%'); return false;\">%title%</a>\n"; /* template for content navigation links in header */
    this.linkHtml = ""; /* html to render */
    this.basePixelSize = 16; /* base pixel size for calculating how much to roll back when reaching an anchor point */

    /* self reference */
    _me = this;

    /* methods */
    /**
     * Function: _init
     * Description: Initializes the navigation menu
     * Arguments: none
     * Returns: nothing
     */
    var _init = function() {
        $("h1").each(function (k,i){
            var ref = "docs_sec_" + k;
            var lnk = _me.htmlTpl.replace("%href%", ref);
            lnk = lnk.replace("%loc%", ref);
            lnk = lnk.replace("%title%", i.innerHTML);
            i.id = ref;
            _me.linkHtml += lnk;
        });
        $("#sspDocNavContainter").html(_me.linkHtml);
        $("#sspDocNav").show();
    }

    /**
     * Function: goto
     * Description: Activates scrolling
     * Arguments: loc = anchor to scroll to
     * Returns: nothing
     */
    this.goto = function (loc) {
        var rollback = parseInt($(loc).css("height"))  + _me.basePixelSize;
        if ($(".navbar-toggler").css("display") != "none") {
            $(".navbar-toggler").click();
        }
        $(window).scrollTo(loc);
        $(window).scrollTo("-=" + rollback + "px");
    }

    _init();
}

/* instantiate Docs object */
$(document).ready(function (){
    window.docNav = new DocNav();
});
