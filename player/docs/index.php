<?php
/**
 * SolaShout Player Documentation Index Page
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

 /* import version information */
 require("../version.php");

/* require the Parsedown module to parse the Markdown */
require("../Parsedown.php");
$mkdn = new Parsedown();
$mkdn->setSafeMode(true); /* make sure we're in safe mode */

/* load the languages available */
$dl_dir = array_diff(scandir('../docs/localization/'), array ('.','..'));
$doc_langs = array();

/* load the proper localization */
/* default language is English */
$setlang = "en";
/* include english first, because there may be strings missing in translation */
if (file_exists("./localization/en.php")) {
    require("./localization/en.php");
    $def_lang = $LANGUAGE;
} else {
    print("<p><b>Critical error!</b> Core localization file missing. Please make sure <code>en.php</code> is located in the <code>docs/localization/</code> folder. It is required for the configuration interface to work.</p>");
    die();
}

/* build list of documenatation languages */
foreach ($dl_dir as $f) {
    include("../docs/localization/{$f}");
    $doc_langs[substr($f,0,strpos($f,'.'))] = $LANGUAGE['languageName'];
}

/* get cookie, if it is set */
if (isset($_COOKIE['lang'])) { $setlang = $_COOKIE['lang']; }
/* if we're switching to a new language, set that as primary */
if (isset($_GET['lang'])) { $setlang = $_GET['lang']; }
/* post language to cookie, which should last for three hours from now */
setcookie('lang', $setlang, time() + (180*60));
/* load localized strings page */
if (file_exists("./localization/{$setlang}.php")){
    include("./localization/{$setlang}.php");
    $LANGUAGE = array_replace_recursive($def_lang, $LANGUAGE);
}

/* header content */
$html = "<!DOCTYPE html>
<html lang=\"{$setlang}\">

<head>
  <meta charset=\"utf-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
  <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
  <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
  <meta http-equiv=\"expires\" content=\"0\" />
  <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
  <meta http-equiv=\"pragma\" content=\"no-cache\" />
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
  <title>{$LANGUAGE['title']}</title>
  <!-- Font Awesome -->
  <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
  <!-- Bootstrap core CSS -->
  <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
  <!-- Your custom styles (optional) -->
  <link href=\"./css/docs.css?v=" . time() . "\" rel=\"stylesheet\">
  <!-- Favicon -->
  <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
</head>

<body>";

/* navigation */
$html .= "    <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
        <a class=\"navbar-brand\" href=\"../docs/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"{$LANGUAGE['title']}\">&nbsp;{$LANGUAGE['title']}</a>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"../\">{$LANGUAGE['nav_player']}</a>
                </li>
                <li class=\"nav-item dropdown\" id=\"sspDocNav\">
                <a class=\"nav-link dropdown-toggle waves-effect waves-light\" id=\"navbarDropdownMenuLink\"
                    data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{$LANGUAGE['nav_contents']}</a>
                <div id=\"sspDocNavContainter\" class=\"dropdown-menu dropdown-info\" aria-labelledby=\"navbarDropdownMenuLink\">
                </div></li>\n";
/* localization */
$html .= "
                   <li class=\"nav-item dropdown\">
                        <a class=\"nav-link dropdown-toggle waves-effect waves-light\" id=\"navbarDropdownMenuLink\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{$LANGUAGE['nav_language']}</a>
                        <div class=\"dropdown-menu dropdown-info\" aria-labelledby=\"navbarDropdownMenuLink\">\n";
foreach($doc_langs as $l => $n) {
    $html .= "                            <a class=\"dropdown-item waves-effect waves-light\" href=\"index.php?lang={$l}\">{$n}</a>\n";
}
$html .="                        </div>
                    </li>\n";

$html .= "            </ul>
        </div>
    </nav>\n";

/* body */
$html .= "<div class=\"container topspace bottomspace\">
    <p class=\"top-matter\">{$LANGUAGE['current_ver']}: " . VERSION . "<br>
    {$LANGUAGE['doc_update']}{$LANGUAGE['updated']}</p>\n";
$html .= $mkdn->text($LANGUAGE['body']);
$html = str_replace('||','<br>', $html);
$html = str_replace('<code><br></code>', "<code>||</code>", $html);

$html .= "</div>\n";

/* footer */
$html .= " <!-- Footer -->
    <footer class=\"container col-12 bottom\">
    <p class=\"text-center\"><small>SolaShout Player {$LANGUAGE['copyright']} &copy; 2020 Hawke AI. {$LANGUAGE['rights']} " . str_replace('[',"<a href=\"{$license_url}\" target=\"blank\">", str_replace(']', '</a>', $LANGUAGE['license'])) . "</small></p>
    </footer>\n\n";

/* JavaScript */
$html .= "    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type=\"text/javascript\" src=\"../js/jquery-3.4.1.min.js\"></script>
    <!-- Bootstrap tooltips -->
    <script type=\"text/javascript\" src=\"../js/popper.min.js\"></script>
    <!-- Bootstrap core JavaScript -->
    <script type=\"text/javascript\" src=\"../js/bootstrap.min.js\"></script>
    <!-- JQuery ScrollTo -->
    <script type=\"text/javascript\" src=\"../js/jquery.scrollTo.min.js\"></script>
    <!-- Page-specific JavaScript -->
    <script type=\"text/javascript\" src=\"js/docs.js\"></script>\n";

/* html end */
$html .= "</body>\n</html>";

print($html);
?>
