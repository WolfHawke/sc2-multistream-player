<?php
/**
 * SolaShout Player Version information
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 * @version 0.11
 */

/* version */
const VERSION = '0.12';
const DATE = '2020-08-06';

/* license url */
$license_url = 'http://www.hawke-ai.com/streamplayer-license';
?>
