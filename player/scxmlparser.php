<?php
/**
 * SolaShout Player
 * ShoutCast v2 statistics XML parsing script
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */


function scXmlToArray($url,$returnxmlvals=FALSE) {

  /* load xml parser */
  if (!function_exists('xml_parser_create')) {
    return array();
  }
  $parser = xml_parser_create('');

  /* get statistics XML file */
  if (function_exists('curl_version') && strpos($url,'http') === 0) {
      /* curl is faster than fopen, so prefer that if we're working with online content */
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $contents = curl_exec($ch);
      curl_close($ch);
  } else {
      /* file reading via fopen only if curl does not exist */
      $contents = '';
      if (!($fp = @ fopen($url, 'rb'))) { return array (); }
      while (!feof($fp)) { $contents .= fread($fp, 8192); }
      fclose($fp);
  }

  /* parse xml */
  xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  xml_parse_into_struct($parser, trim($contents), $xml_values);
  xml_parser_free($parser);
  if (!$xml_values) { return array(); }


  /* arrays needed for parsing xml container */
  $out = array();    /* container for final returned array */
  $s = array();      /* array containing stream objects */
  $s_item = array(); /* array to build single stream object */
  $s_active = FALSE; /* whether or not we've started processing streams */

  /* begin parsing xml_parser array */
  foreach ($xml_values as $v) {
    $d = array();
    /* skip core tags */
    if ($v['tag'] != 'SHOUTCASTSERVER' && $v['tag'] != 'STREAMSTATS') {
      if ($v['tag'] == 'STREAM') {
        /* we've reached the streams so set up parsing them */
        if ($v['type'] == 'open') {
          $s_active = TRUE;
          $s_item['STREAMID'] = $v['attributes']['id'];
        } elseif ($v['type'] == 'close') {
          /* if we have any tags after the Stream, then make sure we close the
             tag and append the content to the streams object */
          $s_active = FALSE;
          $s[] = $s_item;
          $s_item = array();
        }
      } elseif ($s_active) {
        /* parsing stream object contents */
        if (isset($v['value'])){
          $s_item[$v['tag']] = $v['value'];
        } else {
          $s_item[$v['tag']] = '';
        }
      } else {
        /* parsing all other items */
        if (isset($v['value'])){
          $out[$v['tag']] = $v['value'];
        } else {
          $out[$v['tag']] = '';
        }
      }
    }
  }

  /* append streams to output array */
  $out['STREAMS'] = $s;

  return $out;
}

?>
