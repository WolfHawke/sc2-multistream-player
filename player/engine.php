<?php
/**
 * SolaShout Player Engine
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */


/* set constants */
const URL_HISTORY = "played.html?sid=";
if (file_exists("./config/settings.php")){
    require ("./config/settings.php");
} else {
    die();
}

/* functions */
require ("streamconfig/functions.php");
require ("scxmlparser.php");

/* get connection data */
if (isset($_GET['stream'])) {
    $input = $_GET;
} elseif (isset($_POST['stream'])) {
    $input = $_POST;
} else {
    print('{"error":"invaild_input"}'); die();
}
/* split stream info */
if (strrpos($input['stream'],'sid=') !== FALSE){
    preg_match('/sid=[\d]+/',$input['stream'], $sid);
    if (is_array($sid)) {
        $sid = explode('=',$sid[0])[1];
    }
    $url = substr($input['stream'],0,strrpos($input['stream'],'/'));
} elseif (strrpos($input['stream'],'/') > 7) {
    $split = strrpos($input['stream'],'/');
    $sid = substr($input['stream'], ($split+1));
    if ($sid == ',') { $sid = 1; }
    $url = substr($input['stream'], 0, $split);
} else {
    $sid = 1;
    $url = $input['stream'];
}

/* load XML */
if (!$xml = scXmlToArray("{$url}/statistics", FALSE)) {
    print('{"error":"no_stats"}'); die();
}

$out = getStreamStats($xml,$sid);

/* retrieve history */
if (isset($input['history'])) {
    $out['history'] = getHistory($url,$out['sid']);
}

/* Get array for current stream */
print(json_encode($out));

?>
