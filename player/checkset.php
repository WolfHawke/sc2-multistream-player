<?php
/**
 * SolaShout Player Settings
 * String descriptions in English for localizing streamconfig interface
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/**
* validateSettings: checks if the settings array is properly formed
* Returns: a boolean as to whether the array is okay or not.
*
* @since    0.12
* @var      boolean whether to return a boolean or a word
*/
function validateSettings($word=FALSE) {
    global $settings;
    $integers = array('all_on_one', 'initial_stream', 'autoplay','user_change_theme', 'adblock_warning','pwa');
    $access_subarr = array('identity','challenge');
    $themes = array('black', 'blue', 'green', 'red', 'yellow');
    $invalid_chars = array('<', '>', '../', './', '"', "'", "\\", '~');
    $r = 'all_is_well';


    if (isset($settings)) {
        /* check strings */
        if (!isset($settings['url']) || gettype($settings['url']) != 'string' || !validateURL($settings['url'], FALSE)) { $r = 'corrupt'; }
        if (!isset($settings['port']) || gettype($settings['port']) != 'string' || intval($settings['port']) < 1) { $r = 'corrupt'; }
        if (!isset($settings['default_language']) || gettype($settings['default_language']) != 'string' || strlen($settings['default_language']) != 2) { $r = 'corrupt'; }
        if(!isset($settings['theme']) || gettype($settings['theme']) != 'string' || !in_array($settings['theme'], $themes)) { $r = 'corrupt'; }
        if (!isset($settings['pwa_title']) || gettype($settings['pwa_title']) != 'string') { $r = 'corrupt'; }
        /* check integers */
        foreach ($integers as $i) {
            if (!isset($settings[$i]) || (gettype($settings[$i]) != 'integer')) {
                $r = 'corrupt'; }
        }
        /* check streams array */
        if (isset($settings['streams']) && is_array($settings['streams'])) {
            foreach ($settings['streams'] as $k => $i) {
                if(gettype($k) == 'integer' && is_array($i)) {
                    if (!isset($i['name']) || gettype($i['name']) != 'string') { $r = 'corrupt'; }
                    /* make sure no invalid characters are in the name */
                    foreach($invalid_chars as $c) {
                        if (strpos($i['name'], $c) !== FALSE) { $r = 'corrupt'; }
                    }
                    if (!isset($i['sid']) || gettype($i['sid']) != 'string' || intval($i['sid']) < 1) { $r = 'corrupt'; }
                    if (!isset($i['url']) || gettype($i['url']) != 'string' || !validateURL($i['url'], TRUE)) { $r = 'corrupt'; }
                    if (!isset($i['path']) || gettype($i['path']) != 'string' || substr($i['path'],0,1) != '/' || preg_match('#^[A-Za-z0-9-._~!$&\'()*+,;=:/@%]+$#',  $i['path']) == 0) { $r = 'corrupt'; }
                } else {
                    $r = 'corrupt';
                }
            }
        } else {
            $r = 'corrupt';
        }
        /* check languages array */
        if (isset($settings['languages']) && is_array($settings['languages'])) {
            foreach ($settings['languages'] as $i) {
                if (gettype($i) != 'string' || strlen($i) != 2) { $r = 'corrupt'; }
            }
        } else {
            $r = 'corrupt';
        }
        /* check access array */
        if (isset($settings['access']) && is_array($settings['access'])) {
            if (isset($settings['access']['identity']) && gettype($settings['access']['identity'] == 'string')) {
                if (!validateEmail($settings['access']['identity']) && $settings['access']['identity'] != 'none') {
                    $r = 'corrupt';
                }
            } else {
                $r = 'corrupt';
            }
            if (!isset($settings['access']['challenge']) || !gettype($settings['access']['challenge']) == 'string' || preg_match("#^[A-Za-z0-9$./]+$#", $settings['access']['challenge']) != 1) {
                $r = 'corrupt';
            }
        } else {
            $r = 'corrupt';
        }
    } else {
        $r = 'nofile';
    }
    if (!$word) {
        if ($r == 'all_is_well') {
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
        return $r;
    }
}

/**
 * validateEmail: validates whether a string is an e-mail address
 * Returns: boolean containing response
 *
 * @since   0.9
 * @var     string  String containing a valid e-mail address
 * @var     boolean Whether or not to return list of streams available
 */
function validateEmail($e) {
    $regex = '/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i';
    if (preg_match($regex,$e) == 1) {
        return true;
    } else {
        return false;
    }
}

/**
 * validateURL: validates a URL or URL:port
 * Returns: boolean defining validity
 *
 * @since   0.12
 * @var     string  URL to validate
 * @var     boolean whether or not URL can have a port; defaults to TRUE
 */
function validateURL($u, $has_port=TRUE) {
    $fqdn = '/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/';
    $ip = '/(http|https):\/\/[\d]+\.[\d]+\.[\d]+\.[\d]+/';
    $ip_no_port = '/((http|https):\/\/)?(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2}))\.){3}(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2})))$/';
    $ip_has_port = '/((http|https):\/\/)?(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2}))\.){3}(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2})))(:[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535)$/';

    /* check if we have a pattern for an IP address */
    if(preg_match($ip, $u) == 1) {
        if ($has_port) {
            $result = preg_match($ip_has_port, $u);
        } else {
            $result = preg_match($ip_no_port, $u);
        }
        /* make sure we have no trailing slash */
        if ($result && (count(explode("/",$u)) > 3)) { $result = 0; }
    } else {
        /* if not, make sure that the uri is valid */
        $result = preg_match($fqdn, $u);
        /* check for port */
        if ($result == 1 && $has_port == TRUE) {
            $p = explode(":", $u);
            if (isset($p[2])) {
                $p[2] = intval($p[2]);
            } else {
                $p[2] = 0;
            }
            if((count($p) != 3) || ($p[2] < 1) || (count(explode("/", $u)) > 3)) { $result = 0; }
        } elseif ($result == 1) {
            if ((count(explode(":", $u))> 2) || (count(explode("/", $u)) > 3)) { $result = 0; }
        }

    }

    return $result;
    if ($result == 1) {
        return TRUE;
    } else {
        return FALSE;
    }
}


?>
