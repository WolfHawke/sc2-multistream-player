<?php
/**
 * Shoutcast v2 Multi-stream Permission Checker script
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/**
 * check_perms: Checks the permissions for a given scenario; returns boolean
 * @var     boolean Check localization folders, too
 * @since   0.10
 */
function check_perms($localization=FALSE) {
    $result = array();

    $dirs = array('config' => __DIR__ . DIRECTORY_SEPARATOR . 'config');

    if ($localization) {
        $dirs['docs'] = __DIR__ . DIRECTORY_SEPARATOR . 'docs' . DIRECTORY_SEPARATOR . 'localization';
        $dirs['loc_front'] = __DIR__ . DIRECTORY_SEPARATOR . 'localization';
        $dirs['loc_set'] = __DIR__ . DIRECTORY_SEPARATOR . 'streamconfig' . DIRECTORY_SEPARATOR . 'localization';
    }

    foreach ($dirs as $k => $d) {
        $result[$k] = (is_writeable($d)) ? TRUE : FALSE;
    }

    return $result;
}


/**
 * perm_results: writes out the results of check_perms
 * @var     array   Array containing the check_perms result
 * @since   0.10
 */
function perm_results($perms) {
    if(in_array(FALSE, $perms)) {

        $html = "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
  <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
  <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
  <meta http-equiv=\"expires\" content=\"0\" />
  <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
  <meta http-equiv=\"pragma\" content=\"no-cache\" />
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
  <title>SolaShout Player :: Permission Errors</title>
  <!-- Font Awesome -->
  <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
  <!-- Bootstrap core CSS -->
  <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
  <!-- Favicon -->
  <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
</head>
<body>
    <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
        <a class=\"navbar-brand\" href=\"../streamconfig/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"SolaShout Player\">&nbsp;SolaShout Player</a>
        <div class=\"navbar-nav mr-auto\">
            <div class=\"nav-item\">
                <a class=\"nav-link\" href=\"../docs/\">Documentation</a>
            </div>
        </div>
    </nav>
    <div class=\"container\">
        <p style=\"margin-top: 1em; \">Write permissions for SolaShout Player are not set properly for the configuration interface to function. Please check the permissions on the following directories:</p>
        <ul>\n";

    /* list directories that need to be writeable */
    if (!$perms['config']) {
        $html .= "            <li><code>player/config/</code> directory must be writeable for settings to be saved.</li>";
    }
    if (isset($perms['loc_front']) && !$perms['loc_front']) {
        $html .= "            <li><code>player/localization/</code> directory must be writeable to manage localizations.</li>";
    }
    if (isset($perms['loc_set']) && !$perms['loc_set']) {
        $html .= "            <li><code>player/streamconfig/localization/</code> directory must be writeable to manage localizations.</li>";
    }
    if (isset($perms['docs']) && !$perms['docs']) {
        $html .= "            <li><code>player/docs/localization/</code> directory must be writeable to manage localizations.</li>";
    }
    $html .= "      </ul>
        <p>Please see the <a href=\"../docs/\">Documentation</a> for more details.</p>
    </div>\n</body>\n</html>";

    print($html);
    die();
    }
}

perm_results(check_perms());
?>
