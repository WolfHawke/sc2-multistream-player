<?php
/**
 * SolaShout Player Password Hash Generator Tool
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 * @since   0.12
 */

/**
* validatePwd: validates the entered password`
*
* @since   0.7
* @var     string  password to validate
*/
function validatePwd($pwd) {
    $regx = array("uc" => "/[A-Z]/", "lc" => "/[a-z]/", "dig" => "/[0-9]/", "no" => "/[\\\"\']/");
    $tests = array();
    foreach ($regx as $k => $i) {
        $tests[$k] = preg_match($i,$pwd);
    }
    if (strlen($pwd) >= 8 && $tests['uc'] == 1 && $tests['lc'] == 1 && $tests['dig'] == 1 && $tests['no'] == 0) {
        return true;
    } else {
        return false;
    }

}

$pwd_req_msg = "The Password must be at least 8 characters long and contain upper and lower case
characters and at least one numeral. Including special characters (except \", '
or \\) and/or spaces is highly recommended.";

/* make sure that we have exactly one argument, the password to be modified */
if (!isset($argv)) {
    print("This tool can only be run from the command line.");
    die();
}

if (count($argv) == 2) {
    if ($argv[1] == "--help") {
        print("SolaShout Player Password Hash Generator Tool

Usage: php genpwdhash \"mypassword\"

{$pwd_req_msg}\n\n");
    } else {
        if (validatePwd($argv[1])) {
            print("\n\e[1;32mPassword hash:\e[0m ");
            print(password_hash($argv[1], PASSWORD_DEFAULT));
            print("\n\n");
        } else {
            echo "\e[1;31mERROR! Invalid password entered.\e[0m

{$pwd_req_msg}

\e[1;32mPlease check your password and try again.\e[0m\n\n";
        };
    }

} else {
    print("\e[1;31mERROR! Incorrect arguments entered.\e[0m
Be sure to escape special characters and/or enter the password in quotes.
Enter php genpwdhash --help for usage.\n\n");
}

?>
