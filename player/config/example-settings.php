<?php
/* Example Settings for SolaShout Player
 */
$settings = array(
    /* String: Primary or single SHOUTCast v2 server url; this can be either a FQDN or an IP Address WITHOUT a port number; do NOT append a / */
    'url' => 'https://my-shoutcast-server.com',
    /* String: The port number on which the primary or single SHOUTCast v2 server listens */
    'port' => '8000',
    /* Integer: defines whether all streams are hosted on the same server; 0 = false; 1 = true */
    'all_on_one' => 1,
    /* Integer: The stream to initially play when the player loads; corresponds to the subarray key in the 'streams' array */
    'initial_stream' => 1,
    /* Integer: Whether or not to automatically start playing on open; 0 = false; 1 = true */
    'autoplay' => 0,
    /* Array: An array of arrays containing information for the individual streams */
    'streams' => array(
        /* Array: array containing information for an individual stream; the array key must be an Integer. */
        1 => array(
            /* String: The name of the stream to display to select the stream */
            'name' => 'My Internet Stream',
            /* String: The stream id (SID) for the stream defined on the SHOUTCast v2 server */
            'sid' => '1',
            /* String: The URL:port combination of the stream; this can be a concatenation of url and port above, or else a completely different combination; do NOT append a / */
            'url' => 'https://my-shoutcast-server.com:8000',
            /* String: The path at which you can listen to the stream, if defined, requires a / before; if no path is defined, enter , */
            'path' => ',',
        ),
    ),
    /* Array: list of languages the interface is available in */
    'languages' => array(
        /* String: two-letter language code */
        'en','tr',
    ),
    /* String: The default language to display; two-letter language code from list above */
    'default_language' => 'en',
    /* String: name of the theme to intially show; Valid entries: black, blue, green, red, yellow */
    'theme' => 'blue',
    /* Integer: Whether or not users can change the theme; 0 = false; 1 = true */
    'user_change_theme' => 1,
    /* Integer: Whether or not users can change the language; 0 = false; 1 = true */
    'user_set_language' => 1,
    /* Integer: Whether or not to display a warning when adblocking software exists; 0 = false; 1 = true */
    'adblock_warning' => 0,
    /* Integer: Whether or not to enable Progressive Web App mode; 0 = false; 1 = true */
    'pwa' => 0,
    /* String: Title to display for Progressive Web App */
    'pwa_title' => 'My SolaShout Player Instance',
    /* Array: access credentials */
    'access' => array(
        /* String: e-mail address used to access streamconfig/ interface */
        'identity' => 'me@my-shoutcast-server.com',
        /* String: encoded password to access the streamconfig/ interface; default: Asdf1234 */
        /* Use the streamconfig interface or  a password_hash('your password', PASSWORD_DEFAULT) PHP command to generate a password */
        'challenge' => '$2y$10$gA1/RrLBYD7ZcMKdKeiThuwgwExvdP5yk5AAtFYMrElRmNpDju4Cy',
    ),
);
?>
