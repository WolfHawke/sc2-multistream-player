<?php
/**
 * SolaShout Player Localization Container Page
 * (called by index.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

$dev = FALSE;

/* force everything to run through index.php */
if (strpos($_SERVER['REQUEST_URI'],basename(__FILE__)) !== FALSE) {
    header("Location: ../streamconfig/");
}

/* get posted variables */
$input = $_POST;

$input['localize'] = $_GET['localize'];
if (isset($dev) && isset($_GET['target-lang'])) {
    $input = $_GET;
}

$html = "";
$page = isset($input['target-lang']) ? 'localize-edit' : 'localize-select';
$pg = $page;
$jsfile = "{$page}.js";
require("header.php");

/* check to see if localization directories are writeable */
$perms = check_perms(TRUE);
if (in_array(FALSE, $perms)) {
    $html .= "    <div class=\"container\">
    <p style=\"margin-top: 1em; \">{$LANGUAGE['localize']['perm_error_text']}</p>
    <ul>\n";

    /* list directories that need to be writeable */
    if (isset($perms['loc_front']) && !$perms['loc_front']) {
        $html .= "            <li><code>player/localization/</code> {$LANGUAGE['localize']['perm_error_dir_text']}</li>\n";
    }
    if (isset($perms['loc_set']) && !$perms['loc_set']) {
        $html .= "            <li><code>player/streamconfig/localization/</code> directory must be writeable to manage localizations.</li>\n";
    }
    if (isset($perms['docs']) && !$perms['docs']) {
        $html .= "            <li><code>player/docs/localization/</code> directory must be writeable to manage localizations.</li>\n";
    }
    $html .= "      </ul>\n";
    $html .= "<p>" . str_replace('[','<a href="../docs/">', str_replace(']','</a>', $LANGUAGE['localize']['perm_error_docs_ref'])) . "</p>\n";
    $html .= "</div>";

} else {
    /* screen resolution warning */
    $html .= "<div id=\"low_res\" class=\"container text-center\">
    <div class=\"alert alert-danger\">
        <i class=\"fas fa-times\"></i> {$LANGUAGE['localize']['screen_too_small']}
    </div>
    </div>
    <div id=\"loc_iface\">\n";

    /* post to yourself */
    $html .= "<form id=\"localize_nav\" method=\"post\">
    </form>";

    if (isset($input['target-lang'])) {
        include("localize_edit.php");
    } else {
        include("localize_select.php");
    }

    $html .="</div> <!-- end loc_iface -->";
}
$stick_footer_to_bottom = TRUE;
require("footer.php");

print($html);
?>
