<?php
/**
 * SolaShout Player
 * Back End Functions
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* this function is native in some versions of PHP */
if (!function_exists('array_key_last')) {
    /**
     * array_key_last: Returns the last key of an array or FALSE if array is empty or non-array
     * @since   0.7
     * @var     array   Array the last key of which to return
     */
    function array_key_last($a) {
        if(!is_array($a) || count($a) < 1) { return FALSE; }
        $out = '';
        foreach($a as $k => $i) { $out = $k; }
        return $out;
    }
}
/**
 * buildSettings: Builds the string to place inside setting.php
 *
 * @since   0.7
 * @var     array  $_POST array contents
 */
function buildSettings($d) {
    /* convert streams to list */
    $streams = json_decode($d['stream_list'],TRUE);
    $active_stream = 0;

    /* write out header */
    $out = "<?php
/* Settings for SolaShout player
 * Generated " . date('c') . " by ";
    switch($d['action']) {
        case "settings":
            $out .= 'edit_settings.php';
            break;
        case "wizard":
            $out .= 'wizard.php';
    }
     $out .= "\n */
\$settings = array(\n";


    /* write data */
    $out .= "    'url' => '" . $d['url'] . "',\n";
    $out .= "    'port' => '" . sanitize($d['port']) . "',\n";
    $out .= "    'all_on_one' => ";
    $out .= (isset($d['all_on_one'])) ? "1,\n" : "0,\n";
    $out .= "    'initial_stream' => {$d['initial_stream']},\n";
    $out .= "    'autoplay' => ";
    $out .= (isset($d['autoplay'])) ? "1,\n" : "0,\n";
    $out .= "    'streams' => array(\n";
    /* write out streams */
    foreach ($streams as $s) {
        $active_stream++;
        $out .= "        {$active_stream} => array(
            'name' => '" . sanitize($d["s{$s}_name"]) . "',
            'sid' => '" . sanitize($d["s{$s}_sid"]) . "',
            'url' => '";
        $out .= (isset($d['all_on_one'])) ? $d['url'] . ":" . $d['port'] : $d["s{$s}_url"];
        $out .= "',
            'path' => ";

        if ($d["s{$s}_path"] == '/') {
            $out .= "',',\n";
        } elseif (strpos($d["s{$s}_path"], "/") !== 0 ) {
            $out .= "'/" . sanitize($d["s{$s}_path"]) . "',\n";
        } else {
            $out .= "'/" . sanitize(substr($d["s{$s}_path"],1)) . "',\n";
        }
        $out .= "        ),\n";
    }
    $out .= "    ),
    'languages' => array(\n        '";
    $out .= str_replace(',',"','",substr($d['all_langs'],0,-1));
    $out .= "',\n    ),
    'default_language' => '{$d['default_language']}',
    'theme' => '{$d['theme']}',
    'user_change_theme' => ";
    $out .= (isset($d['user_change_theme'])) ? "1,\n" : "0,\n";
    $out .= "    'user_set_language' => ";
    $out .= (isset($d['user_set_language'])) ? "1,\n" : "0,\n";
    $out .= "    'adblock_warning' => ";
    $out .= (isset($d['adblock_warning'])) ? "1,\n" : "0,\n";
    $out .= "    'pwa' => ";
    $out .= (isset($d['pwa'])) ? "1,\n" : "0,\n";
    $out .= "    'pwa_title' => '" . $d['pwa_title'] . "',
    'access' => array(
        'identity' => '" . $d['identity'] . "',
        'challenge' => '" . $d['confirmed_pwd'] . "',
    ),\n";


    /* write out end */
    $out .=  ");
?>\n";

    /* return result */
    return $out;
}

/**
 * confirmPwd: confirms the password
 * Returns new password if old password is correct; old password if nothing is there
 *
 * @since   0.7
 * @var     string old password
 * @var     array  $_POST array contents
 */
function confirmPwd($p, $d) {
    if ($p != '') {
        if ($d['newpwdinit'] != '') {
            if (password_verify($d['oldpwd'], $p)) {
                return password_hash($d['newpwdinit'], PASSWORD_DEFAULT);
            } else {
                return 'old_is_wrong';
            }
        } else {
            return $p;
        }
    } else {
        return '';
    }
}

/**
 * getStreamStats: Pulls the stream statistics from the stats file
 * Returns: array containing stream numeric id and current song and up status 'up' or 'down'
 *
 * @since 0.1
 * @var     array   stats file XML-parsed array item
 * @var     string  either stream number or path
 */
function getStreamStats($stats,$id=1) {
    $found = "";
    $onestream = FALSE;
    if ($stats['TOTALSTREAMS'] == 1) {
        $id = 1;
        $onestream = TRUE;
    }

    if (is_numeric($id)) {
        $streamid = $id - 1;
        $path = "";
    } else {
        $streamid = -1;
        $path = $id;
        if (substr($path,0,1) != "/") { $path = "/{$path}"; }
    }

    if ($streamid < 0) {
        foreach ($stats['STREAMS'] as $key => $item) {
            if (in_array($path, $item)) {
                $found = $item;
                $streamid = $key + 1;
                break;
            }
        }
    } elseif ($onestream) {
        $found = $stats['STREAMS'][0];
        $streamid++;
    } else {
        $found = $stats['STREAMS'][$streamid];
        $streamid++;
    }
    if ($found == "") {
        $out = array('sid'=>0,'song'=>'','status'=>'down');
    } else {
        if ($found['STREAMSTATUS'] == 1) {
            $out = array('sid'=>$streamid,'song'=>$found['SONGTITLE'],'status'=>'up');
        } else {
            $out = array('sid'=>$streamid,'song'=>'','status'=>'down');
        }
    }
    return($out);
}

/**
 * getHistory: Gets and parses the history page for the stream
 * Returns: array containing history data; or blank if nothing returned
 *
 * @since   0.1
 * @var     string  composite URL (with port)
 * @var     integer stream ID whose history to query (defaults to 1)
 */
function getHistory($url,$sid=1) {
    global $settings; /* connect to the global settings variable */

    $out = array();

    if ($hist_html = @file_get_contents("{$url}/played.html?sid={$sid}")) {
        $hist_html = substr($hist_html,strrpos($hist_html,'<table'));
        $a = explode("<td>",$hist_html);
        $i = 6;
        while($i <= count($a)) {
            $out[] = substr($a[$i],0,strpos($a[$i],"</"));
            $i = $i+2;
        }
        return $out;
    } else {
        return array();
    }
}

/**
 * mkdnArray: converts strings in an array with Markdown code to HTML
 * Returns: an array containing the corrected strings
 *
 * @since    0.11
 * @var     array   array with strings to modify
 */
function mkdnArray($a) {
    /* output array */
    $pretty = array();

    /* instantiate parsedown module */
    $mkdn = new Parsedown();
    $mkdn->setUrlsLinked(false);

    foreach ($a as $k => $i) {
        if (is_array($i)) {
            $pretty[$k] = mkdnArray($i);
        } else {
            $pretty[$k] = $mkdn->line($i);
            if (strpos($pretty[$k], '<code>||</code>') === FALSE) {
                $pretty[$k] = str_replace('||', '<br>', $pretty[$k]);
            }
        }
    }
    return $pretty;
}

/**
 * randomId: generates an alphanumeric random ID of a specified length
 * Returns: a string containing alphanumeric random ID
 *
 * @since    0.8
 * @var     int     how many characters the random Id should be in length; defaults to 8
 * @var     string  "any","decimal","alphabet","hexadecimal"; defaults to any
 */
function randomId($length=8,$type='any') {
	$seed = array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G",
	              "H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X",
				  "Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o",
				  "p","q","r","s","t","u","v","w","x","y","z");
	$id = "";
	$start = 0; $end = count($seed)-1;
	if ($type == 'decimal') {
		$end = 9;
	} elseif ($type == 'hexadecimal') {
		$end = 15;
	} elseif ($type == 'alphabet') {
		$start = 10;
	}
	for ($i=0;$i<$length;$i++) {
		$id .= $seed[rand($start,$end)];
	}
	return $id;
}

/**
 * resetChallenge: resets the password (challenge) in the $settings array
 * Returns: string to write to settings file
 *
 * @since   0.8
 * @var     array   settings array
 * @var     string  new password (challenge)
 * @var     boolean whether or not to force password reset on next login; default TRUE
 */
function resetChallenge($s, $pwd, $force_reset=TRUE) {
    $s['access']['challenge'] = password_hash($pwd, PASSWORD_DEFAULT);
    if ($force_reset) {
        $s['access']['force_reset'] = 1;
    } else {
        if (isset($s['access']['force_reset'])) {
            unset($s['access']['force_reset']);
        }
    }
    $out = "<?php
/* Settings for Shoutcast Multi-stream player
 * Generated " . date('c') . " by ";
    if (!$force_reset) {
        $out .= 'edit_settings.php';
    } else {
        $out .= 'login.php';
    }
     $out .= "\n */\n";
     $out .= "\$settings = array(\n";

     /* iterate through passed $settings array and convert to string
      * note that each key and value is checked as to type. If integer
      * or double, then it does not receive quotes. Arrays can be iterated through
      * up to the second level. Otherwise value ($i, $si, $ssi) is considered
      * a string.
      */
     foreach($s as $k => $i) {
         if (gettype($k) == 'integer' || gettype($k) == 'double') {
            $out .= "    {$k} => ";
         } else {
             $out .= "    '{$k}' => ";
         }
         if (is_array($i)) {
             $out .= "array(\n";
             foreach ($i as $sk => $si) {
                 if (gettype($sk) == 'integer' || gettype($sk) == 'double') {
                     $out .= "        {$sk} => ";
                 } else {
                     $out .= "        '{$sk}' => ";
                 }
                 if (is_array($si)) {
                     $out .= "array(\n";
                     foreach ($si as $ssk => $ssi) {
                         if (gettype($ssk) == 'integer' || gettype($ssk) == 'double') {
                             $out .= "            {$ssk} => ";
                        } else {
                            $out .= "            '{$ssk}' => ";
                        }
                        if (gettype($ssi) == 'integer' || gettype($ssi) == 'double') {
                            $out .= "{$ssi},\n";
                        } else {
                            $out .= "'{$ssi}',\n";
                        }
                     }
                     $out .= "        ),\n";
                 } else {
                     if (gettype($si) == 'integer' || gettype($si) == 'double') {
                         $out .= " {$si},\n";
                     } else {
                         $out .= " '{$si}',\n";
                     }
                 }
             }
             $out .= "    ),\n";
         } else {
             if (gettype($i) == 'integer' || gettype($i) == 'double') {
                 $out .= " {$i},\n";
             } else {
                 $out .= " '{$i}',\n";
            }
         }
     }

     $out .= ");
?>";
    return $out;
}

/**
 * sanitize: sanitizes the strings to prevent code from being passed
 * Returns: sanitized string
 *
 * @since   0.9
 * @var     string  text to sanitize
 */
function sanitize($str) {
    $out = htmlentities(strip_tags($str),ENT_COMPAT | ENT_HTML5, 'UTF-8');
    $out = str_replace("'", "\\'", $out);
    return $out;
}

/**
 * sendReset: sends e-mail to reset the password
 * Returns: boolean denoting success
 *
 * @since   0.8
 * @var     string  email address
 * @var     string  temporary password
 */
function sendReset($email, $pwd) {
    /* get localization */
    if (!isset($LANGUAGE)) {
        $lang = (isset($_COOKIE['lang'])) ? $_COOKIE['lang'] :'en';
        include("./localization/{$lang}.php");
    }

    /* set e-mail headers */
    $headers = "From: streamplayer@{$_SERVER['HTTP_HOST']}";

    /* compose e-mail */
    $msg = str_replace('%newcode%', $pwd, $LANGUAGE['pwd_reset']['email_msg']);

    $result = mail($email, $LANGUAGE['pwd_reset']['subject'], $msg, $headers);

    return $result;
}

/**
 * validateScStream: checks to see if the stream at a given URL is a valid Shoutcast v2 stream
 * Returns: boolean containing response; or object containing stream list
 *
 * @since   0.7
 * @var     string  String containing a valid url
 * @var     boolean Whether or not to return list of streams available
 */
 function validateScStream($stream_url, $return_streams=FALSE) {
    if (!ctype_digit(substr($stream_url,-1))) { return false; }
    $stream = scXmlToArray("{$stream_url}/statistics");
    $result = FALSE;
    if (count($stream) > 0) {
        $v = explode('.',$stream['VERSION']);
        if ($v[0] == '2') { $result = TRUE; }
        if ($result && $return_streams) {
                $result = array();
                $streams = $stream['STREAMS'];
                foreach ($streams as $s)  {
                    $result[] = array('sid' => $s['STREAMID'],
                                  'name' => $s['SERVERTITLE'],
                                  'path' => $s['STREAMPATH'],
                                  'status' => $s['STREAMSTATUS'],);
                }
            }
        }
    return $result;
}


/**
 * writeFile: writes data to a file
 * Returns: string containing response
 *
 * @since   0.7
 * @var     string  File name to write file to
 * @var     string  String containing data to be written
 */
function writeFile($f, $s) {
    if ($f == '' || $s == '') { return false; } // do not work with empty strings
     $r = '';
    if($fo = @fopen($f, 'w')) {
        if (fwrite($fo, $s) === FALSE) {
            $r = "error=no_write";
        } else {
            $r = "success=true";
        }
        fclose($fo);
    } else {
        $r = "error=no_write";
    }
    return $r;
}
