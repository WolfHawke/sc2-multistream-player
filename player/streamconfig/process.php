<?php
/**
 * SolaShout Player Settings
 * Process settings and localization forms
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */
$dev = FALSE;

if (!$dev){
    /* only accept queries from the same domain */
    if (!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) == FALSE) { die(); }
    if (!isset($_POST['action'])) { header('Location: index.php'); }
}

require("functions.php");
require("../scxmlparser.php");
require("../config/settings.php");

/* send no-caching headers */
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Connection: close");

/* start session */
session_start();


/* global variable denoting response */
$r = '';

$input = $_POST;

if ($dev && !isset($_POST['action'])) { $input = $_GET; }

/* check for session content before continuing; only 'dologin' is exempt */
if ($input['action'] != 'dologin') {
    if (!isset($_COOKIE['spsess'])) {
        die();
    }
}


switch ($input['action']) {
    case 'check_sc_stream':
        $result = array('result'=>'invalid','sid' => 0);
        if (isset($input['sid'])) { $result['sid'] = $input['sid']; }
        if (validateScStream($input['url'])) {
            $result['result'] = 'valid';
        }
        print(json_encode($result));
        die();
        break;

    case 'dologin':
        $err = FALSE;
        $out = "{\"error\": [";
        if ($input['identity'] != $settings['access']['identity']) {
            $out .= '"identity",';
            $err = TRUE;
        }
        if (!password_verify($input['challenge'], $settings['access']['challenge'])) {
            $out .= '"challenge"';
            $err = TRUE;
        }
        $out .= "]}";
        if (!$err) {
            $spsess = randomId(32);
            setcookie('spsess', $spsess);
            $_SESSION['active'] = randomId(128);
            $out = "{\"success\":\"{$spsess}\", \"key\":\"{$_SESSION['active']}\"}";
        }
        print($out);
        break;

    case 'dologout':
        session_destroy();
        /* clear the session cookie, because Chrome won't */
        setcookie('PHPSESSID', FALSE, 0, '/');
        setcookie('spsess', FALSE, 0, '/');
        if (isset($input['nosuccess'])) {
            header("Location: ../streamconfig/");
        } else {
            header("Location: ../streamconfig/?logout=true");
        }
        break;

    case 'do_pwd_reset':
        if (isset($input['identity']) && $input['identity'] == $settings['access']['identity']) {
            /* reset password with random alphanumeric string */
            $new_pwd = randomId(12);
            if (sendReset($settings['access']['identity'], $new_pwd)) {
                if (writeFile('../config/settings.php', resetChallenge($settings, $new_pwd, TRUE)) == "success=true") {
                    print('{"success":true}');
                } else {
                    print('{"error":"no_write"}');
                }
            } else {
                print('{"error":"email_failed"}');
            }
        } elseif (isset($input['newpwdforce'])) {
            if (isset($_SESSION['active']) && isset($_SESSION['validated']) && $_SESSION['active'] == $_SESSION['validated']){
                if (password_verify($input['oldpwd'], $settings['access']['challenge'])){
                    if (writeFile('../config/settings.php', resetChallenge($settings, $input['newpwdinit'], FALSE)) == 'success=true') {
                        print('{"success":true}');
                    } else {
                        print('{"error":"no_write"}');
                    }
                } else {
                    print('{"error":"bad_old"}');
                }
            } else {
                print('{"error":"not_logged_in"}');
            }
        } else {
            print('{"error":"wrong_id"}');
        }
        break;

    case 'settings':
        $f = '../config/settings.php';
        $epoch = time();
        /* if lockdown is set and the old password lies within 10 sec of current epoch */
        if (isset($input['lockdown']) && ($input['oldpwd'] >= ($epoch-10) && $input['oldpwd'] <= $epoch)) {
            $input['confirmed_pwd'] = password_hash($input['newpwdinit'], PASSWORD_DEFAULT);
            $r = writeFile('../config/lockdown.txt','configuration locked out ' . date('r'));
            if ($r == "error=no_write") {
              print(json_encode(array("result"=>"error")));
              die();
            }
        } else {
            $input['confirmed_pwd'] = confirmPwd($settings['access']['challenge'], $input);
            if ($input['confirmed_pwd'] == "old_is_wrong") {
                print(json_encode(array("error"=>"bad_pass")));
                break;
            }
        }
        $input['identity'] = $settings['access']['identity'];
        $r = writeFile($f, buildSettings($input));
        $r = explode('=',$r);
        print(json_encode(array($r[0]=>$r[1])));
        break;
}

?>
