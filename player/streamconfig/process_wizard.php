<?php
/**
 * SolaShout Player Settings
 * Process settings and localization forms
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */
 $dev = FALSE;

 if (!$dev){
     /* only accept queries from same domain */
     if (!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) == FALSE) { die(); }
     if (!isset($_POST['action'])) { header('Location: index.php'); }
 }

 require("functions.php");
 require("../scxmlparser.php");

 /* send no-caching headers */
 header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
 header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
 header("Cache-Control: post-check=0, pre-check=0", false);
 header("Pragma: no-cache");
 header("Connection: close");

 /* start session */
 session_start();

/* only run items if wizard is real */
if (!isset($_COOKIE['spsess']) || !isset($_COOKIE['spwiz'])) {
    die();
}

$input = $_POST;
if ($dev && !isset($_POST['action'])) { $input = $_GET; }


switch ($input['action']) {
    case 'check_sc_stream':
        $r = array('result' => 'invalid');
        if ($input['branch'] == 'single') {
            $q = validateScStream("{$input['url']}:{$input['port']}", TRUE);
            if (is_array($q)) {
                    $r['result'] = 'valid';
                    $r['streams'] = $q;
                }
        } elseif ($input['branch'] == 'multi') {
            if (validateScStream("{$input['url']}", FALSE)) {
                $r['result'] = 'valid';
            }
        }
        print (json_encode($r));
        break;
    case 'save_data':
        /* make sure we cancel the lockdown before saving anything */
        if (file_exists("../config/lockdown.txt")) { unlink("../config/lockdown.txt"); }
        if($input['newpwdinit'] == $input['newpwdconf']) {
            $input['confirmed_pwd'] = password_hash($input['newpwdinit'], PASSWORD_DEFAULT);
        } else {
        print(json_encode(array("result"=>"error")));
            die();
        }
        if ($input['lockdown'] == 1) {
            $r = writeFile('../config/lockdown.txt','configuration locked out ' . date('r'));
            if ($r == "error=no_write") {
              print(json_encode(array("result"=>"error")));
              die();
            }
        }
        $r = writeFile("../config/settings.php", buildSettings($input));
        if ($r == "error=no_write") {
            print(json_encode(array("result"=>"error")));
        } elseif ($r == "success=true") {
            print(json_encode(array("result"=>"success")));
        }
        break;
}

?>
