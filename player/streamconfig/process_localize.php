<?php
/**
 * SolaShout Player Localization Process Page
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */
 $dev = TRUE;

 if (!$dev){
     /* only accept queries from same domain */
     if (!isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) == FALSE) { die(); }
     if (!isset($_POST['action'])) { header('Location: index.php'); }
 }

 $input = $_POST;
 if ($dev && !isset($_POST['action'])) { $input = $_GET; }

/* includes */
require("functions.php");
require("two-letter-lang-codes.php");

/* send no-caching headers */
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Connection: close");

/* start session */
session_start();

/* functions */
/**
 * arrayToString: Converts an array to a series of strings in PHP array syntax
 *
 * @since   0.11
 * @var     array   array to convert
 * @var     integer defines level of recursion
 */
function arrayToString($arr, $recursion=0) {
    $out = "array(\n";
    foreach ($arr as $k => $i) {
        $out .= ($recursion > 0) ? str_pad(" ",($recursion * 4)) : "";
        $out .= "    '{$k}' => ";
        if (is_array($i)) {
            $out .= arrayToString($i, $recursion+1) ;
        } else {
            $out .= "'{$i}',\n";
        }
    }
    $out .=  ($recursion > 0) ? str_pad(" ",($recursion * 4)) . ")" : ")";
    $out .= ($recursion) ? ",\n":";\n";
    return $out;
}

/**
 * buildLocalization: Builds the string to place inside setting.php
 *
 * @since   0.11
 * @var     string language ISO name
 * @var     string area to write to
 * @var     array  array contents to add to localization
 */
function buildLocalization($iso_name,$area="",$data) {
    $out = "<?php
/**
 * SolaShout Player {$area}
 * Localization file=> {$iso_name}
 */

\$LANGUAGE = ";
    $out .= arrayToString($data);
    return $out;
}

/**
 * getArea: Returns array with path and name for the area that is queried
 *
 * @since   0.11
 * @var     string  area code ('back', 'doc', 'front')
 * @var     string  language code
 */
function getArea($a, $l) {
    $out = array();
    switch ($a) {
        case "front":
            $out['path'] = "../localization/{$l}.php";
            $out['area_name'] = "";
            break;

        case "back":
            $out['path'] = "./localization/{$l}.php";
            $out['area_name'] = "Settings";
            break;

        case "docs":
            $out['path'] = "../docs/localization/{$l}.php";
            $out['area_name'] = "Documentation";
            break;
    }
    return $out;
}

/**
 * makeKeyArray: creates an array of keys with dot notation
 *
 * @since 0.11
 * @var   array   array to flatten
 * @var   string  upper level key
 */
function makeKeyArray($a, $lvl='') {
    $out = array();
    if ($lvl != '') {
        $lvl = $lvl . '-';
    }
    foreach ($a as $k => $i) {
        if (is_array($i)) {
            $out[$k] = makeKeyArray($i, $lvl . $k);
            $out[$k]['self'] = $lvl . $k;
        } else {
            $out[$k] = $lvl . $k;
        }
    }
    return $out;
}

/**
 * mergeArraysToJS: merges the language arrays to a single JSON object
 *
 * @since 0.11
 * @var   array   master language (en)
 * @var   array   string descriptors
 * @var   string  target language code (e.g. 'tr')
 * @var   array   target language
 * @var   array   flattened keycodes
 */
function mergeArraysToJS ($master, $desc, $target_code, $target, $keys) {
    $out = array();

    foreach ($master as $k => $i) {
        if (is_array($i)) {
            $out[$keys[$k]['self']] = array(
                'description' => $desc[$k]['description'],
                'group' => 'true'
            );
            /* make sure we don't throw an error if this group has not been created in the target langauge */
            if (!isset($target[$k])) {
                $target[$k] = array();
            }
            $out = array_merge($out, mergeArraysToJS($i, $desc[$k], $target_code, $target[$k], $keys[$k]));
        } else {
            $out[$keys[$k]] = array(
                'description' => $desc[$k],
                'en' => $i,
                $target_code => ''
            );
            if (isset($target[$k])) {
                $out[$keys[$k]][$target_code] = $target[$k];
            }
        }
    }
    return $out;
}

/**
 * reDimItem: takes a single-key item and turns it into a multidimensional array
 *
 * @since     0.11
 * @var     string  flattened key
 * @var     string  value
 */
function reDimItem($flat_key, $val) {
    $out = array();
    $key_array = explode('-', $flat_key);

    for ($i = (count($key_array)-1); $i >= 0; $i--) {
        if ($i == count($key_array)-1) {
            $out = array($key_array[$i] => $val);
        } else {
            $out = array($key_array[$i] => $out);
        }
    }
    return $out;
}

/* process command */
$r = '';

switch ($input['action']) {
    case "add_lang":
        $a = getArea($input['area'], $input['lang']);
        $r = writeFile($a['path'], buildLocalization($lang_codes[$input['lang']]['iso'], $a['area_name'], array('languageCode' => $input['lang'], 'languageName' => $lang_codes[$input['lang']]['native'], 'languageIsoName' => $lang_codes[$input['lang']]['iso'], 'languageDirection' => $lang_codes[$input['lang']]['dir'])));
        break;

    case "del_lang":
        if (isset($input['area']) && isset($input['lang'])) {
            $a = getArea($input['area'], $input['lang']);
            if (unlink($a['path'])) {
                $r = "success=true";
            } else {
                $r = "error=true";
            }
        } else {
            $r = "error=true";
        }
        break;

    case "load_lang":
        /* loads a given language to return as JSON object */
        /* load language files */
        include(getArea($input['area'], 'en')['path']);
        $master_lang = $LANGUAGE;
        include("./localization/{$input['area']}-stringdesc.php");
        include(getArea($input['area'], $input['target_lang'])['path']);
        print(json_encode(mergeArraysToJS($master_lang, $descriptions, $input['target_lang'], $LANGUAGE, makeKeyArray($master_lang))));
        break;

    case "update_lang":
        if (isset($input['strings'])) {
            $a = getArea($input['area'], $input['lang']);
            include($a['path']);
            foreach ($input['strings'] as $k => $s) {
                $LANGUAGE = array_replace_recursive($LANGUAGE, reDimItem($k, $s));
            }
            $r = writeFile($a['path'], buildLocalization($lang_codes[$input['lang']]['iso'], $a['area_name'], $LANGUAGE));
        } else {
            $r = "success=true";
        }
        break;


}
if ($r != '') {
    $r = explode('=',$r);
    $r = json_encode(array($r[0]=>$r[1]));
    print($r);
}
?>
