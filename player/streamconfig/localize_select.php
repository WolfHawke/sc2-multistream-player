<?php
/**
 * SolaShout Player Localization Select Language Page
 * (called by index.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* get Languages */
switch($input['localize']) {
    case 'front':
        $langs = $front_langs;
        $loc = $LANGUAGE['nav_localize_player'];
        break;

    case 'back':
        $langs = $back_langs;
        $loc = $LANGUAGE['nav_localize_settings'];
        break;

    case 'docs':
        $langs = $doc_langs;
        $loc = $LANGUAGE['nav_localize_documentation'];
        break;
}

/* get language list */
include('two-letter-lang-codes.php');

/* sort language codes */
asort($LANGUAGE['languages']);

/* add language modal */
$html .= "
<!-- Select new language modal -->
<div class=\"modal\" id=\"select_new_lang\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <h5><i class=\"fas fa-plus\"></i> {$LANGUAGE['localize']['add_lang_but']}</h5>
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            <div class=\"modal-body\">
            <p>{$LANGUAGE['localize']['select_lang']}</p>
            <input type=\"hidden\" name=\"selected_lang\" id=\"selected_lang\" value=\"\">
            <input type=\"hidden\" name=\"area\" id=\"area\" value=\"{$_GET['localize']}\">
            <div id=\"error_msg_create\" class=\"alert alert-danger\"><i class=\"fas fa-times\"></i> {$LANGUAGE['localize']['err_msg_create']}</div>
            <div id=\"error_msg_delete\" class=\"alert alert-danger\"><i class=\"fas fa-times\"></i> {$LANGUAGE['localize']['err_msg_delete']}</div>
            <div id=\"langs_list\">
                <ul class=\"list-group\">\n";

foreach ($LANGUAGE['languages'] as $lid => $ln) {
    if (!array_key_exists($lid, $langs)) {
        $html .= "                    <li class=\"list-group-item sel-lang-item\" id=\"select_lang_{$lid}\" onclick=\"localizeObj.selectLang('{$lid}')\">{$ln}";
        $html .= ($ln != $lang_codes[$lid]['native']) ? " : {$lang_codes[$lid]['native']}" : '';
        $html .= "</li>\n";
    }
}

$html .= "                </ul>
            </div>
            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" id=\"add_lang_close_button\"><i class=\"far fa-window-close\"></i> {$LANGUAGE['localize']['close_but']}</button>&nbsp;
                <button type=\"button\" class=\"btn btn-success\" onclick=\"localizeObj.addLang()\" id=\"add_lang_submit_button\">
                <span id=\"add_lang_button_txt\"><i class=\"far fa-plus-square\"></i> {$LANGUAGE['localize']['add_lang_but']}</span>
                <span id=\"add_lang_button_spinner\">
                  <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                </span>
            </div>
        </div>
    </div>
</div>
";

/* deletion confirmation modal */
$html .= "
    <!-- Modal for delete confirmation -->
    <div class=\"modal fade\" id=\"lang_del\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"staticBackdropLabel\">";
$html .= str_replace('%area%', $loc, $LANGUAGE['localize']['del_dialog_title']);
$html .= "</h5>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">
            <input type=\"hidden\" id=\"dellang\" name=\"dellang\" value=\"0\">
            <p id=\"del_dialog_confirm_msg\">";
$LANGUAGE['localize']['del_dialog_confirm_msg'] = str_replace('||', '<br>', str_replace('%area%', $loc, $LANGUAGE['localize']['del_dialog_confirm_msg']));

$html .= "{$LANGUAGE['localize']['del_dialog_confirm_msg']}</p>
          </div>
          <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-primary\" onclick=\"localizeObj.delLang(0, true, '{$input['localize']}');\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['yes']}</button>
            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['no']}</button>
          </div>
        </div>
      </div>
    </div>";



$html .= "
<div class=\"container-fluid\">
  <div class=\"row top\">
  <div class=\"col-1\">&nbsp;</div>
  <div class=\"col-10\">
  <!-- JavaScript warning -->
  <noscript>
   <div class=\"alert alert-danger\"><i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['no_javascript_error']}</div>
  </noscript>

    <h1><i class=\"fas fa-language\"></i> {$LANGUAGE['localize']['title']}</h1>\n";

/* tabs */
$html .= "    <ul class=\"nav nav-tabs\">
            <li class=\"nav-item\">
                <a class=\"nav-link";
$html .= ($input['localize'] == 'front') ? ' active' : '';
$html .= "\" href=\"./?localize=front\"><i class=\"fas fa-play-circle\"></i> {$LANGUAGE['nav_localize_player']}</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link";
$html .= ($input['localize'] == 'back') ? ' active' : '';
$html .= "\" href=\"./?localize=back\"><i class=\"fas fa-wrench\"></i> {$LANGUAGE['nav_localize_settings']}</a>
            </li>
            <li class=\"nav-item\">
                <a class=\"nav-link";
$html .= ($input['localize'] == 'docs') ? ' active' : '';
$html .= "\" href=\"./?localize=docs\"><i class=\"fas fa-file-alt\"></i> {$LANGUAGE['nav_localize_documentation']}</a>
            </li>
        </ul>
  ";

/* add language button */
$html .= "<div id=\"add_lang_but\"><button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#select_new_lang\"><i class=\"fas fa-plus\"></i> {$LANGUAGE['localize']['add_lang_but']}</button></div>
<div id=\"container-fluid\">\n";

/* Iterate through languages */
$firstrow = TRUE;
foreach ($langs as $lid => $lname) {
    $html .= "<div class=\"row rowspace";
    if ($firstrow) {
        $html .= " rowfirst";
        $firstrow = FALSE;
    }
    $html .= "\">
        <div id=\"lang_{$lid}\" class=\"col langname\">{$lname}</div>
        <div class=\"col-3\"><button class=\"btn btn-outline-secondary\" title=\"{$LANGUAGE['localize']['view_lang_but']}\" onclick=\"localizeObj.loadLang('{$lid}','view'); return false;\"><i class=\"far fa-eye\"></i></button>\n";

    /* editing etc only possible for languages *other* than English */
    if ($lid != 'en') {
        $html .= "            <button class=\"btn btn-outline-secondary\" title=\"{$LANGUAGE['localize']['edit_lang_but']}\" onclick=\"localizeObj.loadLang('{$lid}','edit'); return false;\"><i class=\"fas fa-pencil-alt\"></i></button>
            <button class=\"btn btn-outline-secondary\" title=\"{$LANGUAGE['localize']['del_lang_but']}\" onclick=\"localizeObj.delLang('{$lid}'); return false;\"><i class=\"far fa-trash-alt\"></i></button>\n";
    }
    $html .="        </div>
    </div>";
}



/* close columns */
$html .= "  </div>
</div>
<div class=\"col-1\">&nbsp;</div>
</div>
</div>";


?>
