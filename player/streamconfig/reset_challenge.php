<?php
/**
 * SolaShout Player Settings
 * Reset password form (called by index.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* force everything to run through index.php */
if (strpos($_SERVER['REQUEST_URI'],basename(__FILE__)) !== FALSE) {
    header("Location: ../streamconfig/");
}

 $html = "<!DOCTYPE html>
 <html>
 <head>
 <html lang=\"{$setlang}\">

 <head>
   <meta charset=\"utf-8\">
   <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
   <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
   <meta http-equiv=\"expires\" content=\"0\" />
   <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
   <meta http-equiv=\"pragma\" content=\"no-cache\" />
   <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
   <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
   <title>{$LANGUAGE['title']}</title>
   <!-- Font Awesome -->
   <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
   <!-- Bootstrap core CSS -->
   <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
   <!-- FontAwesome Checkbox CSS | © 2014 maxweldsouza. Used by Permission -->
   <link href=\"./css/fac.css\" rel=\"stylesheet\">
   <!-- Your custom styles (optional) -->
   <link href=\"./css/login.css?v=" . time() . "\" rel=\"stylesheet\">
   <!-- Favicon -->
   <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
 </head>
 <body>
     <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
         <a class=\"navbar-brand\" href=\"../streamconfig/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"{$LANGUAGE['title']}\">&nbsp;{$LANGUAGE['title']}</a>
         <div class=\"navbar-nav mr-auto\">
             <div class=\"nav-item\">
                <a class=\"nav-link\" href=\"../\">{$LANGUAGE['nav_player']}</a>
             </div>
         </div>
         <div class=\"logout-md ml-auto\"><button class=\"btn btn-primary\" title=\"{$LANGUAGE['log_out']}\" onclick=\"logout(); return false;\">{$LANGUAGE['log_out']} <i class=\"fas fa-sign-out-alt\"></i></button></div>
     </nav>
     <div class=\"row\" style=\"margin-top: 40px;\">
         <div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>
         <div class=\"col-lg-4 col-md-6 col-sm-8 col-10\">
         <div id=\"pwd_container\" class=\"card\">
            <div id=\"pwd_form\" class=\"card-body\">
                <h5 class=\"card-title text-center\">{$LANGUAGE['pwd_reset']['title']}</h5>
                <p class=\"card-text\">{$LANGUAGE['pwd_reset']['reset_blurb']}</p>
                <div class=\"form-group\">
                <!-- Old Password -->
                <div class=\"form-item\">
                    <label for=\"oldpwd\">{$LANGUAGE['pwd_reset']['pwd_old']}</label>
                    <input type=\"password\" id=\"oldpwd\" name=\"oldpwd\" value=\"\" class=\"form-control field-check\">
                    <div class=\"invalid-feedback\" id=\"password_error_old\">{$LANGUAGE['pwd_reset']['pwd_nomatch']}</div>
                </div>

                <!-- New Password -->
                <div class=\"form-item\"><label for=\"newpwdinit\">{$LANGUAGE['pwd_new']}</label>
                      <input type=\"password\" id=\"newpwdinit\" name=\"newpwdinit\" value=\"\" class=\"form-control field-check\" aria-describedby=\"passwordHelpBlock\">
                      <small id=\"passwordHelpBlock\" class=\"form-text text-muted\">{$LANGUAGE['pwd_new_blurb']}</small>
                      <div class=\"invalid-feedback\" id=\"password_error_incorrect\">{$LANGUAGE['validation_text']['pwd_missing_chars']}
                      </div>
                </div>

                <!-- Confirm new password -->
                <div class=\"form-item\"><label for=\"newpwdconf\">{$LANGUAGE['pwd_conf']}</label>
                    <input type=\"password\" id=\"newpwdconf\" name=\"newpwdconf\" value=\"\" class=\"form-control field-check\">
                    <div class=\"invalid-feedback\" id=\"password_error_nomatch\">{$LANGUAGE['validation_text']['pwd_nomatch']}</div>
                </div>
                </div>
                <p class=\"card-text text-right\">
                    <button type=\"button\" class=\"btn btn-primary\" id=\"do_login\" onclick=\"resetPwd.processChallengeChange(); return false;\">
                        <span id=\"do_login_txt\">{$LANGUAGE['pwd_reset']['reset_btn']}</span>
                        <span id=\"do_login_spinner\">
                          <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                        </span>
                    </button>
                </p>
                </div>
                <div id=\"pwd_success\" class=\"card-body\">
                    <form id=\"logout-form\" name=\"logout-form\" action=\"process.php\" method=\"post\">
                    <input type=\"hidden\" name=\"action\" value=\"dologout\">
                    <input type=\"hidden\" name=\"nosuccess\" value=\"true\">
                    <h5 class=\"card-title text-center\">{$LANGUAGE['pwd_reset']['title']}</h5>
                    <p class=\"card-text\">{$LANGUAGE['pwd_reset']['success']}</p>
                    <p class=\"card-text text-right\"><button class=\"btn btn-success\">{$LANGUAGE['pwd_reset']['continue']} <i class=\"fas fa-sign-out-alt\"></i></button></p>
                    </form>
                </div>
            </div>
         </div>
         </div>
         <div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>
     </div>
";

$stick_footer_to_bottom = TRUE;
$jsfile = 'reset-pwd.js';
require("footer.php");

print($html);
?>
