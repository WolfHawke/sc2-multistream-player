/**
 * SolaShout Player Wizard JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/**
 * Object: StreamConfigWizard
 * Description: object to run the stream configuration wizard.
 * Arguments: selfRef = id used in the window object to refer to the wizard
 * Returns: nothing
 */
function StreamConfigWizard(selfRef) {

    /* object properties */
    this.selfRef = selfRef;
    this.currentStep = 1;
    this.nextStep = this.currentStep + 1;
    this.totalSteps = 15;
    this.branch = 'single'; //other option is 'multi'
    this.lockdown = false;
    this.streams = []; // array of stream objects
    this.streamsFound = []; // array of stream objects found
    this._addStreamState = false; // determines if we're adding a stream on step 5
    this._editingStream = -1; // id of stream being edited
    this._forbiddenChars = /['"\;\*:<>\?\\\|\+]+/g; // regex of characters that should not be allowed in text fields.

    /* object self-reference */
    var _me = this;

    /* object methods */
    /**
     * Function: addStream
     * Description: returns to step 5 to add another stream
     * Arguments: none
     * Returns: nothing
     */
    this.addStream = function() {
        _me._addStreamState = true;
        /* turn off back button */
        $("#back_btn").hide();

        /* reset fields */
        $("#s_multi_url").val("");
        $("#s_multi_name").val("");
        $("#s_multi_sid").val(1);
        $("#s_multi_path").val("");

        /* go to step five */
        _me.nextStep = 5;
        _me.gotoNextStep();
    }

     /**
      * Function: bookmarkUnlockCode
      * Description: bookmarks the unlock code to the browser
      * Arguments: none
      * Returns: nothing
      */
     this.bookmarkUnlockCode = function() {

     }

     /**
      * Function: close
      * Description: closes the wizard and returns you to the proper location
      * Arguments: none
      * Returns: nothing
      */
      this.close = function () {
          if (_me.lockdown) {
              window.location = "../";
          } else {
              window.location = "./";
          }
      }

     /**
      * Function: continueToStepSix
      * Description: moves wizard to step 6 if 5 ends with incomplete content
      * Arguments: none
      * Returns: nothing
      */
     this.continueToStepSix = function() {
         _me.nextStep = 6;
         _me.gotoNextStep();
     }

    /**
     * Function: delStream
     * Description: deletes a stream from the list
     * Arguments: sid = id of stream to delete
     *           confirm = boolean; true to continue
     * Returns: nothing
     */
    this.delStream = function (sid, confirm) {
        if (typeof confirm == "undefined") { confirm = false; }
        if (confirm == false) {
            $("#stream_del").modal({focus:true});
            $("#delsid").val(sid); /* set stream ID to delete */
        } else if (confirm == true) {
            /* execute deletion */
            /* display the add streams button if we are at 10 */
            if (_me.streams.length > 9) {
                $("add_stream").show();
            }

            /* get stream ID */
            if (sid < 0) { sid = $("#delsid").val(); }
            _me.streams.splice(sid);

            /* if we have only one left, renable the back button
               and put the data for this one back */
            if (_me.streams.length < 2) {
                $("#back_btn").show();
                $("#s_multi_url").val(_me.streams[0].url);
                $("#s_multi_name").val(_me.streams[0].name);
                $("#s_multi_sid").val(_me.streams[0].sid);
                $("#s_multi_path").val(_me.streams[0].path);
                _me._addStreamState = false;
            }

            _me.writeStreamListToSix();
        }
    }

    /**
     * Function: doStep
     * Description: executes the current step on the wizard
     * Arguments: direction = 'next' or 'prev'
     * Returns: nothing
     */
    this.doStep = function (direction) {
        if (typeof direction == "undefined") { return false; }
        /* function variables */
        var url = "";
        var port = 0;
        var cont = true;
        var data = {};
        var i = 0;
        var email = "";

        /* clear all errors and validations */
        $(".field-check").removeClass("is-invalid");
        $(".field-check").removeClass("is-valid");

        if (direction == 'prev') {
            _me.gotoPrevStep(); return true;
        } else if (direction == 'next') {
            switch(_me.currentStep) {
                case 1:
                    _me.nextStep = 2;
                    _me.gotoNextStep();
                    $("#back_btn").show();
                    break;
                case 2:
                    if ($("#all_on_one_yes").is(":checked")) {
                        _me.branch = "single";
                        _me.nextStep = 3;
                        _me.totalSteps = 15;
                    } else if ($("#all_on_one_no").is(":checked")) {
                        _me.branch = "multi";
                        _me.nextStep = 5;
                        _me.totalSteps = 14;
                    }
                    _me.gotoNextStep();
                    break;
                case 3:
                    _me.nextStep = 4;

                    /* clear errors */
                    $("#step_3_error_msg").hide();
                    /* test for correct URL and port */
                    url = $("#url").val();
                    port = $("#port").val();
                    if (!_me.validateURL(url)) {
                        $("#url").addClass("is-invalid");
                        cont = false;
                    }
                    if (!/[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535/.test(port)) {
                            $("#port").addClass("is-invalid");
                            cont = false;
                        }
                    if (cont) {
                        /* check the server to see if it is a shoutcast server */
                        _me.nextSpinner('on','next');
                        data = {"action":"check_sc_stream", "branch":"single", "url":url, "port":port}
                        $.post("process_wizard.php", data, "", "json"
                                ).done(function(d) {
                                    _me.nextSpinner('off','next');
                                    if (d.result == 'invalid') {
                                        $("#step_3_error_msg").show();
                                    } else if (d.result == 'valid') {
                                        _me.gotoNextStep();
                                        _me.streams = []; // reset the streams object in case we went back
                                        _me.writeSingleStreams(d.streams);
                                        $("#step_4_url").html($("#url").val() + ":" + $("#port").val());
                                    }
                                }).fail(function(d) {
                                    _me.nextSpinner('off','next');
                                    console.log("error", d);
                                });
                    }
                    break;
                case 4:
                    _me.nextStep = 6;
                    /* hide add stream button on step 6 */
                    $("#add_stream").hide();
                    /* empty streams object */
                    _me.streams = [];
                    /* write stream fields to the streams object */
                    for (var i=0; i < _me.streamsFound.length; i++) {
                        if ($("#s_" + i + "_active").is(":checked")) {
                            _me.streams[_me.streams.length] = {
                                "sid":_me.streamsFound[i].sid,
                                "name":$("#s_" + i + "_name").val(),
                                "path":_me.streamsFound[i].path,
                                "url":$("#url").val() + ":" + $("#port").val(),
                                };
                        }
                    }
                    _me.writeStreamListToSix();
                    _me.gotoNextStep();
                    break;
                case 5:
                    /* clear error message */
                    $("#step_5_error_msg").hide();
                    _me.nextStep = 6;
                    /* if any field is empty and we're adding a new stream display modal */
                    if (_me._addStreamState && _me.streams.length > 0) {
                        if ($("#s_multi_url").val().length < 1 || $("#s_multi_name").val().length < 1 ||
                            $("#s_multi_sid").val().length < 1 ||
                            $("#s_multi_path").val().length < 1) {
                                $("#continue_empty").modal({focus:true});
                                return;
                            }
                    }
                    /* validate fields */
                    if (!_me.validateURL($("#s_multi_url").val(), true)) {
                        $("#s_multi_url").addClass("is-invalid");
                        cont = false;
                    }
                    if ($("#s_multi_name").val().length < 1 || _me._forbiddenChars.test($("#s_multi_name").val())) {
                        $("#s_multi_name").addClass("is-invalid");
                        cont = false;
                    }
                    if ($("#s_multi_sid").val() < 1) {
                        $("#s_multi_sid").addClass("is-invalid");
                        cont = false;
                    }
                    if ($("#s_multi_path").val().indexOf("/") == -1 || $("#s_multi_path").val().length < 1 ||
                        (_me._forbiddenChars.test($("#s_multi_path").val()))) {
                        $("#s_multi_path").addClass("is-invalid");
                        cont = false;
                    }
                    if (cont) {
                        /* validate shoutcast stream server */
                        /* check the server to see if it is a shoutcast server */
                        _me.nextSpinner('on','next');
                        data = {"action":"check_sc_stream", "branch":"multi", "url":$("#s_multi_url").val()}
                        $.post("process_wizard.php", data, "", "json"
                                ).done(function(d) {
                                    _me.nextSpinner('off','next');
                                    if (d.result == 'invalid') {
                                        $("#step_5_error_msg").show();
                                    } else if (d.result == 'valid') {
                                        /* these two statements need to be in this order to make sure that correction of initial stream with back button works! */
                                        if (!_me._addStreamState) {
                                            _me.streams = [];
                                            /* make sure we fill in the url and port fields */
                                            var u = $("#s_multi_url").val().split(":");
                                            $("#url").val(u[0] + ":" + u[1]);
                                            $("#port").val(u[2]);
                                        } else {
                                            $("#back_btn").hide();
                                        }
                                        i = _me.streams.length
                                        if (_me._editingStream > -1) {
                                            i = _me.editingStream;
                                        }
                                         _me.streams[i] = {
                                            "url":$("#s_multi_url").val(),
                                            "name":$("#s_multi_name").val(),
                                            "sid":$("#s_multi_sid").val(),
                                            "path":$("#s_multi_path").val()
                                        };
                                        _me._editingStream = -1;
                                        /* show add stream button on step 6 */
                                        if (_me.streams.length < 10) {
                                            $("#add_stream").show();
                                        } else {
                                            $("#add_stream").hide();
                                        }
                                        _me.writeStreamListToSix();
                                        _me.gotoNextStep();
                                    }
                                }).fail(function(d) {
                                    _me.nextSpinner('off','next');
                                    console.log("error",d);
                                });
                    }
                    break;
                case 6:
                    _me.nextStep = 7;
                    $("#back_btn").show();
                    _me.gotoNextStep();
                    break;
                case 7:
                    _me.nextStep = 8;
                    _me.gotoNextStep();
                    break;
                case 8:
                    _me.nextStep = 9;
                    _me.gotoNextStep();
                    break;
                case 9:
                    _me.nextStep = 10;
                    _me.gotoNextStep();
                    break;
                case 10:
                    /* only show 11 if we are on a secured site */
                    if (window.location.href.substr(0,5) == "https") {
                        _me.nextStep = 11;
                    } else {
                        _me.nextStep = 12;
                    }
                    _me.gotoNextStep();
                    break;
                case 11:
                    _me.nextStep = 12;
                    _me.gotoNextStep();
                    break;
                case 12:
                    if ($("#lockdown_yes").is(":checked")) {
                        _me.lockdown = true;
                        _me.nextStep = 13;
                    } else if ($("#lockdown_no").is(":checked")) {
                        _me.lockdown = false;
                        _me.nextStep = 14;
                    }
                    _me.gotoNextStep();
                    break;
                case 13:
                    _me.nextStep = 15;
                    /* validate e-mail */
                    email = $("#identity_lockdown").val();
                    if (email == "" || _me.validateEmail(email)) {
                        /* create form summary */
                        $("#summary").html(_me.genSummary());
                        _me.gotoNextStep();
                        $("#next_btn").hide();
                        $("#finish_btn").show();
                    }
                    break;
                case 14:
                    _me.nextStep = 15;
                    cont = true;
                    /* validate e-mail */
                    if (!_me.validateEmail($("#identity").val())) {
                        cont = false;
                        $("#identity").addClass("is-invalid");
                    }
                    /* validate password */
                    if (!_me.validatePassword($("#newpwdinit").val())) {
                        cont = false;
                        $("#newpwdinit").addClass("is-invalid");
                    }
                    /* do passwords match? */
                    if ($("#newpwdconf").val() != $("#newpwdinit").val()) {
                        cont = false;
                        $("#newpwdconf").addClass("is-invalid");
                    }
                    /* create form summary */
                    if (cont) {
                        $("#summary").html(_me.genSummary());
                        _me.gotoNextStep();
                        $("#next_btn").hide();
                        $("#finish_btn").show();
                    }
                    break;
                case 15:
                    _me.nextStep = 16;
                    if (_me.lockdown) {
                      $("#step_16_config_txt").hide();
                    } else {
                      $("#step_16_config_txt").show();
                    }
                    /* execute form submission */
                    _me.submitWizard();
                    break;
            }

        } else {
            return false;
        }
    }

    /**
     * Function: editStream
     * Description: loads the stream content into step 5 and displays it
     * Arguments: sid = stream id in _me.streams array
     * Returns: nothing
     */
    this.editStream = function (sid) {
        if (typeof sid == "undefined") { return false; }
        _me._editingStream = sid; /* set editing stream so it's saved properly */
        $("#s_multi_url").val(_me.streams[sid].url);
        $("#s_multi_name").val(_me.streams[sid].name);
        $("#s_multi_sid").val(_me.streams[sid].sid);
        $("#s_multi_path").val(_me.streams[sid].path);
        _me.nextStep = 5;
        _me.gotoNextStep();
    }

    /**
     * Function: genSummary
     * Description: generates the summary string to be shown on step 14
     * Arguments: none
     * Returns: string containing generated HTML code.
     */
    this.genSummary = function () {
        var i = 0;
        var html = "<ul>\n";
        /* single or multi server */
        html += "<li>" + window.language.wizard.sum_server[_me.branch] + "</li>"
        if (_me.branch == "single") {
            html = html.replace("%s%", "<i>" + $("#url").val() + ":" + $("#port").val() + "</i>");
        }
        /* list of streams */
        html += "<li>" + window.language.wizard.sum_streams + ":\n<ul>";
        if (_me.streams.length < 2) {
            html = html.replace("%s%",window.language.wizard.sum_stream_txt_sg);
        } else {
            html = html.replace("%s%",window.language.wizard.sum_stream_txt_pl);
        }
        for (i=0; i < _me.streams.length; i++) {
            html += "<li><i>" + _me.streams[i].name + " (";
            if (_me.branch == 'multi') {
                html += window.language.wizard.url_txt + ": " + _me.streams[i].url + " ; ";
            }
            html += window.language.wizard.sid_txt + ": " + _me.streams[i].sid + " ; ";
            html += window.language.wizard.path_txt + ": " + _me.streams[i].path + ")</i></li>\n";
         }
         html += "</ul>\n</li>\n";

         /* initial_stream */
         html += "<li>" + window.language.wizard.sum_init_stream + ": <em>";
         html += _me.streams[parseInt($("input[name='initial_stream']").val())-1].name;
         html += "</em></li>\n";

         /* Autoplay */
         html += "<li>" + window.language.wizard.sum_autoplay + ": <em>";
         html += ($("input[name='autoplay']").val() == "1") ? window.language.wizard.enabled : window.language.wizard.disabled;
         html += "</em></li>\n";

         /* Get Active language */
         html += "<li>" + window.language.wizard.sum_def_lang + ": <em>";
         html += $("label[for='default_language_" + $("input[name='default_language']").val() + "']").html();
         html += "</em></li>\n";

         /* Can user change the language? */
         html += "<li>" + window.language.wizard.sum_usr_lang + ": <em>";
         html += ($("input[name='user_set_language']").val() == "1") ? window.language.wizard.enabled : window.language.wizard.disabled;
         html += "</em></li>\n";

         /* Get active theme */
         html += "<li>" + window.language.wizard.sum_def_theme + ": <em>";
         html += $("label[for='theme_" + $("input[name='theme']").val() + "']").html();
         html += "</em></li>\n";

         /* Can user change the language? */
         html += "<li>" + window.language.wizard.sum_usr_theme + ": <em>";
         html += ($("input[name='user_change_theme']").val() == "1") ? window.language.wizard.enabled : window.language.disabled;
         html += "</em></li>\n";

         /* Adblock warning */
         html += "<li>" + window.language.wizard.sum_adblock + ": <em>";
         html += ($("input[name='adblock_warning']" == "1")) ? window.language.wizard.enabled : window.language.wizard.disabled;
         html += "</em></li>\n";

         /* Progressive Web App */
         if (window.location.href.substr(0,5) == "https") {
             html += "<li>" + window.language.wizard.sum_pwa + ": <em>";
             html += ($("#pwa_yes").is(":checked")) ? window.language.wizard.enabled : window.language.wizard.disabled;
             html += "</em></li>\n";
             if ($("#pwa_yes").is(":checked")) {
                 html += "<li>" + window.language.wizard.sum_pwa_title + ": <em>";
                 html += $("input[name='pwa_title']").val();
                 html += "</em></li>\n";
             }
         }

         /* Lockdown */
         html += "<li>" + window.language.wizard.sum_config + ": <em>";
         html += _me.lockdown ? window.language.wizard.locked : window.language.wizard.active
         html += "</em></li>\n";

         /* Identity (e-mail address) */
         if (_me.lockdown) {
             html += "<li>" + window.language.wizard.sum_recovery + ": <em>";
             html += ($("#identity_lockdown").val() != "") ? $("#identity_lockdown").val() : window.language.wizard.sum_recovery_not_set;
             html += "</em></li>\n";
         } else {
             html += "<li>" + window.language.wizard.sum_login + ": <em>";
             html += $("#identity").val();
             html += "</em></li>\n";
         }
         html += "</ul>";

         return html;
    }

    /**
     * Function: gotoNextStep
     * Description: moves the wizard forward to the next step
     * Arguments: none
     * Returns: nothing
     */
    this.gotoNextStep = function () {
        $(".step").removeClass("active");
        $("#step_" + _me.nextStep + "_title").addClass("active");
        $("#step_" + _me.nextStep + "_body").addClass("active");
        //$("#current_step").html(_me.nextStep + " / " + _me.totalSteps);
        _me.currentStep = _me.nextStep;
    }

    /**
     * Function: gotoPrevStep
     * Description: moves the wizard back to the previous step
     * Arguments: none
     * Returns: nothing
     */
    this.gotoPrevStep = function() {
        if (_me.currentStep == 5) {
            _me.nextStep = 2;
        } else if (_me.currentStep == 6) {
            if (_me.branch == "multi") {
                _me.nextStep = 5;
                _me._addStreamState = false;
            } else if (_me.branch == "single") {
                _me.nextStep = 4;
            }
        } else if (_me.currentStep == 7) {
            _me.nextStep = 6;
            if (_me._addStreamState) {
                $("#back_btn").hide();
            }
        } else if (_me.currentStep == 12 && (window.location.href.substr(0,5) != "https")) {
            _me.nextStep = 10;
        } else if (_me.currentStep == 14) {
            _me.nextStep = 12;
        } else if (_me.currentStep == 15){
            $("#finish_btn").hide();
            $("#next_btn").show();
            if (_me.lockdown) {
                _me.nextStep = 13;
            } else {
                _me.nextStep = 14;
            }
        } else {
            _me.nextStep = _me.currentStep - 1;
        }
        if (_me.nextStep == 1) {
            $("#back_btn").hide();
        }
        _me.gotoNextStep();
    }

    /**
     * Function: nextSpinner
     * Description: turns the next spinner on or off
     * Arguments: state = 'on' or 'off'
     *            btn = 'next' or 'finish'
     * Returns: nothing
     */
    this.nextSpinner = function (state, btn) {
        if (typeof state == "undefined") { return false; }
        if (typeof btn == "undefined") { btn = "next"; }

        switch (state) {
            case "on":
                $("#" + btn + "_btn_text").hide();
                $("#" + btn + "_btn_spinner").show();
                $("#back_btn").attr("disabled", true);
                break;

            case "off":
                $("#" + btn + "_btn_text").show();
                $("#" + btn + "_btn_spinner").hide();
                $("#back_btn").attr("disabled", false);
                break;
        }

    }

    /**
     * Function: setStream
     * Description: Checks to see if there is content in the Stream Name field
     *              and if not sets the focus onto it.
     * Arguments: sid = stream name ide
     * Returns: nothing
     */
    this.setStream = function (sid) {
        if ($("#s_" + sid + "_name").val() == "") {
            $("#s_" + sid + "_name").focus();
        }
    }

    /**
     * Function: setThumb
     * Description: sets the thumbnail on the theme select step 9
     * Arguments: theme = theme name
     * Returns: nothing
     */
    this.setThumb = function(theme) {
        if (typeof theme == "undefined") { return false; }
        var url = "./img/" + theme + "-scrnshot.png";
        $("#theme_thumbnail").css("background-image", "url(" + url + ")");
    }

    /**
     * Function: showHidePWATitleBox
     * Description: Shows or hides the field pwa_title on step 11
     * Arguments: todo = 'show' or 'hide'
     * Returns: nothing
     */
    this.showHidePWATitleBox = function(todo) {
        if (typeof todo == "undefined") { return false; }
        if (todo == "show"){
            $("#pwa_title_box").show(400, function () {
                $("#pwa_title_box").focus();
            });
        } else if (todo == "hide") {
            $("#pwa_title_box").hide(400);
        }
    }

    /**
     * Function: submitWizard
     * Description: Generates the data object and submits the wizard
     *              items via jQuery.post method
     * Arguments: none
     * Returns: nothing
     */
    this.submitWizard = function() {
        var data = {"action":"save_data"}; // object containing data to be sent
        var fieldNames = ["url", "port", "all_langs", "pwa_title"];
        var radioNames = ["all_on_one", "autoplay", "default_language", "user_set_language", "theme", "user_change_theme", "adblock_warning", "pwa", "initial_stream"];
        var streamList = "[";
        var i = 0; /* loop variable */
        var s = 1; /* i + 1 */

        /* hide error if it's there */
        $("#step_15_error_msg").hide();

        /* get unchanging data */
        for (i=0; i < fieldNames.length; i++) {
            data[fieldNames[i]] = $("input[name='" + fieldNames[i] +"']").val();
        }
        /* radio button data */
        for (i=0; i < radioNames.length; i++) {
          /* Items that are set to 0 should not be passed, because PHP script expects them to not be there */
          if ($("input[name='" + radioNames[i] +"']:checked").val() != "0") {
            data[radioNames[i]] = $("input[name='" + radioNames[i] +"']:checked").val();
          }
        }

        /* get data for stream items */
        var ssUrl = data["url"] + ":" + data["port"];
        for (i=0; i < _me.streams.length; i++) {
            s = i+1;

            data["s" + s + "_url"] = _me.branch == "single" ? ssUrl : _me.streams[i].url;
            data["s" + s + "_name"] = _me.streams[i].name;
            data["s" + s + "_path"] = _me.streams[i].path;
            data["s" + s + "_sid"] = _me.streams[i].sid;
            streamList = streamList + s +",";
        }
        data["stream_list"] = streamList.substr(0,streamList.length-1) + "]";
        /* get proper data according to lockdown */
        if (_me.lockdown) {
            /* identity */
            if ($("input[name='identity_lockdown']").val() == "") {
                data["identity"] = "none";
            } else {
                data["identity"] = $("input[name='identity_lockdown']").val();
            }
            data["newpwdinit"] = $("input[name='challenge_lockdown']").val();
            data["newpwdconf"] = $("input[name='challenge_lockdown']").val();
            data["lockdown"] = 1;
        } else {
            data["identity"] = $("input[name='identity']").val();
            data["newpwdinit"] = $("input[name='newpwdinit']").val();
            data["newpwdconf"] = $("input[name='newpwdconf']").val();;
            data["lockdown"] = 0;

        }
        console.log(data);
        /* execute ajax query */
        $.post("process_wizard.php",data,"","json").done(function(r) {
          if (r.result == "error") {
            $("#step_15_error_msg").show();
          } else if (r.result == "success") {
            console.log(r);
            $("#back_btn").hide();
            $("#finish_btn").hide();
            $("#close_btn").show();
            _me.gotoNextStep();
          }
        }).fail(function (r){
          console.log("error",r);
        });

    }


    /**
     * Function: validateEmail
     * Description: validates that an e-mail address is properly formed
     * Arguments: e = email address to validate
     * Returns: boolean denoting validity
     */
    this.validateEmail = function(e) {
        if (typeof e == "undefined") { return false; }
        var response = false;
        var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return regex.test(e);
    }

    /**
     * Function: validatePassword
     * Description: validates a given password to be at least 8 characters long, contain at least 1 digit
     * Arguments: p = password to test against
     * Returns: boolean denoting validity
     */
    this.validatePassword = function (p) {
        var result = true;
        if ((p.length < 8) || (!p.match(/[A-Z]/) || (!p.match(/[a-z]/)) || (!p.match(/[0-9]/)) || (p.match(/[\"\'\\]/)) )) {
            result = false;
        }
        return result;
    }

    /**
     * Function: validateURL
     * Description: validates a URL either alphanumeric or IP address
     * Arguments: u = URL to validate
     *            hasPort = do we have a port appended?
     * Returns: boolean denoting validity
     */
    this.validateURL = function (u,hasPort) {
        if (typeof hasPort == "undefined") { hasPort = false; }
        if (typeof tld == "undefined") { tld = true; }
        /* patterns -- default is with prefix without port */
        var fqdn = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        /* test for ip pattern of xxx.xxx.xxx.xxx */
        var ip = /(http|https):\/\/[\d]+\.[\d]+\.[\d]+\.[\d]+/;
        /* test for ip address without a purt number */
        var ip_noPort = /((http|https):\/\/)?(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2}))\.){3}(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2})))$/;
        var ip_hasPort = /^((http|https):\/\/)(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$/g

        /* result variable */
        var result = false;

        /* run tests */
        /* check if we have pattern for ip address */
        if (ip.test(u)) {
            if (!hasPort) {
                result = ip_noPort.test(u);
            } else {
                result = ip_hasPort.test(u)
            }
            if (result && (u.split("/").length > 3)) { result = false; }
        } else {
            /* if not, make sure that uri is valid fqdn */
            result = fqdn.test(u);
            if (result && hasPort) {
                    var p = u.split(":");
                    p[2] = parseInt(p[2]);
                    if ((p.length != 3) || (!Number.isInteger(p[2])) || (u.split("/").length > 3)) { result = false; }
            } else if (result) {
                if ((u.split(":").length > 2) || (u.split("/").length > 3)) { result = false; }
            }
        }

        return result;
    }

    /**
     * Function: writeStreamListToSix
     * Description: writes the contents of _me.streams to the list on step 6
     * Arguments: none
     * Returns: nothing
     */
    this.writeStreamListToSix = function() {
        var html = "";
        var s = 0;
        $("#selected_streams_list").html("");
        for (var i=0; i < _me.streams.length; i++) {
            s = i+1;
            html += "<tr><td style=\"width: 2em;\">&nbsp;</td>";
            html += "<td class=\"fac fac-radio fac-primary sl-tab-space\"><span></span>";
            html += "<input type=\"radio\" id=\"rad_s" + s + "\" name=\"initial_stream\"";
            html += " value=\"" + s + "\""
            if (i == 0) { html+= " checked"}
            html +="><label for=\"rad_s" + s + "\">" + _me.streams[i].name + " (";
            html += window.language.wizard.sid_txt + ": " + _me.streams[i].sid + "; ";
            html += window.language.wizard.path_txt + ": " + _me.streams[i].path + ")";
            html += "</label></td>";
            if ((_me.branch == 'multi') && (_me.streams.length > 1)){
                html += "<td>";
                html += "<a href=\"#\" onclick=\"wizard.editStream(" + i + ")\">";
                html += "<i class=\"fas fa-edit\"></i></a>&nbsp;";
                html += "<a href=\"#\" onclick=\"wizard.delStream(" + i + ");\">";
                html += "<i class=\"fas fa-trash-alt\"></i></a>";
                html += "</td>";
            }
            html += "</tr>";
        }
        $("#selected_streams_list").html(html);
    }

    /**
     * Function: writeSingleStreams
     * Description: writes the list of single streams on the server out to step 4
     * Arguments: data = object containing streams data
     * Returns: nothing
     */
    this.writeSingleStreams = function (data) {
        _me.streamsFound = data;
        /* clear out table */
        $("#s_single_streams_table tbody").html("");

        /* create html to go into table */
        var html = "";
        for (var i=0; i < data.length; i++) {
            html += "<tr>";
            html += "<td class=\"checkbox\">\n<div class=\"fac fac-checkbox fac-default\">";
            html += "<span></span><input type=\"checkbox\" id=\"s_" + i + "_active\" ";
            html += "name=\"s_" + i + "_active\" onclick=\"wizard.setStream(" + i + ");\"";
            if (data[i].status == 1) { html += " checked"; }
            html += "><label for=\"s_" + i + "_active\">&nbsp;</label>\n</div>\n</td>\n";
            html += "<td class=\"sid\">" + data[i].sid + "</td>\n";
            html += "<td class=\"name\"><input type=\"text\" id=\"s_" + i + "_name\" ";
            html += "name=\"s_" + i + "_name\" class=\"form-control\" placeholder=\"";
            html += window.language.wizard['enter_stream_name'] + "\" value=\"" + data[i].name;
            html += "\"></td>\n";
            html += "<td class=\"path\">" + data[i].path + "</td>\n";
            html += "<td class=\"status\">";
            if (data[i].status == 1) {
                html += "<span class=\"stream-up\" title=\"" + window.language.wizard['status_up'];
                html += "\"><i class=\"fas fa-volume-up\"></i></span>";
            } else {
                html += "<span class=\"stream-dn\" title=\"" + window.language.wizard['status_dn'];
                html += "\"><i class=\"fas fa-volume-mute\"></i></span>";
            }
            html += "</td>\n</tr>\n";
        }
        $("#s_single_streams_table tbody").append(html);
        $("#s_single_streams_table").show();
        $("#s_single_streams_loading").hide();
    }

}

$(document).ready(function() {
    window.wizard = new StreamConfigWizard('wizard');
    $("#next_btn").attr("disabled", false);

    $(window).keydown(function(event){
      if(event.keyCode == 13) {
        event.preventDefault();
        wizard.doStep();
        return false;
      }
    });

});
