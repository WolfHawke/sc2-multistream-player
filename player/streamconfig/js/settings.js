/**
 * SolaShout Player Settings JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* Settings object */
function Settings() {
    /* properties */

    /* self-referrer */
    _me = this;

    /* methods */
    /**
     * Function: addStream
     * Description: adds a new stream at the end of the streams list (max 10)
     * Arguments: none
     * Returns: nothing
     */
    this.addStream = function() {
        /* turn on delete box */
        if ($("#streams").children().first().children().first().css("display") == "none") {
            $("#streams").children().first().children().first().show();
        }
        var sid = window.streamData.last + 1;
        var sl = window.language.streamTxt; // form strings shortcode
        var vl = window.language.validationText; // validation strings shorcode
        /* build HTML */
        var html = "<div id=\"s" + sid + "_container\" class=\"container stream\">\n";

        /* title and delete box */
        html += "        <div class=\"delbox\"><button onclick=\"settingsObj.delStream(" + sid + ",true); return false;\" title=\"" + sl.del_blurb + "\"";
        html += "><i class=\"fas fa-trash-alt\"></i></button></div>\n";
        html += "        <h2>" + sl.individual + " " + sid + "</h2>\n";
        html += "        <div id=\"s" + sid + "_form\" class=\"container\">\n";
        /* first item: Stream name */
        html +=  "    <div class=\"form-item\">\n";
        html +=  "                <label for=\"s" + sid + "_name\">" + sl.name + "</label>&nbsp;<a data-toggle=\"popover\" data-content=\"" + sl.name_blurb + "\"><i class=\"fas fa-question-circle\"></i></a>\n";
        html += "                 <input id=\"s" + sid + "_name\" name=\"s" + sid + "_name\" type=\"text\" class=\"form-control field-check\" value=\"\">\n";
        html += "                <div class=\"invalid-feedback\" id=\"s" + sid + "_name_error\">" + vl.stream_name + "</div>\n                </div>";
        /* Second item: Stream ID */
        html += "            <div class=\"form-item\">\n";
        html += "                <label for=\"s" + sid + "_sid\">" + sl.sid + "</label>&nbsp;<a data-toggle=\"popover\" data-content=\"" + sl.sid_blurb + "\"><i class=\"fas fa-question-circle\"></i></a>\n";
        html += "                    <input id=\"s" + sid + "_sid\" name=\"s" + sid + "_sid\" type=\"number\" min=\"1\" class=\"form-control field-check\" value=\"1\">\n";
        html += "                    <div class=\"invalid-feedback\" id=\"s" + sid + "_sid_error\">" + vl.stream_id + "</div>\n                </div>\n";
        /* third item: Stream path */
        html += "            <div class=\"form-item\">\n";
        html += "                <label for=\"s" + sid + "_path\">" + sl.path + "</label>&nbsp;<a data-toggle=\"popover\" data-content=\"" + sl.path_blurb + "\"><i class=\"fas fa-question-circle\"></i></a>\n";
        html += "                    <input id=\"s" + sid + "_path\" name=\"s" + sid + "_path\" type=\"text\" class=\"form-control field-check\" value=\"/\">\n";
        html += "                    <div class=\"invalid-feedback\" id=\"s" + sid + "_path_error\">" + vl.stream_path + "</div>\n                </div>\n";
        /* fourth item: stream URL */
        html += "            <div class=\"form-item\">\n";
        html += "                    <label for=\"s" + sid + "_url\">" + sl.url + "</label>&nbsp;<a data-toggle=\"popover\" data-content=\"" + sl.url_blurb + "\"><i class=\"fas fa-question-circle\"></i></a>\n";
        html += "                    <input id=\"s" + sid + "_url\" name=\"s" + sid + "_url\" type=\"text\" class=\"form-control field-check indiv-url\" class=\"form-control\" value=\"\">\n";
        html += "                                <div class=\"invalid-feedback\" id=\"s" + sid + "_url_error\">" + vl.stream_url + "</div>\n                            </div>\n                         </div>\n";

        /* add to streams */
        $("#streams").append(html);

        /* configure indiv_url */
        if ($("#all_on_one").is(":checked")) {
            $("#s" + sid + "_url").val($("#url").val() + ":" + $("#port").val());
            $("#s" + sid + "_url").attr("readonly", true);
        }

        /* update global data */
        window.streamData.count++;
        window.streamData.last = sid;

        if (window.streamData.count > 9) {
            $("#add_stream").hide();
        }

        /* set focus to new stream name */
        $("#s" + sid + "_name").focus();

        /* update initial stream dropdown */
        _me.updateInitStreamDropdown();
    }

    /**
     * Function: checkSettingsForm
     * Description: checks if the settings form is correct before allowing submission
     * Arguments: nextStep = string denoting the next step to take
     *            prevOk = boolean denoting if previous result is clear
     *            subStream = number denoting next stream URL to check
     * Returns: boolean denoting success
     */
    this.checkSettingsForm = function (nextStep, prevOk, subStream) {
        if (typeof nextStep == "undefined") { nextStep = "checkUrlStart"; }
        if (typeof prevOk == "undefined") { prevOk = false; }
        if (typeof subStream == "undefined") { subStream = 1; }

        var url = "";
        var err = false;
        var i = 0;
        var s = "";
        var forbiddenChars = /['"\;\*:<>\?\\\|\+]+/g; // regex of characters that should not be allowed in text fields.

        switch (nextStep) {
            case "checkUrlStart":
            _me.updateStreamList();

            /* open validating modal */
            $("#validatingMessage").modal({ keyboard: false });

            /* reset all invalid fields*/
            $(".field-check").removeClass("is-invalid");
            $("#success_msg").hide();
            $("#error_msg").hide();

            /* only check URL and Port if we are on a single server */
            if ($("#all_on_one").is(":checked")) {
                url = $("#url").val();
                var port = $("#port").val();
                if (_me.validateURL(url)) {
                if (/[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535/.test(port)) {
                    url = url + ":" + port;
                    $.post("process.php", {"action":"check_sc_stream", "url": url}, '','json').done(function(d) {
                                 if (d.result == "valid") {
                                     $("#url").addClass("is-valid");
                                     $("#port").addClass("is-valid");
                                     _me.checkSettingsForm("theRest", true);
                                 } else if (d.result == "invalid") {
                                     $("#validatingMessage").modal('hide');
                                     $("#url_error").html(window.language.validationText.not_sc_server);
                                     $("#url").addClass("is-invalid");
                                 } else {
                                     console.log(d);
                                 }
                        }).fail(function(d) { console.log("error on check_sc_stream", d); });
                    } else {
                        $("#validatingMessage").modal('hide');
                        $("#port").addClass("is-invalid");
                        $("#port").focus();
                    }
                } else {
                    $("#validatingMessage").modal('hide');
                    $("#url_error").html(window.language.validationText.url);
                    $("#url").addClass("is-invalid");
                    $("#url").focus();
                    return false;
                }
            } else {
                _me.checkSettingsForm("subUrl", true, window.streamList[0]);
            }
            break;

            case "subUrl":
                console.log(subStream);
                url = $("#s" + subStream + "_url").val();
                console.log(url);
                var ajax = $.post("process.php", {"action":"check_sc_stream", "url": url, "sid": subStream}, function(r) { console.log(r); }, 'json').done(function(d) {
                             var streams = window.streamList;
                             if (d.result == "valid") {
                                 $("#s" + d.sid + "_url").addClass("is-valid");
                                 var last = streams[streams.length-1];
                                 var sid = parseInt(d.sid);
                                 if (sid < last) {
                                     i = 0;
                                     while (streams[i] <= sid) { i++; }
                                     _me.checkSettingsForm("subUrl", true, streams[i]);
                                 } else {
                                     _me.checkSettingsForm("theRest", true);
                                 }
                             } else if (d.result == "invalid") {
                                 $("#validatingMessage").modal('hide');
                                 $("#s" + d.sid + "_url_error").html(window.language.validationText.not_sc_server);
                                 $("#s" + d.sid + "_url").addClass("is-invalid");
                                 $("#s" + d.sid + "_url").focus();
                             } else {
                                 console.log(d);
                             }
                    }).fail(function(d) { console.log("error on check_sc_stream subUrl", d); });

            break;

            case "theRest":
            /* check stream names */
            for (i=0; i<window.streamList.length; i++) {
                s = $("#s" + window.streamList[i] + "_name");
                if (s.val().length < 1 || forbiddenChars.test(s)) {
                    s.addClass("is-invalid");
                    err = true;
                }
            }

            /* check stream IDs */
            for (i=0; i<window.streamList.length; i++) {
                s = $("#s" + window.streamList[i] + "_sid");
                if (s.val() < 1) {
                    s.addClass("is-invalid");
                    err = true;
                }
            }

            /* check stream paths */
            for (i=0; i<window.streamList.length; i++) {
                s = $("#s" + window.streamList[i] + "_path");
                if (s.val().substr(0,1) != "/" || forbiddenChars.test(s)) {
                    s.addClass("is-invalid");
                    err = true;
                }
            }

            /* check password */
            if ($("#oldpwd").val().length > 0) {
                if (_me.validatePassword($("#newpwdinit").val())) {
                    $("#newpwdinit").addClass('is-valid');
                    if ($("#newpwdinit").val() === $("#newpwdconf").val()) {
                        $("#newpwdconf").addClass('is-valid');
                    } else {
                        $("#newpwdconf").addClass('is-invalid');
                        err = true;
                    }
                } else {
                    $("#newpwdinit").addClass('is-invalid');
                    err = true;
                }
            }

            /* update streamlist field */
            $("#stream_list").val("[" + window.streamList.toString() + "]");

            /* display errors or submit form */
            if (err){
                setTimeout(function() {$("#validatingMessage").modal('hide');}, 500);
                window.scrollTo(0,0);
                $("#error_msg").html($("#error_msg").html().replace("%error%",window.language.validationText.invalid_fields));
                $("#error_msg").show();
            } else {
                /* execute submission via ajax */
                _me.saveSettings();
            }
            break;
        }

    }


    /**
     * Function: delStream
     * Description: first displays a confirmation prompt, then deletes a stream
     * Arguments: sid = integer denoting stream to delete
     *            confirm = boolean; true to delete, false to not
     */
    this.delStream = function (sid, confirm) {
        if (typeof confirm == 'undefined') { confirm = false; }
        if (confirm == false) {
            /* confirm deletion */
            $("#stream_del").modal({focus:true});
            $("#delsid").val(sid); /* set stream ID to delete */
        } else if (confirm == true) {
            /* execute deletion */
            /* get stream ID */
            if (sid == 0) { sid = $("#delsid").val(); }

            /* hide and remove layer */
            $("#s" + sid + "_container").slideUp(400, function () {
                $(this).remove();

                /* update streamData */
                window.streamData.count = $("#streams").children().length;
                window.streamData.last = parseInt($("#streams").children().last().attr("id").match(/\d+/)[0]);

                /* update initial stream dropdown */
                _me.updateInitStreamDropdown();

                /* hide the delete button if we only have one stream */
                if (streamData.count == 1) {
                    $("#s" + window.streamData.last + "_container").children().first().hide();
                }

                /* show the add stream button if we have less than 10 streams */
                if ((streamData.count < 10) && ($("#add_stream").css("display") == "none")) {
                    $("#add_stream").show();
                }
            });
        }
    }

    /**
     * Function: modInitialStream
     * Description: modifies the initial stream dropdown box
     * Arguments: action = "add" or "remove"
     *            sid = stream id to operate on
     * Returns: nothing
     */
    this.modInitialStream = function (action, sid) {
        if (typeof action == "undefined" || typeof sid == "undefined") { return false; }

    }

    /**
     * Function: randomID
     * Description: generates a random alphanumeric id in a given length
     * Arguments: chars = how long the string should be, defaults to 8
     *            kind = "any", "decimal", "alphabet", "hexadecimal",
     *                   defaults to any
     * Returns: string containing random ID
     */
    this.randomID = function (chars, kind) {
        if (typeof chars == "undefined") { chars = 8; }
        if (typeof kind == "undefined") { kind = "any"; }
        var seed = ["0","1","2","3","4","5","6","7","8","9","A","B","C",
                    "D","E","F","G","H","I","J","K","L","M","N","O","P",
                    "Q","R","S","T","U","V","W","X","Y","Z","a","b","c",
                    "d","e","f","g","h","i","j","k","l","m","n","o","p",
                    "q","r","s","t","u","v","w","x","y","z"];
        var r = -1;
        var id = "";
        var start = 0;
        var end = seed.length - 1;
        switch (kind) {
            case "decimal":
                end = 9;
                break;
            case "hexadecimal":
                end = 15;
                break;
            case "alphabet":
                start = 10;
                break;
        }

        for (var i=0; i<chars; i++) {
            while (r < start || r > end) {
                r = Math.floor((Math.random() * 100));
            }
            id += seed[r];
            r = -1;
        }
        return id;
    }

    /**
     * Function: saveSettings
     * Description: submits the form contents via ajax
     * Arguments: none;
     * Returns: nothing
     */
    this.saveSettings = function () {
        var i = 0;
        var data = {"action":"settings", "stream_list":$("#stream_list").val() };
        var fieldNames = ["url", "port", "all_langs", "default_language", "theme", "initial_stream", "oldpwd", "newpwdinit", "newpwdconf", "pwa_title"];
        var checkNames = ["all_on_one", "autoplay", "user_set_language",  "user_change_theme", "adblock_warning", "pwa", ];
        var is = ""; // individual streams placeholder

        /* get unchanging data */
        for (i=0; i < fieldNames.length; i++) {
            data[fieldNames[i]] = $("#" + fieldNames[i]).val();
        }

        /* check box data */
        for (i=0; i < checkNames.length; i++) {
          /* Items that are set to 0 should not be passed, because PHP script expects them to not be there */
          if ($("#" + checkNames[i]).is(":checked")) {
            data[checkNames[i]] = $("#" + checkNames[i]).val();
          }
        }

        /* individual streams */
        for (i=0; i<window.streamList.length; i++) {
            is = window.streamList[i];
            data["s" + is + "_url"] = $("#s" + is + "_url").val();
            data["s" + is + "_name"] = $("#s" + is + "_name").val();
            data["s" + is + "_path"] = $("#s" + is + "_path").val();
            data["s" + is + "_sid"] = $("#s" + is + "_sid").val();
        }

        /* lockdown */
        if ($("#lockdown").is(":checked")) {
            var epoch = Math.round(Date.now() / 1000);
            data["oldpwd"] = epoch;
            data["newpwdinit"] = $("#unlock_code").val();
            data["newpwdconf"] = $("#unlock_code").val();
            data["lockdown"] = "true";
        }

        console.log(data);

        /* execute submission */
        $.post("process.php",data,"","json"
               ).done(function(d) {
                   /* hide validating modal */
                   setTimeout(function() {$("#validatingMessage").modal('hide');}, 500);
                   if (typeof d.error != "undefined") {
                       window.scrollTo(0,0);
                       $("#error_msg").html($("#error_msg").html().replace("%error%", window.language.errorText[d.error]));
                       $("#error_msg").show();
                   } else {
                       if ($("#lockdown").is(":checked")) {
                           $("#lockedDoneMessage").modal();
                       } else {
                           window.scrollTo(0,0);
                           $("#success_msg").show();
                           /* clear out password fields */
                           $("#oldpwd").val("");
                           $("#newpwdinit").val("");
                           $("#newpwdconf").val("");
                       }
                   }
                }).fail(function(d) {
                    console.log("error on submit", d);
                });
    }

    /**
     * Function: showStream
     * Description: shows or hides a given stream upon the state of the checkbox by
     *              the stream title.
     * Arguments: sid = integer denoting stream to show (1-4)
     * Returns: nothing
     */
    this.showStream = function (sid) {
        if ($("#s"+sid).is(":checked")) {
            $("#s" + sid + "_form").removeClass('s-noselect');
        } else {
            $("#s" + sid + "_form").addClass('s-noselect');
        }
    }

    /**
     * Function: setAllOnOne
     * Description: sets the URL fields properly according to the state of
     *              the all_on_one checkbox
     * Arguments: none
     * Returns: nothing
     */
    this.setAllOnOne = function () {
        if ($("#all_on_one").is(":checked")) {
            $("#url").removeAttr("readonly");
            $("#port").removeAttr("readonly");
            $(".indiv-url").attr("readonly",true);
        } else {
            $("#url").attr("readonly",true);
            $("#port").attr("readonly",true);
            $(".indiv-url").removeAttr("readonly");
        }
    }

    /**
     * Function: sOneDeselect
     * Description: Pops up a confirmation dialog when deselecting Stream 1
     * Arguments: okay = whether or not to deselect; boolean; defaults to false
     * Returns: nothing
     */
    this.sOneDeselect = function (okay) {
        if (typeof okay == "undefined") { okay = false; }
        if (okay) {
            $("#s1").prop("checked",false);
            $("#s1_form").slideUp();
        } else {
            if (!$("#s1").is(":checked")){
                $("#s_one_closer").modal({focus:true});
                $("#s1").prop("checked",true);
            } else {
                $("#s1_form").slideDown();
            }

        }
    }

    /**
     * Function: toggleLockout
     * Description: generates the random lockout code and toggles the field
     * Arguments: none
     * Returns: nothing
     */
    this.toggleLockout = function () {
        if ($("#lockdown").is(":checked")) {
            /* generate unlock code */
            var ulCode = _me.randomID(15);

            /* generate url */
            var url = window.location.href;
            url = url.substr(0,(url.indexOf("streamconfig")+"streamconfig".length))
            url += "/?unlock-code=" + ulCode;

            var urlTxt = "<a href=\"" + url + "\" onclick=\"return false;\">";
            urlTxt += url + "</a>";

            /* populate data */
            $("#unlock_code").val(ulCode);
            $("#unlock_code_txt").html(ulCode);
            $("#unlock_code_link").html(urlTxt)

            /* display proper layers */
            $("#pwd_fields").hide();
            $("#lockout_unlock_code").show();

        } else {
            $("#lockout_unlock_code").hide();
            $("#oldpwd").val("");
            $("#newpwdinit").val("");
            $("#newpwdconf").val("");
            $("#pwd_fields").show();
        }
    }

    /**
     * Function: updateInitStreamDropdown
     * Description: updates the Individual Streams dropdown box
     * Arguments: none
     * Returns: nothing
     */
    this.updateInitStreamDropdown = function  () {
        /* update the stream list */
        _me.updateStreamList();

        /* get current field object and stream value */
        var initStreamObj = $("#initial_stream");
        var initStreamVal = parseInt(initStreamObj.val());

        /* initialize function variables */
        var html = "";
        var newInitVal = window.streamList[0];
        var i = 0;

        /* build and insert new list */
        for (i=0; i< window.streamList.length; i++) {
            html += "            <option value=\"" + window.streamList[i];
            html += "\">" + window.streamList[i] + "</option>\n";
        }
        initStreamObj.html(html);

        /* set initial stream in dropdown */
        for (i=0; i < window.streamList.length; i++) {
            if (window.streamList[i] == initStreamVal) {
                newInitVal = initStreamVal;
                break;
            }
        }

        initStreamObj = newInitVal;
    }

    /**
     * Function: updateStreamList
     * Description: updates the global variable with the streams currently
     *              available on the page.
     * Arguments: none
     * Returns: nothing
     */
    this.updateStreamList = function () {
        /* get list of streams on page */
        window.streamList = [];

        /* clear all visible divs */
        $("#streams").children().each(function(i,e) { window.streamList.push(parseInt(e.id.match(/\d+/))); });
    }

    /**
     * Function: validatePassword
     * Description: validates a given password to be at least 8 characters long, contain at least 1 digit
     * Arguments: p = password to test against
     * Returns: boolean denoting validity
     */
    this.validatePassword = function (p) {
        var result = true;
        if ((p.length < 8) || (!p.match(/[A-Z]/) || (!p.match(/[a-z]/)) || (!p.match(/[0-9]/)) || (p.match(/[\"\'\\]/)) )) {
            result = false;
        }
        return result;
    }

    /**
     * Function: validateURL
     * Description: validates a URL either alphanumeric or IP address
     * Arguments: u = URL to validate
     *            hasPort = do we have a port appended?
     * Returns: boolean denoting validity
     */
    this.validateURL = function (u,hasPort) {
        if (typeof hasPort == "undefined") { hasPort = false; }
        if (typeof tld == "undefined") { tld = true; }
        /* patterns -- default is with prefix without port */
        var fqdn = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]?[0-9]?[0-9]?[0-9]?[0-9]|65535)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        /* test for ip pattern of xxx.xxx.xxx.xxx */
        var ip = /(http|https):\/\/[\d]+\.[\d]+\.[\d]+\.[\d]+/;
        /* test for ip address without a purt number */
        var ip_noPort = /((http|https):\/\/)?(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2}))\.){3}(((25[0-5])|(2[0-4]\d)|(1\d{2})|(\d{1,2})))$/;
        var ip_hasPort = /^((http|https):\/\/)(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):[0-9]+$/g

        /* result variable */
        var result = false;

        /* run tests */
        /* check if we have pattern for ip address */
        if (ip.test(u)) {
            if (!hasPort) {
                result = ip_noPort.test(u);
            } else {
                result = ip_hasPort.test(u)
            }
            if (result && (u.split("/").length > 3)) { result = false; }
        } else {
            /* if not, make sure that uri is valid fqdn */
            result = fqdn.test(u);
            if (result && hasPort) {
                    var p = u.split(":");
                    p[2] = parseInt(p[2]);
                    if ((p.length != 3) || (!Number.isInteger(p[2])) || (u.split("/").length > 3)) { result = false; }
            } else if (result) {
                if ((u.split(":").length > 2) || (u.split("/").length > 3)) { result = false; }
            }
        }

        return result;
    }

}

$(document).ready(function() {
    window.settingsObj = new Settings();
    $('[data-toggle="popover"]').popover({ trigger: "hover focus"});
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});
