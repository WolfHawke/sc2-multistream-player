/**
 * SolaShout Player Login JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* login object */
function Login() {
    /* properties */

    /* self-reference */
    var _me = this;

    /* methods */
    /**
     * Function: _modLoginButton
     * Description: Enables or disables the login button
     * Arguments: todo = 'enable' or 'disable'
     * Returns: nothing
     */
    this._modLoginButton = function (todo) {
        if (typeof todo == "undefined") { return false; }
        switch (todo) {
            case "enable":
                $("#do_login_txt").show();
                $("#do_login_spinner").hide();
                $("#do_login").prop("disabled", false);
                break;
            case "disable":
                $("#do_login_txt").hide();
                $("#do_login_spinner").show();
                $("#do_login").prop("disabled", true);
                break;
        }
    }

    /**
    * Function: forgotForm
    * Description: Shows or hides the forgot password form
    * Arguments: todo = 'show' or 'hide'
    * Returns: nothing
    */
    this.forgotForm = function (todo) {
        if (typeof todo == "undefined") { return false; }
        switch(todo) {
            case "show":
                $("#login_form").fadeOut(400, function() {
                    $("#pwd_reset_form").fadeIn();
                    $("#reset_identity").focus();
                })
                break;
            case "hide":
            $("#pwd_reset_form").fadeOut(400, function() {
                $("#reset_identity").prop("disabled",false);
                $("#login_form").fadeIn();
                $("#pwd_reset_cancel").show();
                $("#do_reset_txt").show();
                $("#do_reset_spinner").hide();
                $("#return_from_reset").hide();
                $("#reset_error_msg").hide();
                $("#reset_success_msg").hide();
                $("#reset_identity").removeClass("is-invalid");
                $("#do_reset").show();
                $("#identity").focus();
            })
            break;
        }
    }

    /**
     * Function: processLogin
     * Description: Processes the login form
     * Arguments: none
     * Returns: nothing
     */
    this.processLogin = function () {
        /* disable login button */
        _me._modLoginButton('disable');

        /* remove is-invalid */
        $(".field-check").removeClass("is-invalid");

        /* send data */
        var data = {"action":"dologin",
                    "identity":$("#identity").val(),
                    "challenge":$("#challenge").val()
                    }
        $.post("process.php", data, "", "json").done(function(r) {
            console.log(r);
            if (typeof r.error != "undefined") {
                for (var i=0; i<r.error.length; i++) {
                    $("#" + r.error[i]).addClass("is-invalid");
                    _me._modLoginButton('enable');
                }
            } else {
                if ($.cookie("spsess") == r.success) {
                    window.location = "../streamconfig/?key=" + r.key;
                } else {
                    $("#error_msg").slideDown();
                    _me._modLoginButton('enable');
                }
            }
        }).fail(function(e) {
            console.log(e);
            $("#error_msg").slideDown();
            _me._modLoginButton('enable');
        });

    }

    /**
     * Function: processReset
     * Description: Processes the reset password form
     * Arguments: none
     * Returns: nothing
     */
    this.processReset = function () {
        /* hide layers */
        $("#pwd_reset_cancel").hide();
        $("#do_reset_txt").hide();
        $("#do_reset_spinner").show();
        $("#reset_error_msg").hide();
        $("#reset_identity").removeClass("is-invalid");

        /* execute query */
        var data = {"action":"do_pwd_reset","identity":$("#reset_identity").val()}
        $.post("process.php", data, "", "json").done(function(r) {
            if (typeof r.error !== "undefined") {
                if (r.error == "wrong_id") {
                    /* e-mail is incorrect */
                    $("#reset_identity").addClass("is-invalid");
                    $("#reset_identity").focus();
                } else {
                    /* mail sending error */
                    $("#reset_error_msg").show();
                }
                $("#pwd_reset_cancel").show();
                $("#do_reset_txt").show();
                $("#do_reset_spinner").hide();
            } else {
                /* success! */
                $("#reset_identity").prop("disabled", true);
                $("#do_reset").hide();
                $("#return_from_reset").show();
                $("#return_from_reset").focus();
            }
        }).fail(function(e) { console.log(e); });

    }
}

/* activate form if JavaScript is available */
$(document).ready(function () {
    /* instantiate object */
    window.loginObj = new Login();
    $("#identity").prop("disabled", false);
    $("#challenge").prop("disabled", false);
    $("#do_login").prop("disabled", false);
    $("#identity").focus();
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            if ($("#login_form").css("display") == "none") {
                loginObj.processReset();
            } else {
                loginObj.processLogin();
            }
            event.preventDefault();
            return false;
        }
    });
});
