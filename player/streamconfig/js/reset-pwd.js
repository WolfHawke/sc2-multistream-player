/**
 * SolaShout Player Password Reset JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/** ResetPwd object */
function ResetPwd() {
    /* properties */

    /* self-reference */
    var _me = this;

    /* methods */
    /**
    * Function: logout
    * Description: logs user out of the system
    * Arguments: none
    * Returns: nothing
    */
    this.logout = function () {
     window.location = "process.php?action=dologout&nosuccess=true";
    }

    /**
     * Function: processChallengeChange
     * Description: processes the change to the password
     * Arguments: none
     * Returns: nothing
     */

    this.processChallengeChange = function () {
        /* reset all values */
        $(".field-check").removeClass("is-invalid");

        /* hide field parts */
        $(".fieldcheck").prop("disabled", true);
        $("#do_login").prop("disabled", true);
        $("#do_login_txt").hide();
        $("#do_login_spinner").show();

        var newpwd = $("#newpwdinit").val();
        var data = {"action":"do_pwd_reset", "newpwdforce":"true",
                    "oldpwd": $("#oldpwd").val(),
                    "newpwdinit": newpwd }

        if (_me.validatePassword(newpwd)) {
            if ($("#newpwdconf").val() == newpwd) {
                $.post("process.php", data, "", "json").done(function(r){
                    if (typeof r.error != "undefined") {
                        $(".fieldcheck").prop("disabled", false);
                        $("#do_login").prop("disabled", false);
                        $("#do_login_txt").show();
                        $("#do_login_spinner").hide();

                        if (r.error == "bad_old") {
                            $("#oldpwd").addClass("is-invalid");
                        } else {
                            console.log(r.error);
                        }
                    } else {
                        $("#pwd_form").fadeOut(400, function() {
                            $("#do_login_txt").show();
                            $("#do_login_spinner").hide();
                            $("#pwd_success").fadeIn();
                        })
                    }
                }).fail(function(e) { return(e); });
            } else {
                $("#newpwdconf").addClass("is-invalid");
                $(".fieldcheck").prop("disabled", false);
                $("#do_login").prop("disabled", false);
                $("#do_login_txt").show();
                $("#do_login_spinner").hide();
            }
        } else {
            $("#newpwdinit").addClass("is-invalid");
            $(".fieldcheck").prop("disabled", false);
            $("#do_login").prop("disabled", false);
            $("#do_login_txt").show();
            $("#do_login_spinner").hide();
        }
    }

    /**
     * Function: validatePassword
     * Description: validates a given password to be at least 8 characters long, contain at least 1 digit
     * Arguments: p = password to test against
     * Returns: boolean denoting validity
     */
    this.validatePassword = function (p) {
        var result = true;
        if ((p.length < 8) || (!p.match(/[A-Z]/) || (!p.match(/[a-z]/)) || (!p.match(/[0-9]/)) || (p.match(/[\"\'\\]/)) )) {
            result = false;
        }
        return result;
    }
}

/* activate form  */
$(document).ready(function () {
    window.resetPwd = new ResetPwd();
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            resetPwd.processChallengeChange();
            event.preventDefault();
            return false;
        }
    });
});
