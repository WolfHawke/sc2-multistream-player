/**
 * SolaShout Player Settings JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* script to refresh the page if we have the proper stuff */
/* but only trigger if we have not been refreshed! */
if (window.location.href.indexOf("success") > -1 && window.location.href.indexOf("refreshed") < 0 ) {
    setTimeout(function () {
        window.location = window.location.href + "&refreshed=true";
        console.log(window.location.href);
    },1000);
}
