/**
 * SolaShout Player Localization Selection screen JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* Localize object */
function Localize() {
    /* properties */

    /* self reference */
    var _me = this;

    /* methods */
    /**
     * Function: addLang
     * Description: Adds a language file to a given section
     * Arguments: none
     * Returns: nothing
     */
    this.addLang = function() {
        var data = {
                "action" : "add_lang",
                "area" : $("#area").val(),
                "lang" : $("#selected_lang").val(),
            }
        $("#error_msg_create").hide();
        $("#error_msg_delete").hide();
        $("#add_lang_button_spinner").show();
        $("#add_lang_button_text").hide();
        $("#add_lang_submit_button").prop("disabled",true);
        $("#add_lang_close_button").prop("disabled",true);

        console.log(data);

        $.post("process_localize.php",data,"","json").done(
            function(d) {
                if (typeof d.error != "undefined") {
                    $("#add_lang_button_spinner").hide();
                    $("#add_lang_button_text").show();
                    $("#add_lang_submit_button").prop("disabled",false);
                    $("#add_lang_close_button").prop("disabled",false);
                    $("#select_new_lang").modal("hide");
                    $("#error_msg_create").show();
                    window.scrollTo(0,0);
                } else {
                    window.location.reload(true);
                }
            }
        ).fail(
            function(e) {

            }
        )

        /* when finished:

        */

        $("#select_new_lang").modal("hide");

    }

    /**
     * Function: delLang
     * Description: Deletes a given language asyncronously
     * Arguments: lang = target language
     *            confirm = boolean denoting the modal confirmation; defaults to false
     *            area = docs, front, back; optional
     * Returns: boolean denoting success.
     */
     this.delLang = function(lang, confirm, area) {
        if (typeof lang == "undefined") { lang = false; }
        if (typeof confirm == "undefined") { confirm = false; }
        if (typeof area == "undefined") { area = ""; }

        $("#error_msg_create").hide();
        $("#error_msg_delete").hide();

        if (confirm == false) {
            var msg = $("#del_dialog_confirm_msg").html().replace("%this%", $("#lang_" + lang).html());
            $("#del_dialog_confirm_msg").html(msg);
            $("#dellang").val(lang);
            $("#lang_del").modal({focus:true});
        } else if (confirm == true) {
            var data = {"action": "del_lang",
                        "lang": $("#dellang").val(),
                        "area": area}
            console.log(data);
            $.post("process_localize.php",data,"","json").done(
                function(d) {
                    if (typeof d.error != "undefined") {
                        $("#error_msg_delete").show();
                    } else {
                        location.reload();
                    }
                    $("#lang_del").modal("hide");
                }
            ).fail(
                function(e) {
                    console.log("Error deleting localization file!", e);
                    $("#lang_del").modal("hide");
                }
            )

        }
    }


    /**
     * Function: loadLang
     * Description: Triggers a post to the localize_nave form
     * Arguments: lang = target language
     *            mode = 'edit' or 'view'
     * Returns: boolean denoting success.
     */
    this.loadLang = function(lang, mode) {
        if (typeof lang == "undefined") { return false; }
        if (typeof mode == "undefined") { return false; }
        $("#localize_nav").append("<input type=\"hidden\" name=\"target-lang\" value=\""+ lang + "\">");
        if (mode == "view"){
            $("#localize_nav").append("<input type=\"hidden\" name=\"view\" value=\"1\">");
        }
        $("#localize_nav").submit();
        return true;
    }

    /**
     * Function: selectLang
     * Description: drives the language selection feature on the Add Language Dialog
     * Arguments: lc = language code
     * Returns: nothing
     */
    this.selectLang = function(lc) {
        if (typeof lc == 'undefined') { return false; }
        $(".sel-lang-item").removeClass("active");
        $("#select_lang_" + lc).addClass("active");
        $("#selected_lang").val(lc);
    }
}

$(document).ready(function () {
    window.localizeObj = new Localize();
});
