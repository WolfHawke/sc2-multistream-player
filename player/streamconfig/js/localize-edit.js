/**
 * SolaShout Player Localization Edit Screen JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* Translation object
 * Arguments: area = 'back', 'docs', 'front'
 *            target = language code for target language 
 *
 */
function Translation(area, target) {
    /* public properties */
    this.activeString = "";     /* key name of the active string */
    this.stringsObj = {};       /* object containing strings */
    this.targetArea = area;     /* target area (back, docs, front) */
    this.targetLang = target;   /* target language code */
    this.xlatedStrings = {};    /* translated strings */

    /* private properties */
    this._isDirty = false;      /* translation state */
    this._freshLoad = false;    /* whether or not the item was freshly loaded */
    this._autosaveControl = ""; /* container for autosave timeout object */
    this._prevString = "";      /* previous string to activeString */
    this._nextString = "";      /* next string to activeString */

    /* self-referrer */
    var _me = this;

    /* methods */
    /**
     * Function: _init
     * Description: function to initialize translation interface; loads the strings
     * Arguments: none
     * Returns: boolean denoting sucess
     */
    this._init = function () {
        var r = false;
        var data = {
            "action": "load_lang",
            "area": _me.targetArea,
            "target_lang": _me.targetLang
        }

        /* load translation strings */
        $.post("process_localize.php", data, '', 'json').done(
            function(d) {
                if (typeof d == "object"){
                    _me.stringsObj = d;
                    _me._displayStrings();
                    _me.autosave();
                } else {
                    console.log(d);
                }
            }).fail(
            function (e) {
                console.log("Error getting strings to translate!", e);
            });

        /* activate keyboard shortcuts */
        $(window).keydown(_me.keyDetect);

        /* confirm exit if there are unsaved strings */
        $(window).bind("beforeunload", function() {
            if (_me._isDirty) {
                return true;
            }
        });
    }

    /**
     * Function: _clearSavedRows
     * Description: changes the class and icon of the saved rows to translated
     * Arguments: none
     * Returns: nothing
     */
    this._clearSavedRows = function () {
        $(".not-saved").each(function (i) {
                $(this).children(":first").html("<i class=\"far fa-comment\"></i>");
                $(this).addClass("translated");
        });
        $(".translated").removeClass("not-saved");
    }

    /**
     * Function: _displayStrings
     * Description: function to write the strings to the interface
     * Arguments: none
     * Returns: boolean denoting success
     */
     this._displayStrings = function() {
         var html = "";
         var currentObj;
         var status = {"class":"", "title":"", "icon":""};
         var blurb = "";
         var first = true;
         var firstUntranslated = true;
         var startHere;
         var skip = false;
         for (const key in _me.stringsObj) {
             if (key != "languageCode" && key != "languageName" && key != "languageIsoName" &&
             key != "languageDirection") {
                 /* load up variables so up and down navigation works */
                 if (first) {
                     _me._nextString = key;
                     first = false;
                 }

                 currentObj = _me.stringsObj[key];
                 if (typeof currentObj.group != "undefined") {
                     status.class = "group";
                     status.title = window.language.localize.group;
                     status.icon = "fa-angle-down";
                     blurb = currentObj.description;
                     // skip = true;
                 } else if (currentObj[_me.targetLang] == "") {
                     status.class = "not-translated";
                     status.title = window.language.localize.not_translated;
                     status.icon = "fa-comment-slash";
                     blurb = currentObj.en;
                     if (firstUntranslated) {
                        startHere = key;
                        firstUntranslated = false;
                     }
                 } else {
                     status.class = "translated";
                     status.title = window.language.localize.done;
                     status.icon = "fa-comment";
                     blurb = currentObj.en;
                 }
                 if (!skip) {
                    html += "<div id=\"" + key + "\" class=\"row translation-item ";
                    html += status.class + " border-bottom border-secondary\" title=\"";
                    html += status.title + "\" onclick=\"xlate.loadString('" + key + "')\">\n";
                    html += "<div class=\"translated-icon col-1 text-center\">\n";
                    html += "<i class=\"fas " + status.icon + "\"></i>\n</div>\n";
                    html += "<div class=\"col-8 translation-blurb\">" + blurb + "</div>\n";
                    html += "<div class=\"col-3\">[" + key + "]</div>\n</div>";
                    blurb = "";
                 } else {
                     skip = false;
                 }
            }
         }
         $("#to_translate").html(html);
         $("#to_translate").scrollTo("#" + startHere);
     }



    /**
     * Function: autosave
     * Description: controls the automatic save every 60 seconds
     * Arguments: none
     * Returns: nothing
     */
     this.autosave = function() {
         var secs = 60;
         if ($("#autosave").is(":checked")) {
             if (_me._isDirty) { _me.save(); }
             _me._autosaveControl = setTimeout(function() {_me.autosave()}, (secs * 1000));
         } else {
             clearTimeout(_me._autosaveControl);
         }
     }

     /**
      * Function: dirty
      * Description: sets translation state to dirty
      * Arguments: none
      * Returns: nothing
      */
     this.dirty = function() {
         if (_me.activeString == "" ) { return; }
         if ($("#" + _me.activeString).hasClass('not-saved')) { return; }
         console.log("#" + _me.activeString + " is dirty");
         _me._isDirty = true;
         /* change class */
         $("#" + _me.activeString).removeClass(["translated", "not-translated", "selected"]);
         $("#" + _me.activeString).addClass(["not-saved", "selected"]);
         $("#" + _me.activeString).children(":first").html("<i class=\"fas fa-pencil-alt\"></i>");
     }

     /**
     * Function: duplicateOriginalString
     * Description: duplicates what is in the original string to the translation window
     * Arguments: none
     * Returns: boolean
     */
     this.duplicateOriginalString = function() {
         $("#translate_to_text").val($("#translate_original").val());
         $("#translate_to_text").focus();
         this.dirty();
         return false;
     }

     /**
      * Function: expandTranslation
      * Description: expands and contracts the translation window
      * Arguments: none
      * Returns: boolean
      */
     this.expandTranslation = function() {
         if ($("#pane_string_list").css("display") == "none") {
             /* contract pane */
             $("#pane_string_list").show();
             $("#pane_translation").removeClass("h-100");
             $("#pane_translation").removeClass("mt-2");
             $("#pane_translation").addClass("h-50");
             $("#translate-xpand-btn").removeClass("btn-secondary");
             $("#translate-xpand-btn").addClass("btn-outline-secondary");
         } else {
             /* expand pane */
             $("#pane_string_list").hide();
             $("#pane_translation").removeClass("h-50");
             $("#pane_translation").addClass("h-100");
             $("#pane_translation").addClass("mt-2");
             $("#translate-xpand-btn").removeClass("btn-outline-secondary");
             $("#translate-xpand-btn").addClass("btn-secondary");
         }
         $("#translate_to_text").focus();
         return false;
     }

     /**
      * Function: goToItem
      * Description: moves to previous or next item
      * Arguments: dir = 'prev' or 'next'
      * Returns: nothing
      */
    this.goToItem = function(dir) {
        if (typeof dir == "undefined") { return false; }
        if (dir == "prev" && _me._prevString != "") {
            $("#" + _me._prevString).click();
        } else if (dir == "next" && _me._nextString != "") {
            $("#" + _me._nextString).click();
        }
        if (_me._prevString == "") {
            $("#to_translate").scrollTo("#" + _me.activeString);
        } else {
            $("#to_translate").scrollTo("#" + _me._prevString);
        }
    }

     /**
      * Function: keyDetect
      * Description: event handler for application-specific keyboard shortcuts
      * Arguments: none
      * Returns: false to prevent default executions
      */
    this.keyDetect = function() {
         var evtobj = window.event ? event : e;
         var key = "";
         //console.log(evtobj);
         if (evtobj.ctrlKey && evtobj.shiftKey) {
             key = evtobj.key;
             _me.queueString();
             switch (evtobj.key) {
                 case "Q":
                     $('#localize_nav').submit();
                     return false;
                     break;
                 case "S":
                     _me.save();
                     return false;
                     break;
                 case "V":
                     _me.duplicateOriginalString();
                     return false;
                     break;
                 case "E":
                     _me.expandTranslation();
                     return false;
                     break;
                 case "Enter":
                     _me.goToItem('next');
                     return false;
                    break;
                 case "ArrowUp":
                     _me.goToItem('prev');
                     return false;
                     break;
             }
         }
     }

     /**
      * Function: loadString
      * Description: loads the string data into the interface
      * Arguments: s = key of string to load
      * Returns: boolean denoting success
      */
     this.loadString = function(s) {
         var currentObj = _me.stringsObj[s];
         var foundIt = false;
         _me.activeString = s;
         _me._freshLoad = true;
         $(".translation-item").removeClass("selected");
         $("#" + s).addClass("selected");
         /* check for group */
         if (typeof currentObj.groups != "undefined") {
             $("#translate_original").val("");
             $("#translate_to_text").val("");
             $("#translate_to_text").prop("disabled",true);
             $("#translate_description").html(currentObj.description);
         } else {
             $("#translate_original").val(currentObj.en);
             $("#translate_to_text").val(currentObj[_me.targetLang]);
             $("#translate_to_text").prop("disabled",false);
             $("#translate_description").html(currentObj.description);
             $("#translate_to_text").focus();
         }
         /* calculate previous and next strings */
         _me._prevString = "";
         _me._nextString = "";
         for (const key in _me.stringsObj) {
             if (foundIt) {
                 _me._nextString = key;
                 if (_me._prevString == "languageDirection") { _me._prevString = ""; }
                 return true;
             } else if (_me.activeString == key) {
                 foundIt = true;
             } else {
                 _me._prevString = key;
             }
         }
         return true;
     }

     /**
      * Function: queueString
      * Description: queues a string into the xlatedStrings object
      * Arguments: none
      * Returns: boolean denoting success
      */
     this.queueString = function () {
         var currentObj = _me.stringsObj[_me.activeString];
         var xlatedString = $("#translate_to_text").val().replace("'","’");
         if (_me.activeString != "") {
             if (currentObj[_me.targetLang] != xlatedString) {
                 currentObj[_me.targetLang] = xlatedString;
                 _me.xlatedStrings[_me.activeString] = currentObj[_me.targetLang];
            }
        }
     }

     /**
      * Function: save
      * Description: saves the translated strings to the system
      * Arguments: none
      * Returns: boolean denoting result
      */
     this.save = function() {
         var data = {"action": "update_lang",
                     "area": _me.targetArea,
                     "lang": _me.targetLang,
                     "strings": _me.xlatedStrings}
         $("#saving").show();
         $.post("process_localize.php",data).done(
             function (d) {
                 $("#saving").hide();
                console.log(d);
                _me._clearSavedRows();
                _me.xlatedStrings = {};
                _me._isDirty = false;
         }).fail(
             function(e) {
                console.log("Error on saving!",e);
         });
     }

     /* initialize object on start */
     _me._init();
}



$(document).ready(function() {
    window.xlate = new Translation(window.localizationData.area, window.localizationData.target);
});
