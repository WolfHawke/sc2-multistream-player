<?php
/**
 * SolaShout Player Settings
 * Edit settings form (called by index.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* force return to index.php if page is called directly */
 if (strpos($_SERVER['REQUEST_URI'],basename(__FILE__)) !== FALSE) {
     header("Location: ../streamconfig/");
 }

/* force page refresh */
if ((isset($_GET['success']) || isset($_GET['error'])) && (!isset($_GET['refreshed']))) {
    /* we add the epoch to make sure that it always refreshes -- stupid Chrome! */
    $url = (isset($_SERVER['HTTPS'])) ? "https://" : "http://";
    $url .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . "&refreshed=" . date('U');
    header("Location: {$url}");
    exit;
}

/* Make it clear we're running edit_settings */
$pg = 'settings';

/* set readonly attribute for server URL and Server Port strings if
 * all streams are not on one server
 */
$serv_readonly = "";
if ($settings['all_on_one'] == 0) {
    $serv_readonly = " readonly";
}

/* get header */
$html = '';
$page = 'settings';
require('header.php');

$html .= "
    <!-- Modal for delete confirmation -->
    <div class=\"modal fade\" id=\"stream_del\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"staticBackdropLabel\">{$LANGUAGE['del_stream_dialog']['title']}</h5>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">
            <input type=\"hidden\" id=\"delsid\" name=\"delsid\" value=\"0\">
            <p>";
$LANGUAGE['del_stream_dialog']['confirm_msg'] = str_replace('||', '<br>', $LANGUAGE['del_stream_dialog']['confirm_msg']);
$html .= "{$LANGUAGE['del_stream_dialog']['confirm_msg']}</p>
            <small>{$LANGUAGE['del_stream_dialog']['renumber_msg']}</small>
          </div>
          <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-primary\" onclick=\"settingsObj.delStream(0,true);\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['yes']}</button>
            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['no']}</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal for successful lockdown -->
    <div class=\"modal fade\" id=\"lockedDoneMessage\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"staticBackdropLabel\">{$LANGUAGE['lock_done_dialog']['title']}</h5>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">
            <input type=\"hidden\" id=\"delsid\" name=\"delsid\" value=\"0\">
            <p>{$LANGUAGE['lock_done_dialog']['text']}</p>
            <small>{$LANGUAGE['del_stream_dialog']['renumber_msg']}</small>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" onclick=\"window.location='../'\">{$LANGUAGE['lock_done_dialog']['button']}</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class=\"modal fade\" id=\"validatingMessage\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" data-backdrop=\"static\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-body\">
          <div class=\"d-flex align-items-center\">
            <strong>{$LANGUAGE['validation_text']['processing']}...</strong>
            <div class=\"spinner-border ml-auto\" role=\"status\" aria-hidden=\"true\"></div>
          </div>
          </div>
        </div>
      </div>
    </div>

  <div class=\"row top\">
  <div class=\"col-lg-3 col-md-2 col-sm-1 col-1\">&nbsp;</div>
  <div class=\"col-lg-6 col-md-8 col-sm-10 col-10\">
  <!-- JavaScript warning -->
  <noscript>
   <div class=\"alert alert-danger\"><i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['no_javascript_error']}</div>
  </noscript>

  <!-- Success and error messages -->
  <div id=\"success_msg\" class=\"alert alert-success\"%display_success%><i class=\"fas fa-check\"></i> {$LANGUAGE['settings_update_success']}</div>
  <div id=\"error_msg\" class=\"alert alert-danger\"%display_error%><i class=\"fas fa-times\"></i> {$LANGUAGE['settings_update_errors']['blanket']} %error%</div>

  <!-- Begin form -->
  <form id=\"stream_settings\" method=\"post\" action=\"process.php\" class=\"needs-validation\" novalidate>

  <!-- Hidden fields -->
  <input type=\"hidden\" name=\"action\" value=\"settings\">
  <input type=\"hidden\" id=\"stream_list\" name=\"stream_list\" value=\"\">

  <!-- Basic stream settings -->
  <h1>{$LANGUAGE['server_settings_title']}</h1>

  <!-- Global server URL -->
  <div class=\"form-item\"><label for=\"url\">{$LANGUAGE['server_url']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['server_url_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
    <input type=\"text\" id=\"url\" name=\"url\" value=\"{$settings['url']}\" class=\"form-control field-check\"{$serv_readonly}>
    <div class=\"invalid-feedback\" id=\"url_error\">{$LANGUAGE['validation_text']['url']}</div>
  </div>

  <!-- Global server port -->
  <div class=\"form-item\"><label for=\"port\">{$LANGUAGE['server_port']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['server_port_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
    <input type=\"text\" id=\"port\" name=\"port\" value=\"{$settings['port']}\" class=\"form-control field-check\"{$serv_readonly}>
    <div class=\"invalid-feedback\" id=\"port_error\">{$LANGUAGE['validation_text']['port']}</div>
  </div>

  <!-- All streams on one server checkbox -->
  <div class=\"form-item fac fac-checkbox fac-default\"><span></span>
    <input type=\"checkbox\" class=\"form-check-input\" id=\"all_on_one\" name=\"all_on_one\"";
if ($settings['all_on_one'] == 1) { $html .= " checked"; }
$html .= " onchange=\"setAllOnOne();\"><label for=\"all_on_one\">{$LANGUAGE['all_streams']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['all_streams_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
   </div>

   <!-- Initial stream selection dropdown -->
   <div  class=\"input-group mb-3\">
       <div class=\"input-group-prepend\">
       <label class=\"input-group-text\" for=\"initial_stream\">{$LANGUAGE['initial_stream']}</label>
      </div>
       <select class=\"browser-default custom-select\" id=\"initial_stream\" name=\"initial_stream\">\n";
for ($i=1; $i<=count($settings['streams']); $i++) {
 $html .= "            <option value=\"{$i}\"";
 if ($i == $settings['initial_stream']) {
     $html .= " selected";
 }
 $html .= ">{$i}</option>\n";
}
$html .= "        </select>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['initial_stream_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
    </div>

    <!-- Autoplay checkbox -->
    <div class=\"fac fac-checkbox fac-default\"><span></span>
       <input type=\"checkbox\" id=\"autoplay\" name=\"autoplay\"";
if ($settings['autoplay'] == 1) { $html .= " checked"; }
$html .= ">&nbsp; <label for=\"autoplay\">{$LANGUAGE['autoplay']}</label>
    </div>

    <!-- Streams list -->
    <h1 class=\"headspace\">{$LANGUAGE['streams_head']}</h1>
    <p>{$LANGUAGE['total_streams_blurb']}</p>

    <!-- Write out individual streams -->
    <div id=\"streams\">\n";
/* write out streams */
if (count($settings['streams']) < 1) {
   $settings['streams'][1] = array('name'=>'', 'sid'=>1, 'path' =>'/', 'url'=>$settings['url'] . ':' . $settings['port']);
}
foreach ($settings['streams'] as $i => $s) {
   if ($s['path'] == ',') { $s['path'] = '/'; }
   $html .= "    <div id=\"s{$i}_container\" class=\"container stream\">\n";
   /* delete button */
   $html .= "        <div class=\"delbox\"><button onclick=\"settingsObj.delStream({$i}); return false;\" title=\"{$LANGUAGE['stream_txt']['del_blurb']}\"";
   if (count($settings['streams']) < 2) {
       $html .= " style=\"display: none;\"";
   }
   $html .= "><i class=\"fas fa-trash-alt\"></i></button></div>\n";

   /* header */
   $html .= "        <h2>{$LANGUAGE['stream_txt']['individual']} {$i}</h2>\n";
   /* form */
   $html .= "        <div id=\"s{$i}_form\" class=\"container\">
           <div class=\"form-item\">
               <label for=\"s{$i}_name\">{$LANGUAGE['stream_txt']['name']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['stream_txt']['name_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
               <input id=\"s{$i}_name\" name=\"s{$i}_name\" type=\"text\" class=\"form-control field-check\" value=\"{$s['name']}\">
               <div class=\"invalid-feedback\" id=\"s{$i}_name_error\">{$LANGUAGE['validation_text']['stream_name']}</div>
           </div>
           <div class=\"form-item\">
               <label for=\"s{$i}_sid\">{$LANGUAGE['stream_txt']['sid']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['stream_txt']['sid_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
               <input id=\"s{$i}_sid\" name=\"s{$i}_sid\" type=\"number\" min=\"1\" class=\"form-control field-check\" value=\"{$s['sid']}\">
               <div class=\"invalid-feedback\" id=\"s{$i}_sid_error\">{$LANGUAGE['validation_text']['stream_id']}</div>
           </div>
           <div class=\"form-item\">
               <label for=\"s{$i}_path\">{$LANGUAGE['stream_txt']['path']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['stream_txt']['path_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
               <input id=\"s{$i}_path\" name=\"s{$i}_path\" type=\"text\" class=\"form-control field-check\" value=\"{$s['path']}\">
               <div class=\"invalid-feedback\" id=\"s{$i}_path_error\">{$LANGUAGE['validation_text']['stream_path']}</div>
           </div>
           <div class=\"form-item\">
               <label for=\"s{$i}_url\">{$LANGUAGE['stream_txt']['url']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['stream_txt']['url_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
               <input id=\"s{$i}_url\" name=\"s{$i}_url\" type=\"text\" class=\"form-control field-check indiv-url\"";
   /* disable URL field, if all streams are on a single server */
   if ($settings['all_on_one'] == 1) { $html.= " readonly";}
   $html .= " class=\"form-control\" value=\"{$s['url']}\">
               <div class=\"invalid-feedback\" id=\"s{$i}_url_error\">{$LANGUAGE['validation_text']['stream_url']}</div>
           </div>
       </div>\n";
   /* close div */
   $html .= "    </div>";
}
$html .= "    </div>\n";

    /* add stream button */
$html .= "
    <!-- Add Stream Button -->
        <div id=\"add_stream\"><button class=\"btn btn-secondary\" onclick=\"settingsObj.addStream(); return false;\"><i class=\"far fa-plus-square\"></i> {$LANGUAGE['add_stream']}</button>
        </div>";
        $html .= "      <h1 class=\"headspace\">{$LANGUAGE['interface']}</h1>
               <div class=\"form-item\">
               <div class=\"input-group mb-3\" >
                 <div class=\"input-group-prepend\">
                 <label class=\"input-group-text\" for=\"default_language\">{$LANGUAGE['default_language']}</label>
                </div>

    <!-- Look and Feel -->
    <!-- Languages -->
    <select class=\"browser-default custom-select\" id=\"default_language\" name=\"default_language\">\n";
$langlist = '';
foreach ($front_langs as $key => $value) {
  $html .= "<option value=\"{$key}\"";
  if ($key == $settings['default_language']) {
    $html .= " selected";
  }
  $html .= ">{$value}</option>\n";
  $langlist .= "{$key},";
}
$html .= "          </select>
    </div>
    <input type=\"hidden\" id=\"all_langs\" name=\"all_langs\" value=\"{$langlist}\">
  </div>

  <!-- Initial Theme -->
  <div class=\"form-item\">
    <div  class=\"input-group mb-3\">
        <div class=\"input-group-prepend\">
        <label class=\"input-group-text\" for=\"theme\">{$LANGUAGE['theme']}</label>
       </div>
       <select class=\"browser-default custom-select\" id=\"theme\" name=\"theme\">\n";
asort($LANGUAGE['themes']);
foreach ($LANGUAGE['themes'] as $key => $value) {
 $html .= "<option value=\"{$key}\"";
 if ($key == $settings['theme']) { $html .= " selected"; }
 $html .= ">{$value}</option>";
}
$html .= "          </select>
        </div>
   </div>

   <!-- Check boxes -->
   <div class=\"fac fac-checkbox fac-default form-item\"><span></span>
   <input type=\"checkbox\" class=\"form-check-input\" id=\"user_change_theme\" name=\"user_change_theme\"";
if ($settings['user_change_theme'] == 1) { $html .= " checked"; }
$html .= "><label for=\"user_change_theme\">{$LANGUAGE['user_change_theme']}</label></div>
    <div class=\"fac fac-checkbox fac-default form-item\"><span></span>
    <input type=\"checkbox\" class=\"form-check-input\" id=\"user_set_language\" name=\"user_set_language\"";
if ($settings['user_set_language'] == 1) { $html .= " checked"; }
$html .= "><label for=\"user_set_language\">{$LANGUAGE['user_set_language']}</label></div>
    <div class=\"fac fac-checkbox fac-default form-item\"><span></span>
        <input type=\"checkbox\" class=\"form-check-input\" id=\"adblock_warning\" name=\"adblock_warning\"";
 if ($settings['adblock_warning'] == 1) { $html .= " checked"; }
 $html .= "><label for=\"adblock_warning\">{$LANGUAGE['adblock_warning']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['adblock_warning_blurb']}\"><i class=\"fas fa-question-circle\"></i></a></div>\n";
 /* progressive web apps -- only if interface works of https */
 if (isset($_SERVER['HTTPS'])) {
    $html .= "    <!-- Enable PWA -->
    <div class=\"fac fac-checkbox fac-default form-item\"><span></span><input type=\"checkbox\" id=\"pwa\" name=\"pwa\" class=\"form-check-input\"";
    if ($settings['pwa'] == 1) { $html .= " checked"; }
    $html .= "> <label for=\"pwa\">{$LANGUAGE['pwa']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['pwa_blurb']}\"><i class=\"fas fa-question-circle\"></i></a></div>
    <div class=\"form-item\">
        <label for=\"pwa_title\">{$LANGUAGE['pwa_title']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['pwa_title_blurb']}\"><i class=\"fas fa-question-circle\"></i></a>
        <input id=\"pwa_title\" name=\"pwa_title\" type=\"text\" class=\"form-control field-check\" value=\"{$settings['pwa_title']}\" placeholder=\"{$LANGUAGE['pwa_title_initial']}\">
    </div>
\n";

 }
 /* lock out configuration interface */
 $html .= " <!-- lock out configuration interface -->
    <div class=\"fac fac-checkbox fac-default form-item\"><span></span></span><input type=\"checkbox\" id=\"lockdown\" name=\"lockdown\" class=\"form-check-input\" value=\"true\" onchange=\"settingsObj.toggleLockout();\"> <label for=\"lockdown\">{$LANGUAGE['lockout_title']}</label>&nbsp;<a data-toggle=\"popover\" data-content=\"{$LANGUAGE['lockout_blurb']}\"><i class=\"fas fa-question-circle\"></i></a></div>
    <div id=\"lockout_unlock_code\">
        <p>{$LANGUAGE['wizard']['steps']['13']['text_1']}</p>
        <input type=\"hidden\" id=\"unlock_code\" name=\"unlock_code\" value=\"\">
        <p class=\"text-center\">{$LANGUAGE['wizard']['steps']['13']['unlock_txt']}: <span id=\"unlock_code_txt\"></span></p>
        <p class=\"text-center\" id=\"unlock_code_link\"></p>
        <p>&nbsp;</p>
    </div>\n";

 /* password reset */
 $html .= "
    <!-- Password Reset -->
    <div id=\"pwd_fields\">
    <h1 class=\"headspace\">{$LANGUAGE['pwd_change']}</h1>";
 if (!isset($_SERVER['HTTPS'])) {
    $html .= "<div class=\"alert alert-warning\" role=\"alert\">
    <i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['pwd_warning']}</div>";
 }

 $html .= "
    <!-- Old Password -->
    <div class=\"form-item\">
        <label for=\"oldpwd\">{$LANGUAGE['pwd_old']}</label>
        <input type=\"password\" id=\"oldpwd\" name=\"oldpwd\" value=\"\" class=\"form-control\">
    </div>

    <!-- New Password -->
    <div class=\"form-item\"><label for=\"newpwdinit\">{$LANGUAGE['pwd_new']}</label>
          <input type=\"password\" id=\"newpwdinit\" name=\"newpwdinit\" value=\"\" class=\"form-control field-check\" aria-describedby=\"passwordHelpBlock\">
          <small id=\"passwordHelpBlock\" class=\"form-text text-muted\">{$LANGUAGE['pwd_new_blurb']}</small>
          <div class=\"invalid-feedback\" id=\"password_error_incorrect\">{$LANGUAGE['validation_text']['pwd_missing_chars']}
          </div>
    </div>

    <!-- Confirm new password -->
    <div class=\"form-item\"><label for=\"newpwdconf\">{$LANGUAGE['pwd_conf']}</label>
        <input type=\"password\" id=\"newpwdconf\" name=\"newpwdconf\" value=\"\" class=\"form-control field-check\">
        <div class=\"invalid-feedback\" id=\"password_error_nomatch\">{$LANGUAGE['validation_text']['pwd_nomatch']}</div>
    </div>
    </div>

    <!-- Submit and reset buttons -->
    <div id=\"buttons\">
        <button type=\"button\" class=\"btn btn-light\" onclick=\"window.location = 'index.php';\"><i class=\"fas fa-redo\"></i> {$LANGUAGE['reset']}</button>&nbsp;
        <button type=\"button\" class=\"btn btn-success\" onclick=\"settingsObj.checkSettingsForm(); return false;\"><i class=\"far fa-save\"></i> {$LANGUAGE['save']}</button>
    </div>


  </form>
  <!-- End form -->
  </div>
  <div class=\"col-lg-3 col-md-2 col-sm-1 col-1\">&nbsp;</div>
  </div>
";

/* include footer */
$stick_footer_to_bottom = TRUE;
require("footer.php");

/* replace success and error strings */
if (isset($_POST['success']) || isset($_GET['success'])) {
    $html = str_replace('%display_success%',' style="display: block;"', $html);
} else {
    $html = str_replace('%display_success%','', $html);
}
if (isset($_POST['error']) || isset($_GET['error'])) {
    if (isset($_POST['error'])) {
        $err = $_POST['error'];
    } elseif (isset($_GET['error'])) {
        $err = $_GET['error'];
    }
    $html = str_replace('%display_error%',' style="display: block;"', $html);
    $html = str_replace('%error%', $LANGUAGE['settings_update_errors'][$err], $html);

} else {
    $html = str_replace('%display_error%','', $html);
}


print($html);

?>
