<?php
/**
 * SolaShout Player
 * Setup Wizard
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

 /* force everything to run through index.php */
 if (strpos($_SERVER['REQUEST_URI'],basename(__FILE__)) !== FALSE) {
  header("Location: ../streamconfig/");
}

/* do not allow wizard to run unless settings.php does not exist or is invalid or user
 * is logged in */
if (validateSettings()) {
    if (!isset($_SESSION['active']) || !isset($_SESSION['validated']) || $_SESSION['active'] != $_SESSION['validated']) {
        header('Location: ../streamconfig');
    }
}

/* create spsess cookie if it doesn't exist */
if (!isset($_COOKIE['spsess'])) {
    setcookie('spsess', randomId(32));
}
if (!isset($_COOKIE['spwiz'])) {
    setcookie('spwiz', randomId(32));
}

/* Make it clear we're running the wizard */
$pg = 'wizard';

/* create new unlock code before deployment */
$ulc = randomId(15);

/* random e-mail addresses to use as placeholders */
$rnd_emails = array('taran@prydain.net', 'jpicard@ncc-1701-d.ship', 'bruce@wayneenterprises.com', 'frtim@kavanaugh.name', 'kenner.olivier@borsovlawn.biz', 'streams@myradio.info', 'kumquat@university.edu', 'heather@cloudmountain.org', 'alan.bradley@encom.com', 'ivanova@babylon5.net');
$email_placeholder = $rnd_emails[rand(0,9)];

/* next step */
$n = '1';

/* create string from front-end langauges for wizard step 8 */
$langlist = '';
$front_langs_markup = '';
foreach ($front_langs as $key => $value) {
  $front_langs_markup .= "    <div class=\"card-text fac fac-radio fac-primary fac-nowrap fac-indent\"><span></span>
        <input type=\"radio\" name=\"default_language\" id=\"default_language_{$key}\" value=\"{$key}\"";
  if ($key == $setlang) { $front_langs_markup .= " checked"; }
  $front_langs_markup .=">
        <label class=\"form-check-label\" for=\"default_language_{$key}\">{$value}</label>
    </div>\n";
  $langlist .= "{$key},";
}

/* create string of themes for wizard step 9 */
$themes_list_markup = '';
asort($LANGUAGE['themes']);
foreach ($LANGUAGE['themes'] as $key => $value) {
    $themes_list_markup .= "    <div class=\"card-text fac fac-radio fac-primary fac-nowrap fac-indent\"><span></span>
            <input type=\"radio\" name=\"theme\" id=\"theme_{$key}\" value=\"{$key}\"";
    if ($key == 'blue') { $themes_list_markup .= " checked"; }
    $themes_list_markup .=" onclick=\"wizard.setThumb('{$key}');\">
            <label class=\"form-check-label\" for=\"theme_{$key}\" onclick=\"wizard.setThumb('{$key}');\">{$value}</label>
        </div>\n";
}

/* instantiate proper server url */
$srv_url = 'http';
if (isset($_SERVER['HTTPS'])) { $srv_url .= 's'; }
$srv_url .= '://' . $_SERVER['HTTP_HOST'];

/* instantiate proper REQUEST URI */
$srv_uri = $_SERVER['REQUEST_URI'];
if (strpos($srv_uri, '?') !== FALSE) {
    $srv_uri = substr($srv_uri, 0, strpos($srv_uri, '?'));
}

/* Array containing content for steps */
require('./wizard_steps.php');

/* header */
$html = "<!DOCTYPE html>
<html>
<head>
<html lang=\"en\">

<head>
  <meta charset=\"utf-8\">
  <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
  <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
  <meta http-equiv=\"expires\" content=\"0\" />
  <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
  <meta http-equiv=\"pragma\" content=\"no-cache\" />
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
  <title>SolaShout Player Settings</title>
  <!-- Font Awesome -->
  <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
  <!-- Bootstrap core CSS -->
  <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
  <!-- FontAwesome Checkbox CSS | © 2014 maxweldsouza. Used by Permission -->
  <link href=\"./css/fac.css\" rel=\"stylesheet\">
  <!-- Your custom styles (optional) -->
  <link href=\"./css/wizard.css?v=" . time() ."\" rel=\"stylesheet\">
  <!-- Favicon -->
  <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
</head>
<body>
    <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
        <a class=\"navbar-brand\" href=\"../streamconfig/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"SolaShout Player Settings\">&nbsp;SolaShout Player Setup Wizard</a>
    </nav>
";

/* screen resolution warning */
$html .= "<div id=\"low_res\" class=\"container text-center\">
  <div class=\"alert alert-danger\">
    <i class=\"fas fa-times\"></i> {$LANGUAGE['wizard']['screen_too_small']}
  </div>
</div>";

/* wizard base */
$html .= "
    <!-- Modal for delete confirmation -->
    <div class=\"modal fade\" id=\"stream_del\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\" id=\"staticBackdropLabel\">{$LANGUAGE['del_stream_dialog']['title']}</h5>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">
            <input type=\"hidden\" id=\"delsid\" name=\"delsid\" value=\"0\">
            <p>{$LANGUAGE['del_stream_dialog']['confirm_msg']}</p>
          </div>
          <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-primary\" onclick=\"wizard.delStream(-1,true);\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['yes']}</button>
            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['no']}</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal for continuation confirmation if not all data entered -->
    <div class=\"modal fade\" id=\"continue_empty\" data-backdrop=\"static\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"staticBackdropLabel\" aria-hidden=\"true\">
      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\">{$LANGUAGE['wizard']['dialogs']['empty_stream_title']}</h5>
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">
            <p>{$LANGUAGE['wizard']['dialogs']['empty_stream_confirm_msg']}</p>
          </div>
          <div class=\"modal-footer\">
          <button type=\"button\" class=\"btn btn-primary\" onclick=\"wizard.continueToStepSix();\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['yes']}</button>
            <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">{$LANGUAGE['del_stream_dialog']['no']}</button>
          </div>
        </div>
      </div>
    </div>

    <form id=\"wizard_form\" action=\"proc-wizard.php\" method=\"post\" onsubmit=\"return false;\">
    <div class=\"row wiz-row\">
        <div class=\"col-sm-2 col-0\">&nbsp;</div>
        <div class=\"col-sm-8 col-12\" style=\"display: flex; flex-direction: column;\">
            <div class=\"card flex-grow-1\">
            <input type=\"hidden\" name=\"all_langs\" value=\"{$langlist}\">\n";

/* write out steps */
foreach($steps as $s => $c) {
    $a = '';
    if ($s == $n) { $a = ' active'; }
    $html .= "                <!-- Step {$s} :: {$c['title']} -->
                    <div id=\"step_{$s}_title\" class=\"card-header noscroll step{$a}\" style=\"flex-shrink: 0;\">
                        {$c['title']}
                    </div>
                    <div id=\"step_{$s}_body\" class=\"card-body step{$a}\" style=\"overflow-y: auto;\">";
        if ($s == '1' && validateSettings(TRUE) == 'corrupt') {
            $html .= "                        <div class=\"alert alert-danger\" role=\"alert\">
                                                <i class=\"fas fa-exclamation-triangle\"></i>";
            $html .= str_replace('[', "<a href=\"{$srv_url}/docs/\">", str_replace(']', '</a>', $LANGUAGE['wizard']['dialogs']['bad_settings_msg']));
            $html .= "                        </div>\n";
        }

    $html .= "                        {$c['markup']}
                    </div>\n";
}

/* wizard end */
$html .= "                <div class=\"card-footer noscroll text-right\" style=\"flex-shrink: 0;\">
                    <!-- <div id=\"current_step\">1 / 15</div> -->
                    <button id=\"back_btn\" class=\"btn btn-outline-primary\" onclick=\"wizard.doStep('prev'); return false;\"><i class=
                    \"fas fa-chevron-circle-left\"></i>&nbsp;{$LANGUAGE['wizard']['back_btn']}</button>&nbsp;&nbsp;&nbsp;
                    <button id=\"next_btn\" class=\"btn btn-primary\" onclick=\"wizard.doStep('next'); return false;\" disabled>
                        <span id=\"next_btn_text\">{$LANGUAGE['wizard']['next_btn']}&nbsp;<i class=\"fas fa-chevron-circle-right\"></i></span>
                        <span id=\"next_btn_spinner\">
                          <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                         </span>
                    </button>
                    <button id=\"finish_btn\" class=\"btn btn-success\" onclick=\"wizard.doStep('next'); return false;\"><span id=\"finish_btn_txt\"><i class=\"fas fa-check\"></i>&nbsp;{$LANGUAGE['wizard']['finish_btn']}</span>
                        <span id=\"finish_btn_spinner\">
                            <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                        </span>
                    </button>
                    <button id=\"close_btn\" class=\"btn btn-success\" onclick=\"wizard.close(); return false;\"></i>&nbsp;{$LANGUAGE['wizard']['close_btn']}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class=\"col-sm-2 col-0\">&nbsp;</div>
    </div>
    </form>\n";

/* include footer */
$stick_footer_to_bottom = TRUE;
$jsfile = 'wizard.js';
require("footer.php");

print($html);

?>
