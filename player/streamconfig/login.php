<?php
/**
 * SolaShout Player
 * Login Form
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* force everything to run through index.php */
if (strpos($_SERVER['REQUEST_URI'],basename(__FILE__)) !== FALSE) {
    header("Location: ../streamconfig/");
}

/* set password strings depending on unlock state */
$pwd_title = $LANGUAGE['login']['pwd_title'];
$pwd_prompt = $LANGUAGE['login']['pwd_prompt'];
$pwd_error = $LANGUAGE['login']['pwd_error'];
$unlock = FALSE;

if (isset($_GET['unlocked'])) {
    $pwd_title = $LANGUAGE['login']['unlock_code'];
    $pwd_prompt = $LANGUAGE['login']['unlock_prompt'];
    $pwd_error = $LANGUAGE['login']['unlock_error'];
    $unlock = TRUE;
}

$html = "<!DOCTYPE html>
<html lang=\"{$setlang}\">
<head>
  <meta charset=\"utf-8\">
  <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
  <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
  <meta http-equiv=\"expires\" content=\"0\" />
  <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
  <meta http-equiv=\"pragma\" content=\"no-cache\" />
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
  <title>{$LANGUAGE['title']}</title>
  <!-- Font Awesome -->
  <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
  <!-- Bootstrap core CSS -->
  <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
  <!-- FontAwesome Checkbox CSS | © 2014 maxweldsouza. Used by Permission -->
  <link href=\"./css/fac.css\" rel=\"stylesheet\">
  <!-- Your custom styles (optional) -->
  <link href=\"./css/login.css?v=" . time() . "\" rel=\"stylesheet\">
  <!-- Favicon -->
  <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
</head>
<body>
    <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
        <a class=\"navbar-brand\" href=\"../streamconfig/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"{$LANGUAGE['title']}\">&nbsp;{$LANGUAGE['title']}</a>
        <div class=\"navbar-nav mr-auto\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"../\">{$LANGUAGE['nav_player']}</a>
                </li>
                <li class=\"nav-item dropdown\">
                <a class=\"nav-link dropdown-toggle waves-effect waves-light\" id=\"navbarDropdownMenuLink\"
                    data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{$LANGUAGE['nav_language']}</a>
                <div class=\"dropdown-menu dropdown-info\" aria-labelledby=\"navbarDropdownMenuLink\">\n";
foreach($back_langs as $l => $n) {
$html .= "                            <a class=\"dropdown-item waves-effect waves-light\" href=\"index.php?lang={$l}\">{$n}</a>\n";
}
$html .="                        </div>
            </li>
        </div>
    </nav>
    <div class=\"row\" style=\"margin-top: 40px;\">
        <div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>
        <div class=\"col-lg-4 col-md-6 col-sm-8 col-10\">
        <div id=\"login_container\" class=\"card\">
            <div id=\"login_form\" class=\"card-body\">
                <h5 class=\"card-title text-center\">{$LANGUAGE['login']['title']}</h5>
                <!-- JavaScript warning -->
                <noscript>
                 <div class=\"alert alert-danger\"><i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['no_javascript_error']}</div>
                </noscript>
";

if (!isset($_SERVER['HTTPS'])) {
    $html .= "
            <div class=\"alert alert-warning\" role=\"alert\">
    <i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['pwd_warning']}</div>";
}

$html .="
                <!-- Success and error messages -->
                <div id=\"success_msg\" class=\"alert alert-success\"%display_success%><i class=\"fas fa-check\"></i> {$LANGUAGE['login']['logout_success']}</div>
                <div id=\"error_msg\" class=\"alert alert-danger\"%display_error%><i class=\"fas fa-times\"></i> {$LANGUAGE['login']['login_error']} </div>

                <!-- login form -->
                <form id=\"login\" name=\"login\">
                <div class=\"form-group\">
                  <label for=\"identity\">{$LANGUAGE['login']['email_title']}</label>
                  <input type=\"email\" class=\"form-control field-check\" id=\"identity\" name=\"identity\" aria-describedby=\"emailHelp\" placeholder=\"{$LANGUAGE['login']['email_prompt']}\" disabled>
                  <small id=\"emailHelp\" class=\"form-text text-muted\">{$LANGUAGE['login']['email_blurb']}</small>
                  <div class=\"invalid-feedback\" id=\"identity_error_incorrect\">{$LANGUAGE['login']['email_error']}</div>
                </div>
                <div class=\"form-group\">
                  <label for=\"challenge\">{$pwd_title}</label>
                  <input type=\"password\" class=\"form-control field-check\" id=\"challenge\" name=\"challenge\" placeholder=\"{$pwd_prompt}\" disabled>
                  <div class=\"invalid-feedback\" id=\"challenge_incorrect\">{$pwd_error}</div>
                </div>\n";
if (!$unlock) {
    /* only display password reset link if we're not in unlock mode */
    $html .= "                <p class=\"card-text\"><a href=\"#\" onclick=\"loginObj.forgotForm('show');\">{$LANGUAGE['login']['forgot']['link']}</a></p>\n";
}
$html.="                <p class=\"card-text text-right\">
                    <button type=\"button\" class=\"btn btn-primary\" id=\"do_login\" onclick=\"loginObj.processLogin(); return false;\" disabled>
                        <span id=\"do_login_txt\">{$LANGUAGE['login']['login_btn']} <i class=\"fas fa-sign-in-alt\"></i></span>
                        <span id=\"do_login_spinner\">
                          <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                        </span>
                    </button>
                </p>
                </form>
            </div>

        <!-- Reset Password Form -->
        <div id=\"pwd_reset_form\" class=\"card-body\">
        <h5 class=\"card-title text-center\">{$LANGUAGE['login']['forgot']['title']}</h5>
";

if (!isset($_SERVER['HTTPS'])) {
    $html .= "
            <div class=\"alert alert-warning\" role=\"alert\">
    <i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['pwd_warning']}</div>";
}

$html .="
        <!-- Success and error messages -->
        <div id=\"reset_success_msg\" class=\"alert alert-success\"><i class=\"fas fa-check\"></i> {$LANGUAGE['login']['forgot']['success']}</div>
        <div id=\"reset_error_msg\" class=\"alert alert-danger\"><i class=\"fas fa-times\"></i> {$LANGUAGE['login']['forgot']['fail']} </div>

            <p class=\"card-title\">{$LANGUAGE['login']['forgot']['blurb']}</p>
            <form id=\"reset\" name=\"reset\">
            <div class=\"form-group\">
              <label for=\"reset_identity\">{$LANGUAGE['login']['email_title']}</label>
              <input type=\"email\" class=\"form-control field-check\" id=\"reset_identity\" name=\"reset_identity\" aria-describedby=\"emailHelp\" placeholder=\"{$LANGUAGE['login']['email_prompt']}\">
              <div class=\"invalid-feedback\" id=\"reset_identity_error_incorrect\">{$LANGUAGE['login']['email_error']}</div>
            </div>
            <div id=\"pwd_reset_cancel\" class=\"card-text\"><button class=\"btn btn-outline-primary\" onclick=\"loginObj.forgotForm('hide'); return false;\">{$LANGUAGE['login']['forgot']['cancel']}</button></div>
            <p class=\"card-text text-right\">
                <button type=\"button\" class=\"btn btn-primary\" id=\"do_reset\" onclick=\"loginObj.processReset(); return false;\">
                    <span id=\"do_reset_txt\">{$LANGUAGE['login']['forgot']['reset_btn']}</span>
                    <span id=\"do_reset_spinner\">
                      <span class=\"spinner-border spinner-border-sm\" role=\"status\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Processing...</span>
                    </span>
                </button>
                <button type=\"button\" class=\"btn btn-success\" id=\"return_from_reset\" onclick=\"loginObj.forgotForm('hide'); return false;\">{$LANGUAGE['login']['forgot']['return']}</button>
            </p>
            </form>
        </div>
        </div>
        </div>
        <div class=\"col-lg-4 col-md-3 col-sm-2 col-1\">&nbsp;</div>
    </div>
    <footer class=\"container col-12 bottom\">
        <p>{$LANGUAGE['login']['cookies']}</p>
        <p>SolaShout Player {$LANGUAGE['copyright']} v." . VERSION . " &copy; 2020 Hawke AI. {$LANGUAGE['rights']} " . str_replace('[',"<a href=\"{$license_url}\" target=\"blank\">", str_replace(']', '</a>', $LANGUAGE['license'])) . "</p>
    </footer>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type=\"text/javascript\" src=\"../js/jquery-3.4.1.min.js\"></script>
    <!-- JQuery Cookie management -->
    <script type=\"text/javascript\" src=\"js/jquery.cookie.js\"></script>
    <!-- Bootstrap tooltips -->
    <script type=\"text/javascript\" src=\"../js/popper.min.js\"></script>
    <!-- Bootstrap core JavaScript -->
    <script type=\"text/javascript\" src=\"../js/bootstrap.min.js\"></script>
    <!-- login javascript -->
    <script type=\"text/javascript\" src=\"js/login.js?v=" . time() . "\"></script>

</body>
</html>
";

/* replace success and error strings */
if (isset($_POST['logout']) || isset($_GET['logout'])) {
    $html = str_replace('%display_success%',' style="display: block;"', $html);
} else {
    $html = str_replace('%display_success%','', $html);
}
if (isset($_POST['error']) || isset($_GET['error'])) {
    if (isset($_POST['error'])) {
        $err = $_POST['error'];
    } elseif (isset($_GET['error'])) {
        $err = $_GET['error'];
    }
    $html = str_replace('%display_error%',' style="display: block;"', $html);
} else {
    $html = str_replace('%display_error%','', $html);
}

print($html);
