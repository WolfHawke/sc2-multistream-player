<?php
/**
 * SolaShout Player Settings
 * Localization file=> English
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

$LANGUAGE = array(
    'languageCode' => 'en',
    'languageName'=> 'English',
    'languageIsoName'=> 'English',
    'languageDirection' => 'ltr',
    'title' => 'SolaShout Player Settings',
    'logo' => 'logo',
    'log_out' => 'Log Out',
    'nav_docs' => 'Documentation',
    'nav_localize' => 'Localize',
    'nav_localize_player' => 'Player',
    'nav_localize_settings' => 'Back End',
    'nav_localize_documentation' => 'Documentation',
    'nav_settings' => 'Settings',
    'nav_language' => 'Languages',
    'nav_player' => 'Player',
    'nav_wizard' => 'Setup Wizard',
    'nav_wizard_blurb' => 'Rerun setup wizard to reset all settings',
    'save' => 'Save Settings',
    'server_settings_title' => 'Server Settings',
    'server_url' => 'Server URL',
    'server_url_blurb' => 'The URL or IP address of the the Shoutcast v2 server on which  stream is available (e.g. http://sc.myserver.com or https://11.22.33.44). Be sure to include the http or https prefix.',
    'server_port' => 'Server port',
    'server_port_blurb' => 'The port on which the Shoutcast v2 server is listening (e.g. 8000).',
    'all_streams' => 'All streams on one server',
    'all_streams_blurb' => 'Check this box if all of the streams you wish to serve on this player instance are on the server URL above.',
    'initial_stream' => 'Initial Stream',
    'initial_stream_blurb' => 'Designates the stream that should be loaded when the player opens.',
    'autoplay' => 'Automatically start playing when player loads',
    'streams_head' => 'Streams',
    'total_streams_blurb' => 'You can add a maximum of 10 streams.',
    'stream_txt' => array (
        'del_blurb' => 'Remove this stream',
        'individual' => 'Stream',
        'name' => 'Stream Name (Title)',
        'name_blurb' => 'A descriptive name of the stream (e.g. Radio Fun-in-the-sun). The stream name may not include the following characters: &#39; &quot; ; * : &lt; &gt; ? &#92; | +',
        'sid' => 'Stream ID (Number)',
        'sid_blurb' => 'The ID number of the stream (sid) on the Shoutcast server. Set this to 1 if you&rsquo;re only pushing a single stream from your server.',
        'path' => 'Stream Path',
        'path_blurb' => 'The path (mount point) at which you can listen to your stream. If your stream is avalable at http://sc.myserver.com:8000/stream enter /stream in this field. If the stream is the only one on the port, and no path is defined, enter /. The path may not include the following characters: &#39; &quot; ; * : &lt; &gt; ? &#92; | + This is required for the player to work properly.',
        'url' => 'Stream URL',
        'url_blurb' => 'The full URL and port to the stream (e.g. http://sc.myserver.com:8000 or https://11.22.33.44:8000).'
    ),
    'add_stream' => 'Add New Stream',
    'interface' => 'Interface Settings',
    'default_language' => 'Default Language',
    'theme' => 'Default Player Theme',
    'themes' => array(
        'black' => 'Black',
        'blue' => 'Blue',
        'green' => 'Green',
        'red' => 'Red',
        'yellow' => 'Yellow'
    ),
    'user_change_theme' => 'Allow users to change the theme',
    'user_set_language' => 'Allow users to select the language of the player',
    'adblock_warning' => 'Show Adblocker warning',
    'adblock_warning_blurb' => 'Some adblockers can cause the audio stream to not play. This option causes the player to check for an advertisement blocker and display a warning if one is enabled. The warning is benign and disappears after 30 seconds.',
    'pwa' => 'Enable player as Progressive Web App',
    'pwa_blurb' => 'Check this box, if you want the player to be available for installation as a standalone app on mobile devices.',
    'pwa_title' => 'Title for Progressive Web App',
    'pwa_title_blurb' => 'The title to display with the button when using SolaShout Player as a Progressive Web App',
    'pwa_title_initial' => 'My SolaShout Player Instance',
    'lockout_title' => 'Lock out configuration settings',
    'lockout_blurb' => 'Check this box to disable access to this configuration settings form. You can unlock the configuration settings using an unlock code that will be displayed when you check the box.',
    'pwd_change' => 'Change Password',
    'pwd_warning' => 'This site is not secured with HTTPS. The data submitted will be transmitted over the internet as plain text, which anyone can read.',
    'pwd_old' => 'Old password',
    'pwd_new' => 'New password',
    'pwd_new_blurb' => "Password must be at least 8 characters long and contain upper and lower case characters and at least one numeral. Including special characters (except &quot;, &apos; or &bsol;) and/or spaces is highly recommended.",
    'pwd_conf' => 'Confirm new password',
    'reset' => 'Reset Settings',
    'js' => array(
        'del_confirm_title' => 'Delete Stream %?',
        'yes_but' => 'Yes',
        'no_but' => 'No',
        'ok_but' => 'OK',
        'cancel_but' => 'Cancel',
        ),
    'validation_text' => array(
        'url' => 'Please enter a valid server address with https:// or http:// prepended, no :port, / or /subfolder at the end.',
        'port' => 'Please enter a valid port number.',
        'stream_name' => 'Please enter a valid stream name.',
        'stream_id' => 'Please enter a valid stream ID. The number must be 1 or greater.',
        'stream_path' => 'Please enter at least a / in the stream path. The app cannot work without it. Check to see if there are invalid characters in the path.',
        'stream_url' => 'Please enter a proper URL in the form of http://domain.tld:port',
        'pwd_nomatch' => 'Passwords do not match.',
        'pwd_missing_chars' => 'The password does not match the requirements.',
        'not_sc_server' => 'No SHOUTCast v2 server was found at the url:port combination you entered. Please check the address and try again.',
        'invalid_fields' => 'The settings could not be validated. Please check what you entered and try again.',
        'processing' => 'Validating Settings',
    ),
    'settings_update_success' => 'Settings successfully updated.',
    'settings_update_errors' => array(
        'blanket' => 'Settings could not be updated.',
        'bad_pass' => 'The old password you entered was incorrect. Therefore the new password could not be updated.',
        'no_write' => 'Unable to write to the settings file. Please check your file permissions and try again.',
        'server_error' => 'An unspecified error occured.'),
    'copyright' => 'Copyright',
    'rights' => 'All Rights reserved.',
    'license' => 'See [License] for usage details.',
    'del_stream_dialog' => array(
        'title' => 'Delete Stream?',
        'yes' => 'Yes',
        'no' => 'No',
        'confirm_msg' => 'Are you sure you want to delete this stream?|| **This cannot be undone!**',
        'renumber_msg' => 'Note: When you save the settings, the streams will be re-numbered sequentially, so the number of the following streams will likely change.',
    ),
    'lock_done_dialog' => array(
        'title' => 'Settings are now locked down',
        'text' => 'You have successfully locked down the settings for SolaShout Player. You will only be able to access the settings page if you unlock it using the unlock code that was generated for you. Click the button below to go open SolaShout Player and listen to your streams.',
        'button' => 'Go to Player',
    ),
    'no_javascript_error' => 'JavaScript has been disabled in this browser. This page will not work without JavaScript. Please enable it and refresh the page to continue using the stream configuration module.',
    'login' => array(
        'title' => 'Sign in to manage settings',
        'email_title' => 'e-Mail address',
        'email_prompt' => 'Enter e-mail address',
        'email_blurb' => 'This e-mail address was defined when SolaShout Player was first configured.',
        'email_error' => 'The e-mail address entered is incorrect.',
        'pwd_title' => 'Password',
        'pwd_prompt' => 'Password',
        'pwd_error' => 'The password entered is incorrect.',
        'login_btn' => 'Sign In',
        'forgot' => array(
            'link' => 'Forgot Password',
            'title' => 'Forgot your password?',
            'cancel' => 'Cancel',
            'return' => 'Return to Login Screen',
            'reset_btn' => 'Reset Password',
            'blurb' => 'Enter the e-mail address associated with this instance of SolaShout Player to reset your password. You will receive a temporary password at that e-mail address, through which you will be able to reset your password.',
            'success' => 'Your password reset request has succeeded. A temporary password was e-mailed to you. Please check your e-mail for how to continue. Be sure to look in your spam box, as well.',
            'fail' => 'Your password could not be reset, because it was not possible to send you the e-mail containing the temporary password',
        ),
        'cookies' => 'The stream configuration portion of this site requires cookies to function. They are necessary to track your login credentials and drive site localization. You will not be tracked through them.',
        'logout_success' => 'You have been successfully logged out.',
        'login_error' => 'Authentication error.',
        'unlock_code' => 'Enter unlock code to log in for the first time',
        'unlock_prompt' => 'Unlock code',
        'unlock_error' => 'The unlock code entered is incorrect.'
    ),
    'pwd_reset' => array(
        'email_msg' => 'You have requested a password reset for your SolaShout Player instance. Below is a temporary password you can use to access the password reset screen. You will be required to reset your password once more before you can continue configuring SolaShout Player.

Temporary password: %newcode%

The SolaShout Player Team',
        'subject' => 'SolaShout Player Password Reset Request',
        'title' => 'Reset password',
        'reset_blurb' => 'Fill in the fields below to set up your new password.',
        'pwd_old' => 'Temporary password',
        'reset_btn' => 'Set Password',
        'success' => 'Your password was successfully changed. Please log in again using your new password to configure SolaShout Player.',
        'continue' => 'Return to Login Prompt',
        'pwd_nomatch' => 'The temporary password was entered incorrectly.',
    ),
    'wizard' => array(
        'title' => 'SolaShout Player Setup Wizard',
        'next_btn' => 'Next',
        'back_btn' => 'Back',
        'finish_btn' => 'Finish',
        'close_btn' => 'Close Wizard',
        'yes' => 'Yes',
        'no' => 'No',
        'dialogs' => array(
            'empty_stream_title' => 'Stream Information Incomplete',
            'empty_stream_confirm_msg' => 'The information for this stream was not entered completely. Since you have already added at least one stream, you can choose to continue to the next step and not add this stream to the list of ones you want to broadcast. Do you want to continue without adding this stream?',
            'bad_settings_title' => 'Settings file is corrupt',
            'bad_settings_msg' => 'The file at `../config/settings.php` did not check out when tested. You can fix it by running the wizard again or, if you have server access, by checking the file itself. See the [documentation] for more details.',
        ),
        'screen_too_small' => 'The screen resolution on your device is too low to use the SolaShout Player Setup Wizard. Please use a device with a minimum screen resolution 800 x 600 pixels.',
        'steps' => array(
            '1' => array('title' => 'Welcome!',
                          'text' => 'This wizard will help you set up SolaShout Player to play your various streams. Click the Next button to step through the options.',
                           'warning' => 'Please note that **SolaShout Player will *only* work with SHOUTCast v2 servers!** The wizard will verify each server at the URL entered, to determine if it is serving a SHOUTCast v2 stream. Icecast or SHOUTCast v1 streams will not be detected.',
                           'noscript' => 'The SolaShout Player setup wizard requires JavaScript to function. Please enable JavaScript end reload this page to use the wizard.'),
            '2' => array('title' => 'Select Stream Location',
                         'text' => 'Are all of the streams that you wish to host on a single server or are they on multiple servers?',
                         'explanation' => 'If you wish to broadcast only one stream, select the single server option.',
                         'radio_yes' => 'The streams are on a single server',
                         'radio_no' => 'The streams are on multiple servers'),
            '3' => array('title' => 'Configure Stream Server URL',
                         'text' => 'Enter the URL and port of the server that hosts your SHOUTCast v2 streams. The wizard will validate that the server is running SHOUTCast v2 before continuing.',),
            '4' => array('title' => 'Select Streams to Broadcast',
                         'text' => 'SolaShout Player found the following streams at [the URL you entered]. Select up to ten (10) streams to broadcast in SolaShout Player.',
                         'th_sid' => 'ID',
                         'th_name' => 'Stream Name',
                         'th_path' => 'Path',
                         'th_status' => 'Status'),
            '5' => array('title' => 'Configure Streams to Broadcast',
                         'text' => 'Enter the information for the stream that you want to broadcast. The wizard will validate the stream before continuing.',
                         'stream_placeholder' => 'My Stream Name',
                         'path_placeholder' => '/path'),
            '6' => array('title' => 'Select Which Stream to Broadcast First',
                         'text_1' => 'You have defined the following streams.',
                         'text_2' => 'Please select which one you want to be broadcast first when SolaShout Player opens.',),
            '7' => array('title' => 'Set Autoplay Feature',
                         'text' => 'Do you want SolaShout Player to automatically start playing when it loads?',
                         'info' => 'Be aware that privacy settings in some browsers will prevent the autoplay feature from functioning. The user can always start the player manually.'),
            '8' => array('title' => 'Select Default Interface Language',
                      'text_1' => 'Select the language that you want the interface initially to be displayed in:',
                      'text_2' => 'Can users select the interface language?',),
            '9' => array('title' => 'Select Default Theme',
                         'text_1' => 'Select the theme you want the player to use:',
                         'text_2' => 'Can users change the theme?',),
            '10' => array('title' => 'Enable Adblocker Warning',
                         'text_1' => 'Some advertisement blocking (&ldquo;adblocker&rdquo;) extensions can interfere with the playing of audio streams and sometimes block them altogether. Because of this SolaShout Player contains a feature that allows you to warn users to turn off their adblocker. SolaShout Player is specifically designed to not track users and has no native capability to display advertisements, so it is safe to use with the ablocker software. This warning can be seen as intrusive by some users, so you can disable it, if you like.',
                         'text_2' => 'Do you want to enable the adblocker warning in SolaShout Player?',
                         'enable' => 'Enable the adblocker warning',
                         'disable' => 'Disable the adblocker warning',),
            '11' => array('title' => 'Enable Progressive Web App (PWA)',
                         'text_1' => 'Progressive Web Apps (PWA) are web sites that can be installed as apps on mobile devices or via Chromium-based browsers (e.g. Google Chrome, Microsoft Edge, Brave or Opera). This allows your instance of SolaShout Player to look and act like a real mobile app, keeping your internet streams front and center on a user&rsquo;s device. SolaShout Player must be installed with https security to be used as a PWA.',
                         'text_2' => 'Do you want to enable SolaShout Player to be used as a Progressive Web App?',
                         'text_3' => 'Enter a title for SolaShout Player to display a unique string when a user pins the app to their home screen, differentiating your instance of SolaShout Player from all the others.',
                         'warning' => 'Please be aware that for the PWA mode to work best, your SHOUTCast streams should also be served over https. [See the documentation] for more information.'),
            '12' => array('title' => 'Lock Out Configuration Interface',
                          'text_1' => 'To increase the security of your instance of SolaShout Player, you can lock out the configuration interface and documentation pages. This is an especially good practice if you are not going to host SolaShout Player over https. If choose to lock out the configuration interface and need to access it in the future, you will be provided with an unlock code.',
                          'text_2' => 'Do you want to lock out the configuration interface?',),
           '13' => array('title' => 'Lock Out Configuration Interface',
                         'text_1' => 'You have chosen to lock out the configuration interface. Below is the unlock code and a URL to re-enable access the configuration interface. Please save them somewhere safe.',
                         'text_2' => 'If you enter an e-mail address below, you will be able to configure the saved settings when you unlock the configuration. If you choose not to, you will have to set up your streams using the wizard and will lose your current settings. Adding the e-mail address will also increase security of the site, as the site will prompt for this e-mail address after unlocking the configuration interface.',
                         'unlock_txt' => 'Unlock Code',
                         'email' => 'Recovery e-Mail (optional)',
                         'email_blurb' => 'Once the configuration interface is unlocked, the e-mail address you enter is used for two purposes: (1) as a login identifier, (2) as a place to send a password reset code, if you forget your password. *It is stored only on your server* and is not transmitted anywhere. The unlock code above becomes a temporary password to allow login to the configuration interface. You will be required to change it on the first login.',
                         ),
            '14' => array('title' => 'Set Up Login Credentials',
                          'text_1' => 'Define an e-mail address and password to allow login to the configuration interface:',
                          'email_blurb' => 'The e-mail address you enter is used for two purposes: (1) as a login identifier, (2) as a place to send a password reset code, if you forget your password. *It is stored only on your server* and is not transmitted anywhere.',
                          'pwd_new' => 'Password',
                          'pwd_conf' => 'Confirm Password',
                          'email_error' => 'The e-mail address you have entered is not in the form of name@domain.com. Please check it and try again.'),
            '15' => array('title' => 'Settings Summary',
                         'text_1' => 'The wizard is ready to write out the following settings. If you are satisfied with them, click Finish to save them.',
                         'error' => 'There was an error writing the settings to the server. Please check your read-write permissions and try again.',),
            '16' => array('title' => 'SolaShout Player Setup Complete',
                          'text_1' => 'Hurray! The Setup Wizard has completed setting up SolaShout Player.',
                          'text_2' => 'You can log into the Stream Configuration Interface by visiting %url%.',
                          'text_3' => 'Start listening to your streams by clicking the Finish button.'),

        ),
        'js_text' => array(
            'status_up' => 'Up',
            'status_dn' => 'Down',
            'enter_stream_name' => 'Enter a name for the stream',
            'sid_txt' => 'ID',
            'path_txt' => 'Path',
            'url_txt' => 'URL',
            'disabled' => 'Disabled',
            'enabled' => 'Enabled',
            'active' => 'Active',
            'locked' => 'Locked out',
            'sum_server' => array(
                'single' => 'Streams are configured to be broadcast from a single SHOUTCast v2 server located at: %s%',
                'multi' => 'Streams are configured to be broadcast from multiple SHOUTCast v2 servers as defined below.'
            ),
            'sum_streams' => 'The following %s% will be broadcast',
            'sum_stream_txt_sg' => 'stream',
            'sum_stream_txt_pl' => 'streams',
            'sum_init_stream' => 'Initial stream to broadcast',
            'sum_autoplay' => 'Autoplay',
            'sum_def_lang' => 'Default language',
            'sum_usr_lang' => 'User can select language',
            'sum_def_theme' => 'Default theme',
            'sum_usr_theme' => 'User can select theme',
            'sum_adblock' => 'Adblocker warning',
            'sum_pwa' => 'Progressive Web App',
            'sum_pwa_title' => 'Unique Title for Progressive Web App',
            'sum_config' => 'Configuration interface',
            'sum_login' => 'Configuration interface login identity (e-mail)',
            'sum_recovery' => 'Configuration interface unlock identity (e-mail)',
            'sum_recovery_not_set' => 'Not set',
        ),
    ),
    'localize' => array(
        'title' => 'Localize SolaShout Player',
        'screen_too_small' => 'The screen resolution on your device is too low to use the SolaShout Player Localization interface. Please use a device with a minimum screen resolution 1024 x 768 pixels.',
        'perm_error_text' => 'Write permissions for SolaShout Player are not set properly for the localization interface to function. Please check the permissions on the following directories:',
        'perm_error_dir_text' => 'directory must be writeable to manage localizations.',
        'perm_error_docs_ref' => 'Please see the [Documentation] for more details.',
        'add_lang_but' => 'Add Language',
        'view_lang_but' => 'View Language',
        'edit_lang_but' => 'Edit Language',
        'del_lang_but' => 'Delete Language',
        'close_but' => 'Close',
        'select_lang' => 'Select the language you wish to add localization for:',
        'err_msg_create' => '**Error!** Unable to create the localization file. Please check the write permissions on your system and try again.',
        'err_msg_delete' => '**Error!** Unable to delete the localization file. Please check the write permissions on your system and try again.',
        'del_dialog_title' => 'Delete localization file for the %area%?',
        'del_dialog_confirm_msg' => 'Are you sure you want to delete %this% localization file for the the %area%?||**This cannot be undone!**',
        'js' => array(
            'not_translated' => 'Item is not translated',
            'not_saved' => 'Item is translated but not saved',
            'group' => 'Item group',
            'done' => 'Item translation is complete',
        ),
        'title_edit' => 'Edit Language',
        'title_view' => 'View Language',
        'autosave' => 'Autosave',
        'header_strings' => 'Strings to Translate',
        'header_original' => 'Original String',
        'header_xlation_box' => 'Translation',
        'keycombo_title' => 'Keyboard Shortcuts',
        'key_cpytxt_combo' => 'Ctrl + Shift + V',
        'key_cpytxt_blurb' => 'Copy source text to translation field',
        'key_save_combo' => 'Ctrl + Shift + S',
        'key_save_blurb' => 'Save all unsaved strings',
        'key_prev_combo' => 'Ctrl + Shift + ↑',
        'key_prev_blurb' => 'Go to previous string',
        'key_next_combo' => 'Ctrl + Shift + Enter',
        'key_next_blurb' => 'Go to next string',
        'key_xpand_combo' => 'Ctrl + Shift + E',
        'key_xpand_blurb' => 'Expand translation box',
        'key_exit_combo' => 'Ctrl + Shift + Q',
        'key_exit_blurb' => 'Close the translation interface',
        'markdown_blurb' => 'You can use [Github Markdown] to add formatting to the strings you translate and `||` to add a line break.',
        'unsaved' => 'The interface will ask for confirmation if there are any unsaved strings when it is closed.',
    ),
    'languages' => array(
        'ab' => 'Abkhazian',
        'aa' => 'Afar',
        'af' => 'Afrikaans',
        'ak' => 'Akan',
        'sq' => 'Albanian',
        'am' => 'Amharic',
        'ar' => 'Arabic',
        'an' => 'Aragonese',
        'hy' => 'Armenian',
        'as' => 'Assamese',
        'av' => 'Avaric',
        'ae' => 'Avestan',
        'ay' => 'Aymara',
        'az' => 'Azerbaijani',
        'bm' => 'Bambara',
        'ba' => 'Bashkir',
        'eu' => 'Basque',
        'be' => 'Belarusian',
        'bn' => 'Bengali',
        'bh' => 'Bihari languages',
        'bi' => 'Bislama',
        'bs' => 'Bosnian',
        'br' => 'Breton',
        'bg' => 'Bulgarian',
        'my' => 'Burmese',
        'ca' => 'Catalan, Valencian',
        'ch' => 'Chamorro',
        'ce' => 'Chechen',
        'ny' => 'Chichewa, Chewa, Nyanja',
        'zh' => 'Chinese',
        'cv' => 'Chuvash',
        'kw' => 'Cornish',
        'co' => 'Corsican',
        'cr' => 'Cree',
        'hr' => 'Croatian',
        'cs' => 'Czech',
        'da' => 'Danish',
        'dv' => 'Dhivehi',
        'nl' => 'Dutch, Flemish',
        'dz' => 'Dzongkha',
        'en' => 'English',
        'eo' => 'Esperanto',
        'et' => 'Estonian',
        'ee' => 'Ewe',
        'fo' => 'Faroese',
        'fj' => 'Fijian',
        'fi' => 'Finnish',
        'fr' => 'French',
        'ff' => 'Fulah',
        'gl' => 'Galician',
        'ka' => 'Georgian',
        'de' => 'German',
        'el' => 'Greek',
        'gn' => 'Guarani',
        'gu' => 'Gujarati',
        'ht' => 'Haitian Creole',
        'ha' => 'Hausa',
        'he' => 'Hebrew',
        'hz' => 'Herero',
        'hi' => 'Hindi',
        'ho' => 'Hiri Motu',
        'hu' => 'Hungarian',
        'id' => 'Indonesian',
        'ga' => 'Irish',
        'ig' => 'Igbo',
        'ik' => 'Inupiaq',
        'io' => 'Ido',
        'is' => 'Icelandic',
        'it' => 'Italian',
        'iu' => 'Inuktitut',
        'ja' => 'Japanese',
        'jv' => 'Javanese',
        'kl' => 'Greenlandic',
        'kn' => 'Kannada',
        'kr' => 'Kanuri',
        'ks' => 'Kashmiri',
        'kk' => 'Kazakh',
        'km' => 'Central Khmer',
        'ki' => 'Kikuyu',
        'rw' => 'Kinyarwanda',
        'ky' => 'Kyrgyz',
        'kv' => 'Komi',
        'kg' => 'Kongo',
        'ko' => 'Korean',
        'ku' => 'Kurdish',
        'kj' => 'Kuanyama',
        'la' => 'Latin',
        'lb' => 'Luxembourgish',
        'lg' => 'Ganda',
        'li' => 'Limburgan',
        'ln' => 'Lingala',
        'lo' => 'Lao',
        'lt' => 'Lithuanian',
        'lu' => 'Luba-Katanga',
        'lv' => 'Latvian',
        'gv' => 'Manx',
        'mk' => 'Macedonian',
        'mg' => 'Malagasy',
        'ms' => 'Malay',
        'ml' => 'Malayalam',
        'mt' => 'Maltese',
        'mi' => 'Maori',
        'mr' => 'Marathi',
        'mh' => 'Marshallese',
        'mn' => 'Mongolian',
        'na' => 'Nauru',
        'nv' => 'Navajo',
        'nd' => 'North Ndebele',
        'ne' => 'Nepali',
        'ng' => 'Ndonga',
        'nb' => 'Norwegian Bokmål',
        'nn' => 'Norwegian Nynorsk',
        'no' => 'Norwegian',
        'ii' => 'Sichuan Yi',
        'nr' => 'South Ndebele',
        'oc' => 'Occitan',
        'oj' => 'Ojibwa',
        'om' => 'Oromo',
        'or' => 'Oriya',
        'os' => 'Ossetian',
        'pa' => 'Punjabi',
        'pi' => 'Pali',
        'fa' => 'Persian',
        'pl' => 'Polish',
        'ps' => 'Pashto',
        'pt' => 'Portuguese',
        'qu' => 'Quechua',
        'rm' => 'Romansh',
        'rn' => 'Rundi',
        'ro' => 'Romanian, Moldavian, Moldovan',
        'ru' => 'Russian',
        'sa' => 'Sanskrit',
        'sc' => 'Sardinian',
        'sd' => 'Sindhi',
        'se' => 'Northern Sami',
        'sm' => 'Samoan',
        'sg' => 'Sango',
        'sr' => 'Serbian',
        'gd' => 'Gaelic',
        'sn' => 'Shona',
        'si' => 'Sinhalese',
        'sk' => 'Slovak',
        'sl' => 'Slovenian',
        'so' => 'Somali',
        'st' => 'Southern Sotho',
        'es' => 'Spanish',
        'su' => 'Sundanese',
        'sw' => 'Swahili',
        'ss' => 'Swati',
        'sv' => 'Swedish',
        'ta' => 'Tamil',
        'te' => 'Telugu',
        'tg' => 'Tajik',
        'th' => 'Thai',
        'ti' => 'Tigrinya',
        'bo' => 'Tibetan',
        'tk' => 'Turkmen',
        'tl' => 'Tagalog',
        'tn' => 'Tswana',
        'to' => 'Tonga (Tonga Islands)',
        'tr' => 'Turkish',
        'ts' => 'Tsonga',
        'tt' => 'Tatar',
        'tw' => 'Twi',
        'ty' => 'Tahitian',
        'ug' => 'Uyghur',
        'uk' => 'Ukrainian',
        'ur' => 'Urdu',
        'uz' => 'Uzbek',
        've' => 'Venda',
        'vi' => 'Vietnamese',
        'vo' => 'Volapük',
        'wa' => 'Walloon',
        'cy' => 'Welsh',
        'wo' => 'Wolof',
        'fy' => 'Western Frisian',
        'xh' => 'Xhosa',
        'yi' => 'Yiddish',
        'yo' => 'Yoruba',
        'za' => 'Zhuang',
        'zu' => 'Zulu',
    ),
);


?>
