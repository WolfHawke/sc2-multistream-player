<?php
/**
 * SolaShout Player Settings
 * String descriptions in English for localizing documentation interface
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

 $descriptions = array(
     'languageCode' => 'A two-letter language code (e.g. en)',
     'languageName' => 'The name of the language in that language (e.g. en = English)',
     'languageIsoName'=> 'The official ISO name for the language',
     'languageDirection' => 'Direction the language is written: ltr or rtl',
     'updated' => 'Last time the documentation was updated. Should be the same as whatever the original language is.',
     'title' => 'Documentation page title',
     'nav_player' => 'Nav link: to player',
     'nav_contents' => 'Nav category: link to list of contents; populated via JavaScript',
     'nav_language' => 'Nav category: languages dropdown',
     'copyright' => 'Text: Copyright',
     'rights' => 'Text: All Rights reserved.',
     'license' => 'Text: License usage blurb; enclose link to license in [brackets].',
     'current_ver' => 'Text: current version',
     'doc_update' => 'Text: documentation last updated; needs colon and rev. in text',
     'body' => 'Documentation body; Use <a href="https://guides.github.com/features/mastering-markdown/" target="_blank">GitHub Markdown</a> for formatting. Do not use HTML tags!'
);
?>
