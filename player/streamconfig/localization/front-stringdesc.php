<?php
/**
* SolaShout Player Settings
* String descriptions in English for localizing front end interface
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

$descriptions = array(
    'languageCode' => 'A two-letter language code (e.g. en)',
    'languageName' => 'The name of the language in that language (e.g. en = English)',
    'languageIsoName'=> 'The official ISO name for the language',
    'languageDirection' => 'Direction the language is written: ltr or rtl',
    'pageTitle' => 'Application title. Recommend not translating this one.',
    'showPlayedSongs' => 'Button label: Show last 10 songs played',
    'hidePlayedSongs' => 'Button label: Hide last 10 songs played',
    'loading'=> 'Message displayed while loading last 10 songs',
    'play' => 'MouseOver: Play button',
    'pause' => 'MouseOver: Pause button',
    'mute' => 'MouseOver: Mute button',
    'unmute' => 'MouseOver: Un-mute button',
    'volume' => 'MouseOver: Volume slider',
    'themes' => array(
        'description' => 'Labels for list of themes that the user can choose',
        'black' => 'Button: Black theme',
        'blue' => 'Button: Blue theme',
        'green' => 'Button: Green theme',
        'red' => 'Button: Red theme',
        'yellow' => 'Button: Yellow theme'
    ),
    'offline' => 'Message to display when stream is down',
    'showstyles' => 'Description for the button to show the styles flyout',
    'setlang' => 'Description for the button to show set language flyout',
    'blockwarning' => 'Warning to display if an adblocker was detected.',
    'copyright' => 'The word &ldquo;copyright&rdquo; for display in on the front end',
    'stations' => array(
        'description' => 'Labels on station selection buttons',
        'select' => 'Button: Select Stream',
        'prev' => 'Button: Previous Stream',
        'next' => 'Button: Next Stream'
    ),
    'nohistory' => 'Message to display when no history is returned',
);

 ?>
