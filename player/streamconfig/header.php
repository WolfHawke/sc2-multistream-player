<?php
/**
 * SolaShout Player Settings
 * Header (called by edit_settings.php, localize.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 * @since   0.8
 */

$html = "<!DOCTYPE html>
<html lang=\"{$setlang}\" dir=\"{$LANGUAGE['languageDirection']}\">

<head>
  <meta charset=\"UTF-8\">
  <meta name=\"robots\" content=\"noindex, nofollow\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
  <meta http-equiv=\"cache-control\" content=\"no-cache, must-revalidate, post-check=0, pre-check=0\" />
  <meta http-equiv=\"cache-control\" content=\"max-age=0\" />
  <meta http-equiv=\"expires\" content=\"0\" />
  <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
  <meta http-equiv=\"pragma\" content=\"no-cache\" />
  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
  <meta name=\"ROBOTS\" content=\"NOINDEX,NOFOLLOW\">
  <title>{$LANGUAGE['title']}</title>
  <!-- Font Awesome -->
  <link rel=\"stylesheet\" href=\"../fontawesome/css/all.min.css\">
  <!-- Bootstrap core CSS -->
  <link href=\"../css/bootstrap.min.css\" rel=\"stylesheet\">
  <!-- FontAwesome Checkbox CSS | © 2014 maxweldsouza. Used by Permission -->
  <link href=\"./css/fac.css\" rel=\"stylesheet\">
  <!-- Your custom styles (optional) -->
  <link href=\"./css/{$page}.css?v=" . time() . "\" rel=\"stylesheet\">
  <!-- Favicon -->
  <link rel=\"shortcut icon\" href=\"../img/player_icons/blue/favicon.ico\">
</head>

<body>
    <nav class=\"navbar navbar-dark bg-dark navbar-expand-lg sticky-top \">
        <a class=\"navbar-brand\" href=\"../streamconfig/\"><img src=\"../img/player_icons/blue/sp_icon_032.png\" alt=\"{$LANGUAGE['title']}\">&nbsp;{$LANGUAGE['title']}</a>
        <div class=\"logout-md ml-auto\"><button class=\"btn btn-primary\" title=\"{$LANGUAGE['log_out']}\" onclick=\"$(\"#logout_form\").submit(); return false;\">{$LANGUAGE['log_out']} <i class=\"fas fa-sign-out-alt\"></i></button></div>
        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            <ul class=\"navbar-nav mr-auto\">
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"../\">{$LANGUAGE['nav_player']}</a>
                </li>
                <li class=\"nav-item\">
                   <a class=\"nav-link waves-effect waves-light\" href=\"../docs/\">{$LANGUAGE['nav_docs']}</a>
                </li>\n";

/* localization menu dropdown -- display in settings */
if ($page == "settings") {
    $html .= "                <li class=\"nav-item dropdown\">
                    <a class=\"nav-link dropdown-toggle waves-effect waves-light\" id=\"navbarDropdownMenuLink\"
                        data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{$LANGUAGE['nav_localize']}</a>
                    <div class=\"dropdown-menu dropdown-info\" aria-labelledby=\"navbarDropdownMenuLink\">
                        <a class=\"dropdown-item waves-effect waves-light\" href=\"?localize=front\">{$LANGUAGE['nav_localize_player']}</a>
                        <a class=\"dropdown-item waves-effect waves-light\" href=\"?localize=back\">{$LANGUAGE['nav_localize_settings']}</a>
                        <a class=\"dropdown-item waves-effect waves-light\" href=\"?localize=docs\">{$LANGUAGE['nav_localize_documentation']}</a>
                    </div>
                </li>\n";
}

if ($page == 'localize-select' || $page == 'localize-edit') {
    $html .= "                <li class=\"nav-item\">
                       <a class=\"nav-link waves-effect waves-light\" href=\"../streamconfig/\">{$LANGUAGE['nav_settings']}</a>
                    </li>\n";
}

$html .= "
                    <li class=\"nav-item\">
                       <a class=\"nav-link waves-effect waves-light\" href=\"?wizard=true\" title=\"{$LANGUAGE['nav_wizard_blurb']}\">{$LANGUAGE['nav_wizard']}</a>
                    </li>
                    <li class=\"nav-item dropdown\">
                        <a class=\"nav-link dropdown-toggle waves-effect waves-light\" id=\"navbarDropdownMenuLink\"
                            data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">{$LANGUAGE['nav_language']}</a>
                        <div class=\"dropdown-menu dropdown-info\" aria-labelledby=\"navbarDropdownMenuLink\">\n";
foreach($back_langs as $l => $n) {
    $html .= "                            <a class=\"dropdown-item waves-effect waves-light\" href=\"index.php?lang={$l}\">{$n}</a>\n";
}
$html .="                        </div>
                    </li>\n";
if ($page == "settings") {
    $html .="                <li class=\"nav-item active\">
                   <a class=\"nav-link waves-effect waves-light\" href=\"#\" onclick=\"settingsObj.checkSettingsForm();\">{$LANGUAGE['save']}</a>
                </li>\n";
}
$html .="            </ul>
            <div class=\"logout-sm ml-auto\"><button class=\"btn btn-primary\" title=\"{$LANGUAGE['log_out']}\" onclick=\"$(\"#logout_form\").submit(); return false;\">{$LANGUAGE['log_out']} <i class=\"fas fa-sign-out-alt\"></i></button></div>
        </div>
        <div class=\"logout-lg ml-auto\">
        <form id=\"logout_form\" name=\"logout_form\" action=\"process.php\" method=\"post\">
            <input type=\"hidden\" name=\"action\" value=\"dologout\">
            <button class=\"btn btn-primary\" title=\"{$LANGUAGE['log_out']}\">{$LANGUAGE['log_out']} <i class=\"fas fa-sign-out-alt\"></i></button>
        </form>
        </div>

    </nav>\n";

?>
