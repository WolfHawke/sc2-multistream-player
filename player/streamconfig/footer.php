<?php
/**
 * SolaShout Player Settings
 * Header (called by edit_settings.php, localize.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 * @since   0.8
 */

if (!isset($jsfile)) {
    $jsfile = "settings.js";
}

$html .= "  <!-- Footer -->
    <footer class=\"container col-12%sticky_footer%\">
    <p class=\"text-center\"><small>SolaShout Player {$LANGUAGE['copyright']} v." . VERSION . " &copy; 2020 Hawke AI. {$LANGUAGE['rights']} " . str_replace('[',"<a href=\"{$license_url}\" target=\"blank\">", str_replace(']', '</a>', $LANGUAGE['license'])) . "</small></p>
    </footer>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type=\"text/javascript\" src=\"../js/jquery-3.4.1.min.js\"></script>
    <!-- Bootstrap tooltips -->
    <script type=\"text/javascript\" src=\"../js/popper.min.js\"></script>
    <!-- Bootstrap core JavaScript -->
    <script type=\"text/javascript\" src=\"../js/bootstrap.min.js\"></script>
    <!-- JQuery ScrollTo -->
    <script type=\"text/javascript\" src=\"../js/jquery.scrollTo.min.js\"></script>
    <!-- JQuery Cookie management -->
    <script type=\"text/javascript\" src=\"js/jquery.cookie.js\"></script>
    <!-- Page-specific JavaScript -->
    <script type=\"text/javascript\" src=\"js/{$jsfile}\"></script>
    <script type=\"text/javascript\">
      /* javascript language variables */
      window.language = ";
    /* don't expose all the text strings in the wizard */
    if (!isset($pg)) { $pg = ''; }
    switch($pg) {
        case 'wizard':
            $LANGUAGE['js']['wizard'] = $LANGUAGE['wizard']['js_text'];
            break;
        case 'settings':
            $LANGUAGE['js']['streamTxt'] = $LANGUAGE['stream_txt'];
            $LANGUAGE['js']['validationText'] = $LANGUAGE['validation_text'];
            $LANGUAGE['js']['errorText'] = $LANGUAGE['settings_update_errors'];
            break;
        case 'localize-edit':
            $LANGUAGE['js']['localize'] = $LANGUAGE['localize']['js'];
            break;
    }
    $html .= json_encode($LANGUAGE['js']);
    if ($pg == 'localize-edit') {
        $html .= "
    /* localization data */
    window.localizationData = {\"area\":\"{$input['localize']}\", \"target\":\"{$input['target-lang']}\"};\n";
    }
    if (isset($settings['streams'])) {
     $html .= "
      /* number of streams */
      window.streamData = {\"count\":" . count($settings['streams']) . ", \"last\":" . array_key_last($settings['streams']) . "};\n";
      }
      $html .= "
    </script>
</body>
</html>
";

$foot_class = '';
if (isset($stick_footer_to_bottom)) {
    $foot_class = " bottom";
}
$html = str_replace('%sticky_footer%', $foot_class, $html);

?>
