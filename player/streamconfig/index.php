<?php
/**
 * SolaShout Player Settings Index Page
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* make sure write permissions are correct */
require("../permcheck.php");

/* import version information */
require("../version.php");

/* require Parsedown */
require('../Parsedown.php');

/* import functions */
require("./functions.php");

/* import settings validator */
require("../checkset.php");

/* send no-caching headers */
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Connection: close");

/* load available interface languages */
$fl_dir = array_diff(scandir('../localization/'), array ('.','..'));
$bl_dir = array_diff(scandir('./localization/'), array ('.','..'));
$dl_dir = array_diff(scandir('../docs/localization/'), array ('.','..'));
$front_langs = array();
$back_langs = array();
$doc_langs = array();

/* build list of front-end languages */
foreach($fl_dir as $f) {
    include ("../localization/{$f}");
    $front_langs[substr($f,0,strpos($f,'.'))] = $LANGUAGE['languageName'];
}
/* build list of back-end languages */
foreach($bl_dir as $f) {
    if (strpos($f, 'stringdesc') === FALSE && strpos($f, 'localization') === FALSE) {
        include ("./localization/{$f}");
        $back_langs[substr($f,0,strpos($f,'.'))] = $LANGUAGE['languageName'];
    }
}
/* build list of documenatation languages */
foreach ($dl_dir as $f) {
    include("../docs/localization/{$f}");
    $doc_langs[substr($f,0,strpos($f,'.'))] = $LANGUAGE['languageName'];
}

/* load the proper localization */
/* default language is English */
$setlang = "en";
/* include english first, because there may be strings missing in translation */
if (file_exists("./localization/en.php")) {
    require("./localization/en.php");
    $def_lang = $LANGUAGE;
} else {
    print("<p><b>Critical error!</b> Core localization file missing. Please make sure <code>en.php</code> is located in the <code>setconfig/localization/</code> folder. It is required for the configuration interface to work.</p>");
    die();
}
/* get cookie, if it is set */
if (isset($_COOKIE['lang'])) { $setlang = $_COOKIE['lang']; }
/* if we're switching to a new language, set that as primary */
if (isset($_GET['lang'])) { $setlang = $_GET['lang']; }
/* post language to cookie, which should last for three hours from now */
setcookie('lang', $setlang, time() + (180*60));
/* load localized strings page */
if (file_exists("./localization/{$setlang}.php")){
    include("./localization/{$setlang}.php");
    $LANGUAGE = array_replace_recursive($def_lang, $LANGUAGE);
    setlocale(LC_ALL, $setlang);
}
/* execute markdown of localization */
$LANGUAGE = mkdnArray($LANGUAGE);

/* add local language names to language items */
foreach ($front_langs as $fk => $fl) {
    if ($LANGUAGE['languages'][$fk] != $fl) {
        $front_langs[$fk] = "{$fl} ({$LANGUAGE['languages'][$fk]})";
    }
}
foreach ($back_langs as $fk => $fl) {
    if ($LANGUAGE['languages'][$fk] != $fl) {
        $back_langs[$fk] = "{$fl} ({$LANGUAGE['languages'][$fk]})";
    }
}
foreach ($doc_langs as $fk => $fl) {
    if ($LANGUAGE['languages'][$fk] != $fl) {
        $doc_langs[$fk] = "{$fl} ({$LANGUAGE['languages'][$fk]})";
    }
}

/* if we're locked down, do not allow streamconfig to work,
* unless unlock code is provided */
/* import settings */
if (file_exists('../config/lockdown.txt') && file_exists('../config/settings.php')) {
    if (isset($_GET['unlock-code'])) {
        require('../config/settings.php');
        if (password_verify($_GET['unlock-code'], $settings['access']['challenge'])) {
            if (file_exists("../config/lockdown.txt")){ unlink("../config/lockdown.txt"); }
            /* check if we have a valid e-mail address in the settings file */
            if (validateEmail($settings['access']['identity'])) {
                /* force password reset */
                writeFile('../config/settings.php', resetChallenge($settings, $_GET['unlock-code'], TRUE));
                /* load login screen */
                header("Location: ../streamconfig/?unlocked=true");
            } else {
                /* if no valid e-mail address, run wizard */
                include("./wizard.php");
            }
            die();
        } else {
            header("Location: ../");
        }
    } else {
        header("Location: ../");
    }
} elseif (file_exists('../config/settings.php')) {
    require("../config/settings.php");
}

if (!validateSettings()) {
    /* load setup wizard if settings don't exist */
    session_start();
    include("./wizard.php");
    die();
}

/* validation */
/* initial validation */
if (isset($_GET['key']) && isset($_COOKIE['spsess'])) {
    session_start();
    if ($_SESSION['active'] == $_GET['key']) {
        /* because login was successful, set the logged in state to true */
        $_SESSION['validated'] = $_GET['key'];
        header("Location: ../streamconfig/");
    } else {
        /* login was unsuccessful because of authentication error */
        $_GET['error'] = 'auth_error';
        include("./login.php");
    }
/* Because session ID exists, we should be logged in */
} elseif (isset($_COOKIE['PHPSESSID'])) {
    session_start();
    /* make sure logged in state is set */
    if (isset($_SESSION['active']) && isset($_SESSION['validated']) && ($_SESSION['active'] == $_SESSION['validated'])) {
        if (isset($_GET['localize'])) {
            include('./localize.php');
        } elseif (isset($_GET['wizard'])) {
            include('./wizard.php');
        } else {
            /* force the reset of a password */
            if (isset($settings['access']['force_reset'])) {
                include('./reset_challenge.php');
            } else {
                include('./edit_settings.php');
            }
        }
    } else {
        /* if we're not logged in kill the session */
        if (isset($_SESSION)) { session_destroy(); }
        if (isset($_COOKIE['PHPSESSID'])) { setcookie('PHPSESSID', FALSE, 0, '/'); }
        if (isset($_COOKIE['spsess'])) {setcookie('spsess', FALSE, 0, '/');}
        $_GET['error'] = 'auth_error';
        include('./login.php');
    }
} else {
    /* kill the session */
    if (isset($_SESSION)) { session_destroy(); }
    if (isset($_COOKIE['PHPSESSID'])) { setcookie('PHPSESSID', FALSE, 0, '/'); }
    if (isset($_COOKIE['spsess'])) { setcookie('spsess', FALSE, 0, '/'); }
    include("./login.php");
}

?>
