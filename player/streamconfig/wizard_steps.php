<?php
/**
 * SolaShout Player
 * Setup Wizard Steps
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */
/** @var    array   Array containing the code to generate each step in the wizard
 */
if (!isset($ulc)) { $ulc = "123456789ABCDEF"; }

$steps = array(
     /* welcome */
     '1' => array('title' => $LANGUAGE['wizard']['steps']['1']['title'], 'markup' => "
     <noscript><p class=\"card-text alert alert-danger\"><i class=\"fas fa-exclamation-triangle\"></i> {$LANGUAGE['wizard']['steps']['1']['noscript']}</p></noscript>
     <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['1']['text']}</p>\n<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['1']['warning']}</p>
     <p class=\"card-text\">{$LANGUAGE['login']['cookies']}</p>\n"),
     /* single server or multiple servers */
     '2' => array('title' => $LANGUAGE['wizard']['steps']['2']['title'], 'markup' => "<p class=\"card-text \">{$LANGUAGE['wizard']['steps']['2']['text']}</p>
         <div class=\"card-text fac fac-radio fac-primary fac-nowrap\"><span></span>
             <input type=\"radio\" name=\"all_on_one\" id=\"all_on_one_yes\" value=\"1\" checked>
             <label class=\"form-check-label\" for=\"all_on_one_yes\">{$LANGUAGE['wizard']['steps']['2']['radio_yes']}</label>
         </div>
         <div class=\"card-text fac fac-radio fac-primary fac-nowrap\"><span></span>
             <input type=\"radio\" name=\"all_on_one\" id=\"all_on_one_no\" value=\"0\">
             <label class=\"form-check-label\" for=\"all_on_one_no\">{$LANGUAGE['wizard']['steps']['2']['radio_no']}</label>
         </div>
         <small class=\"text-muted\">{$LANGUAGE['wizard']['steps']['2']['explanation']}</small>\n"),
     /* Configure streams on single server */
     /* URL and port */
     '3' => array('title' => $LANGUAGE['wizard']['steps']['3']['title'], 'markup' => "<p class=\"card-text \">{$LANGUAGE['wizard']['steps']['3']['text']}</p>
         <!-- Not SC v2 Server -->
         <div id=\"step_3_error_msg\" class=\"alert alert-danger error-msg\"><i class=\"fas fa-times\"></i> {$LANGUAGE['validation_text']['not_sc_server']}</div>
         <!-- Global server URL -->
         <div class=\"form-item card-text\"><label for=\"url\">{$LANGUAGE['server_url']}</label>
           <input type=\"text\" id=\"url\" name=\"url\" value=\"\" class=\"form-control field-check\" placeholder=\"http://sc.myserver.com\">
           <small class=\"text-muted\">{$LANGUAGE['server_url_blurb']}</small>
           <div class=\"invalid-feedback\" id=\"url_error\">{$LANGUAGE['validation_text']['url']}</div>
         </div>
         <!-- Global server port -->
         <div class=\"form-item card-text\"><label for=\"port\">{$LANGUAGE['server_port']}</label>&nbsp;
           <input type=\"number\" id=\"port\" name=\"port\" value=\"\" class=\"form-control field-check\" placeholder=\"8000\">
           <small class=\"text-muted\">{$LANGUAGE['server_port_blurb']}</small>
           <div class=\"invalid-feedback\" id=\"port_error\">{$LANGUAGE['validation_text']['port']}</div>
         </div>

     "),
     /* Select streams to broadcast */
     '4' => array('title' => $LANGUAGE['wizard']['steps']['4']['title'], 'markup' => "<p class=\"card-text \">" . str_replace('[', '<span id="step_4_url">', str_replace(']', '</span>', $LANGUAGE['wizard']['steps']['4']['text'])) . "</p>
         <div id=\"s_single_streams\" class=\"flex-grow-1\">
             <div id=\"s_single_streams_loading\">
                 <span class=\"spinner-border text-secondary\" role=\"status\" aria-hidden=\"true\"></span>
                 <span class=\"sr-only\">Loading...</span>
             </div>
             <table id=\"s_single_streams_table\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                 <thead>
                     <tr>
                         <th class=\"checkbox\">&nbsp;</th>
                         <th class=\"sid\">{$LANGUAGE['wizard']['steps']['4']['th_sid']}</th>
                         <th class=\"name\">{$LANGUAGE['wizard']['steps']['4']['th_name']}</th>
                         <th class=\"path\">{$LANGUAGE['wizard']['steps']['4']['th_path']}</th>
                         <th class=\"status\">{$LANGUAGE['wizard']['steps']['4']['th_status']}</th>
                     </tr>
                 </thead>
                 <tbody>
                 </tbody>
             </table>
         </div>\n"),

     /* Configure streams on multiple servers */
     '5' => array('title' => $LANGUAGE['wizard']['steps']['5']['title'], 'markup' => "
     <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['5']['text']}</p>
     <!-- Not SC v2 Server -->
     <div id=\"step_5_error_msg\" class=\"alert alert-danger error-msg\"><i class=\"fas fa-times\"></i> {$LANGUAGE['validation_text']['not_sc_server']}</div>
     <div class=\"form-item\">
         <label for=\"s_multi_url\">{$LANGUAGE['stream_txt']['url']}</label>
         <input id=\"s_multi_url\" type=\"text\" class=\"form-control field-check indiv-url\"class=\"form-control\" value=\"\" placeholder=\"http://sc.myserver.com:8000\">
         <small class=\"text-muted\">{$LANGUAGE['stream_txt']['url_blurb']}</small>
         <div class=\"invalid-feedback\" id=\"s_multi_url_error\">{$LANGUAGE['validation_text']['stream_url']}</div>
     </div>
     <div class=\"form-item\">
         <label for=\"s_multi_name\">{$LANGUAGE['stream_txt']['name']}</label>
         <input id=\"s_multi_name\" name=\"s_multi_name\" type=\"text\" class=\"form-control field-check\" value=\"\" placeholder=\"{$LANGUAGE['wizard']['steps']['5']['stream_placeholder']}\">
         <small class=\"text-muted\">{$LANGUAGE['stream_txt']['name_blurb']}</small>
         <div class=\"invalid-feedback\" id=\"s_multi_name_error\">{$LANGUAGE['validation_text']['stream_name']}</div>
     </div>
     <div class=\"form-item\">
         <label for=\"s_multi_sid\">{$LANGUAGE['stream_txt']['sid']}</label>
         <input id=\"s_multi_sid\" name=\"s_multi_sid\" type=\"number\" min=\"1\" class=\"form-control field-check\" value=\"1\">
         <small class=\"text-muted\">{$LANGUAGE['stream_txt']['sid_blurb']}</small>
         <div class=\"invalid-feedback\" id=\"s_multi_sid_error\">{$LANGUAGE['validation_text']['stream_id']}</div>
     </div>
     <div class=\"form-item\">
         <label for=\"s_multi_path\">{$LANGUAGE['stream_txt']['path']}</label>
         <input id=\"s_multi_path\" name=\"s_multi_path\" type=\"text\" class=\"form-control field-check\" value=\"\" placeholder=\"{$LANGUAGE['wizard']['steps']['5']['path_placeholder']}\">
         <small class=\"text-muted\">{$LANGUAGE['stream_txt']['path_blurb']}</small>
         <div class=\"invalid-feedback\" id=\"s_multi_path_error\">{$LANGUAGE['validation_text']['stream_path']}</div>
     </div>\n"),

     /* Confirm streams and select initial stream */
     '6' => array('title' => $LANGUAGE['wizard']['steps']['6']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['6']['text_1']}</p>
     <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['6']['text_2']}</p>
     <table id=\"selected_streams_list\">
     </table>
     <div id=\"add_stream\">
     <button class=\"btn btn-secondary\" onclick=\"wizard.addStream(); return false;\"><i class=\"far fa-plus-square\"></i> {$LANGUAGE['add_stream']}</button>
     </div>\n"),

     /* Auto-start player */
     '7' => array('title' => $LANGUAGE['wizard']['steps']['7']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['7']['text']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"autoplay_yes\" name=\"autoplay\">
             <label for=\"autoplay_yes\" value=\"1\">{$LANGUAGE['wizard']['yes']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"autoplay_no\" name=\"autoplay\" value=\"0\" checked>
             <label for=\"autoplay_no\">{$LANGUAGE['wizard']['no']}</label>
         </div>
         <div class=\"alert alert-info\" role=\"alert\">
         <i class=\"fas fa-info-circle\"></i>  {$LANGUAGE['wizard']['steps']['7']['info']}</div>\n"),

     /* Select default language for player interface and whether user can select */
     '8' => array('title' => $LANGUAGE['wizard']['steps']['8']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['8']['text_1']}</p>
         {$front_langs_markup}
         <p class=\"card-text\">&nbsp;</p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['8']['text_2']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"user_set_language_yes\" name=\"user_set_language\" value=\"1\" checked>
             <label for=\"user_set_language_yes\">{$LANGUAGE['wizard']['yes']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"user_set_language_no\" name=\"user_set_language\" value=\"0\">
             <label for=\"user_set_language_no\">{$LANGUAGE['wizard']['no']}</label>
         </div>\n"),

     /* Select the default theme and whether user can select */
     '9' => array('title' => $LANGUAGE['wizard']['steps']['9']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['9']['text_1']}</p>
         {$themes_list_markup}
         <p class=\"card-text\">&nbsp;</p>
         <div id=\"theme_thumbnail\"></div>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['9']['text_2']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"user_change_theme_yes\" name=\"user_change_theme\" value=\"1\" checked>
             <label for=\"user_change_theme_yes\">{$LANGUAGE['wizard']['yes']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"user_change_theme_no\" name=\"user_change_theme\" value=\"0\">
             <label for=\"user_change_theme_no\">{$LANGUAGE['wizard']['no']}</label>
         </div>\n"),

     /* Adblock warning */
     '10' => array('title' => $LANGUAGE['wizard']['steps']['10']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['10']['text_1']}</p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['10']['text_2']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"adblock_warning_yes\" name=\"adblock_warning\" value=\"1\" checked>
             <label for=\"adblock_warning_yes\">{$LANGUAGE['wizard']['steps']['10']['enable']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"adblock_warning_no\" name=\"adblock_warning\" value=\"0\">
             <label for=\"adblock_warning_no\">{$LANGUAGE['wizard']['steps']['10']['disable']}</label>
         </div>\n"),


     /* enable progressive web app */
     '11' => array('title' => $LANGUAGE['wizard']['steps']['11']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['11']['text_1']}</p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['11']['text_2']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"pwa_yes\" name=\"pwa\" value=\"1\" onclick=\"wizard.showHidePWATitleBox('show');\">
             <label for=\"pwa_yes\">{$LANGUAGE['wizard']['yes']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"pwa_no\" name=\"pwa\" value=\"0\" checked onclick=\"wizard.showHidePWATitleBox('hide');\">
             <label for=\"pwa_no\">{$LANGUAGE['wizard']['no']}</label>
         </div>
         <div id=\"pwa_title_box\">
            <label for=\"pwa_title\">{$LANGUAGE['wizard']['steps']['11']['text_3']}</label>
           <input type=\"text\" id=\"pwa_title\" name=\"pwa_title\" value=\"\" class=\"form-control field-check\" placeholder=\"{$LANGUAGE['pwa_title_initial']}\">
          </div>
         <div class=\"alert alert-info\" role=\"alert\">
         <i class=\"fas fa-info-circle\"></i> " . str_replace('[', '<a href="../docs/" target="_blank">', str_replace(']', '</a>', $LANGUAGE['wizard']['steps']['11']['warning'])) . "</div>\n"),


     /* Lock down the interface */
     '12' => array('title' => $LANGUAGE['wizard']['steps']['12']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['12']['text_1']}</p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['12']['text_2']}</p>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"lockdown_yes\" name=\"lockdown\" value=\"1\">
             <label for=\"lockdown_yes\">{$LANGUAGE['wizard']['yes']}</label>
         </div>
         <div class=\"fac fac-radio fac-primary fac-nowrap fac-indent\">
             <span></span>
             <input type=\"radio\" id=\"lockdown_no\" name=\"lockdown\" value=\"0\" checked>
             <label for=\"lockdown_no\">{$LANGUAGE['wizard']['no']}</label>
         </div>\n"),

     /* Lockdown confirmation */
     '13' => array('title' => $LANGUAGE['wizard']['steps']['13']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['13']['text_1']}</p>
         <p class=\"card-text text-center\">{$LANGUAGE['wizard']['steps']['13']['unlock_txt']}:</p>
             <p class=\"card-text text-center\"><b>{$ulc}</b>
                <input type=\"hidden\" id=\"challenge_lockdown\" name=\"challenge_lockdown\" value=\"{$ulc}\">
             </p>
         <p class=\"card-text text-center\"><a id=\"reset_url\" href=\"{$srv_url}{$srv_uri}?unlock-code={$ulc}\" onclick=\"return false;\">{$srv_url}{$srv_uri}?unlock-code={$ulc}</a></p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['13']['text_2']}</p>
         <div class=\"form-group\">
           <label for=\"identity_lockdown\">{$LANGUAGE['wizard']['steps']['13']['email']}</label>
           <input type=\"email\" class=\"form-control field-check\" id=\"identity_lockdown\" name=\"identity_lockdown\" aria-describedby=\"emailHelp\" placeholder=\"{$email_placeholder}\">
           <div class=\"invalid-feedback\" id=\"identity_error_incorrect\">{$LANGUAGE['login']['email_error']}</div>
         </div>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['13']['email_blurb']}</p>
         \n"),

     /* Login information */
     '14' => array('title' => $LANGUAGE['wizard']['steps']['14']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['14']['text_1']}</p>
         <div class=\"form-group\">
           <label for=\"identity\">{$LANGUAGE['login']['email_title']}</label>
           <input type=\"email\" class=\"form-control field-check\" id=\"identity\" name=\"identity\" aria-describedby=\"emailHelp\" placeholder=\"{$email_placeholder}\">
           <small id=\"emailHelp\" class=\"form-text text-muted\">{$LANGUAGE['wizard']['steps']['14']['email_blurb']}</small>
           <div class=\"invalid-feedback\" id=\"identity_error_incorrect\">{$LANGUAGE['wizard']['steps']['14']['email_error']}</div>
         </div>
         <div class=\"form-item\"><label for=\"newpwdinit\">{$LANGUAGE['wizard']['steps']['14']['pwd_new']}</label>
               <input type=\"password\" id=\"newpwdinit\" name=\"newpwdinit\" value=\"\" class=\"form-control field-check\" aria-describedby=\"passwordHelpBlock\">
               <small id=\"passwordHelpBlock\" class=\"form-text text-muted\">{$LANGUAGE['pwd_new_blurb']}</small>
               <div class=\"invalid-feedback\" id=\"password_error_incorrect\">{$LANGUAGE['validation_text']['pwd_missing_chars']}
               </div>
         </div>
         <div class=\"form-item\"><label for=\"newpwdconf\">{$LANGUAGE['wizard']['steps']['14']['pwd_conf']}</label>
             <input type=\"password\" id=\"newpwdconf\" name=\"newpwdconf\" value=\"\" class=\"form-control field-check\">
             <div class=\"invalid-feedback\" id=\"password_error_nomatch\">{$LANGUAGE['validation_text']['pwd_nomatch']}</div>
         </div>\n"),


     /* Summary */
     '15' => array('title' => $LANGUAGE['wizard']['steps']['15']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['15']['text_1']}</p>
     <div id=\"step_15_error_msg\" class=\"alert alert-danger error-msg\"><i class=\"fas fa-times\"></i> {$LANGUAGE['wizard']['steps']['15']['error']}</div>
     <div id=\"summary\" class=\"flex-grow-1\">
     </div>\n"),

     /* Completion Message */
     '16' => array('title' => $LANGUAGE['wizard']['steps']['16']['title'], 'markup' => "<p class=\"card-text\">{$LANGUAGE['wizard']['steps']['16']['text_1']}</p>
         <p id=\"step_16_config_txt\" class=\"card-text\">" .str_replace('%url%', "<a href=\"{$srv_url}/streamconfig/\">{$srv_url}/streamconfig/</a>", $LANGUAGE['wizard']['steps']['16']['text_2']) . "</p>
         <p class=\"card-text\">{$LANGUAGE['wizard']['steps']['16']['text_3']}</p>\n"),


 );
