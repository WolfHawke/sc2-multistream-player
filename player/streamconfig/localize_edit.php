<?php
/**
 * SolaShout Player Localization Edit Language Page
 * (called by index.php)
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* loading spinner */
$html .= "\n";

/* page structure */
$html .= "<div class=\"container-fluid vh-100\">
    <div class=\"row mt-4\">
        <div class=\"col-9\">
            <h2>%sec_title%: %loaded_lang%</h2>
        </div>
        <div class=\"col-1 d-flex align-items-center\">
            <div id=\"saving\" class=\"text-primary\">
                <i class=\"far fa-save\"></i>
            </div>
            <div class=\"custom-control custom-switch\">
                <input type=\"checkbox\" class=\"custom-control-input\" id=\"autosave\" checked onclick=\"xlate.autosave();\">
                <label id=\"autosave_label\" class=\"custom-control-label\" for=\"autosave\">{$LANGUAGE['localize']['autosave']}</label>
            </div>
        </div>
        <div class=\"col-2 text-right\">
            <button class=\"btn btn-menu btn-outline-secondary\" title=\"{$LANGUAGE['localize']['key_save_blurb']}\"  onclick=\"xlate.save()\"><i class=\"far fa-save\"></i></button>&nbsp;
            <button class=\"btn btn-menu btn-outline-secondary\" title=\"{$LANGUAGE['localize']['key_exit_blurb']}\" onclick=\"$('#localize_nav').submit();\"><i class=\"fas fa-times\"></i></button>
        </div>
    </div>
    <div id=\"pane_string_list\" class=\"row h-40\">
        <div class=\"d-flex flex-column col-";
$html .= (isset($input['view'])) ? "12" : "9";
$html .= "\">
            <p><strong>{$LANGUAGE['localize']['header_strings']}:</strong></p>
            <div id=\"to_translate\" class=\"border border-dark\">
                <div id=\"strings_loading\">
                    <span class=\"spinner-border text-secondary\" role=\"status\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Loading...</span>
                </div>
            </div>
        </div>\n";

if (!isset($input['view'])) {
    $html .="        <div id=\"hints\" class=\"d-flex flex-column col-3\">
            <p style=\"margin-bottom: 0;\"><strong>{$LANGUAGE['localize']['keycombo_title']}</strong></p>
            <table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\">
                <tbody>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_next_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_next_blurb']}</td>
                    </tr>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_save_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_save_blurb']}</td>
                    </tr>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_cpytxt_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_cpytxt_blurb']}</td>
                    </tr>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_prev_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_prev_blurb']}</td>
                    </tr>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_xpand_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_xpand_blurb']}</td>
                    </tr>
                    <tr>
                        <td>{$LANGUAGE['localize']['key_exit_combo']}</td>
                        <td>{$LANGUAGE['localize']['key_exit_blurb']}</td>
                    </tr>
                </tbody>
            </table>
            <p class=\"extra\">";
$html .= str_replace('[', '<a href="https://guides.github.com/features/mastering-markdown/" target="_blank">', str_replace(']', '</a>', $LANGUAGE['localize']['markdown_blurb']));
$html .= "</p>
            <p class=\"extra\">{$LANGUAGE['localize']['unsaved']}</p>
        </div>\n";
    }
$html .= "   </div>
    <div id=\"pane_translation\" class=\"row h-50\">
        <div class=\"d-flex flex-column col-";
$html .= (isset($input["view"])) ? "12" : "5";
$html .= "\">
            <div class=\"row h-50\">
                <div class=\"col-12\">
                    <p><strong>{$LANGUAGE['localize']['header_original']}:</strong></p>
                    <textarea id=\"translate_original\" class=\"border border-dark";
$html .= isset($input['view']) ? " w-95" : "";
$html .= "\" disabled></textarea>
                </div>
            </div>
            <div class=\"row h-50\">
                <div class=\"col-12\">
                    <div id=\"translate_description\" class=\"small text-secondary\"></div>
                </div>
            </div>
        </div>\n";
if (!isset($input['view'])) {
$html .="        <div class=\"d-flex flex-column col-1 text-right\">
            <div id=\"translate_copy_button\">
                <button class=\"btn btn-menu btn-outline-secondary\" title=\"{$LANGUAGE['localize']['key_cpytxt_blurb']}\" onclick=\"xlate.duplicateOriginalString(); return false;\"><i class=\"fas fa-arrow-circle-right\"></i></button>
            </div>
        </div>
        <div class=\"d-flex flex-column col-6\">
            <div id=\"translate-xpand\">
                <button id=\"translate-xpand-btn\"class=\"btn btn-outline-secondary\" title=\"{$LANGUAGE['localize']['key_xpand_blurb']}\" onclick=\"xlate.expandTranslation(); return false;\">
                    <i class=\"fas fa-expand-arrows-alt\"></i>
                </button>
            </div>
            <p><strong>{$LANGUAGE['localize']['header_xlation_box']}:</strong></p>
            <textarea id=\"translate_to_text\" class=\"border border-dark\" onkeyup=\"xlate.dirty();\" onblur=\"xlate.queueString()\"></textarea>
        </div>\n";
}
$html .= "    </div>
</div>\n";

/* add correct title */
if (isset($input['view'])) {
    $html = str_replace('%sec_title%',$LANGUAGE['localize']['title_view'],$html);
} else {
    $html = str_replace('%sec_title%',$LANGUAGE['localize']['title_edit'],$html);
}

/* add language we're localizing */
switch($input['localize']) {
    case 'back':
        $html = str_replace('%loaded_lang%', $back_langs[$input['target-lang']], $html);
        break;
    case 'docs':
        $html = str_replace('%loaded_lang%', $doc_langs[$input['target-lang']], $html);
        break;
    case 'front':
        $html = str_replace('%loaded_lang%', $front_langs[$input['target-lang']], $html);
        break;
}


?>
