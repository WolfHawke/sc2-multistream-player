<?php
/**
 * SolaShout Player Front Page
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 * @version 0.12
 */
/* load version info */
include("./version.php");

/* require settings validator */
require("./checkset.php");

/* require Parsedown */
require('./Parsedown.php');

/* load settings */
if (file_exists('./config/settings.php')) {
    require('./config/settings.php');
}
/* if there are no settings or the validation fails, run setup wizard */
if (!validateSettings()) {
    if (file_exists('./streamconfig') && !file_exists('./config/lockdown.txt')) {
        header("Location: ./streamconfig/");
    } else {
        $template = @file_get_contents("configbad.html");
        print($template);
        die();
    }
}

/* allow read from anywhere */
header("Access-Control-Allow-Origin: *");
if (isset($_GET['lang'])) {
    $lang = $_GET['lang'];
} else {
    $lang = $settings['default_language'];
}

if ($settings['user_set_language'] == 1) {
    /* get list of languages */
    $d = scandir("localization");
    $langlist = array();
    if (is_array($d)) {
        foreach ($d as $f) {
            if ($f !== "." && $f !== "..") {
                $langlist[] = substr($f,0,2);
            }
        }
    }
}


if (isset($_GET['stream'])) {
    if (!is_numeric($_GET['stream'])) {
         /* iterate through streams to find the path */
         foreach ($settings['streams'] as $key => $stream){
             if (strpos($stream['path'],$_GET['stream']) !== FALSE) {
                 $settings['initial_stream'] = $key;
                 break;
             }
         }
    } else {
        if (array_key_exists($_GET['stream'],$settings['streams'])){
            $settings['initial_stream'] = $_GET['stream'];
        }
    }
}

/* load the proper localization */
/* include english first, because there may be strings missing in translation */
if (file_exists("./localization/en.php")) {
    require("./localization/en.php");
    $def_lang = $LANGUAGE;
} else {
    print("<p><b>Critical error!</b> Core localization file missing. Please make sure <code>en.php</code> is located in the <code>setconfig/localization/</code> folder. It is required for the configuration interface to work.</p>");
    die();
}
require ("localization/{$lang}.php");
$LANGUAGE = array_replace_recursive($def_lang, $LANGUAGE);
setlocale(LC_ALL, $lang);

/* pull in default strings */
foreach ($def_lang as $k => $i) {
    if (!isset($LANGUAGE[$k])) { $LANGUAGE[$k] = $i; }
}
/* clean up unnecessary default language array */
unset($def_lang);


$template = @file_get_contents("layout.html");

/* set language code and direction */
$template = str_replace("%lang_code%", $lang, $template);
if ($LANGUAGE['languageDirection'] != 'ltr') {
    $template = str_replace("%lang_dir%", " dir=\"{$LANGUAGE['languageDirection']}\"", $template);
    $template = str_replace("%body_lang_dir_class%", "rtl", $template);
} else {
    $template = str_replace("%lang_dir%", '', $template);
    $template = str_replace("%body_lang_dir_class%", "ltr", $template);
}



/* set Version */
$template = str_replace("%version%", VERSION, $template);


/* set theme colors */
$template = str_replace("%theme_color%",$settings['theme'],$template);
$template = str_replace("%icon_color%",$settings['theme'],$template);

/* replace language */
$template = str_replace("%page_title%", $LANGUAGE['pageTitle'], $template);
$template = str_replace("%lang%", $lang, $template);
$template = str_replace("%play%", $LANGUAGE['play'], $template);
$template = str_replace("%mute%", $LANGUAGE['mute'], $template);
$template = str_replace("%volume%", $LANGUAGE['volume'], $template);
$template = str_replace("%played_songs%", $LANGUAGE['showPlayedSongs'], $template);
$template = str_replace("%loading%", $LANGUAGE['loading'], $template);
$template = str_replace('%copyright%', $LANGUAGE['copyright'], $template);
foreach ($LANGUAGE['stations'] as $k => $s) {
    $template = str_replace("%{$k}_station%", $s, $template);
}


/* insert settings */
$firstStream = $settings['streams'][$settings['initial_stream']]['url'];
$template = str_replace("%streams%",json_encode($settings['streams']),$template);
$template = str_replace("%baseurl%",$firstStream,$template);
if ($settings['autoplay'] == 1) {
    $template = str_replace('%autoplay%',' autoplay', $template);
} else {
    $template = str_replace('%autoplay%','', $template);
}


/* create station ist */
$tabs = '';
foreach ($settings['streams'] as $id => $s){
    $tab = "<li id=\"s{$id}\" onclick=\"loadStream({$id})\"";
    if ($id == $settings['initial_stream']){
        $template = str_replace("%track_title%",$s['name'],$template);
        $tab .= " class=\"first selected\">";
        if (substr($s['path'],0,1) != "/") { $firstStream .= "/"; }
        $firstStream .= "{$s['path']}";
    } else {
        $tab .=">";
    }
    $tab .= "<i class=\"fas fa-volume-up\">&nbsp;</i>";
    $tab .= "<span class=\"longname\">{$s['name']}</span>";
    // $tab .= "<span class=\"shortname\">{$s['short_name']}</span>";
    $tab .= "</li>";
    $tabs .= $tab;
}
$template = str_replace("%tabs%",$tabs,$template);
$template = str_replace("%first_stream%",$firstStream,$template);
$template = str_replace("%lang_obj%",json_encode($LANGUAGE),$template);

/* theme colors and language buttons are both visible */
$theme_lang = '';

/* set theme colors */
foreach ($LANGUAGE['themes'] as $k => $i) {
    $template = str_replace("%{$k}_theme%", $i, $template);
}
if ($settings['user_change_theme'] == 1) {
    $template = str_replace("%user_change_theme%",'    <div id="stylebutton"><button id="styleonoff" onclick="showStyles(\'#styleonoff\');" class="btn btn-sm btn-outline-danger-dark px-2" title="' . $LANGUAGE['showstyles'] . '"><i class="fas fa-palette"></i></button></div>' . "\n",$template);
} else {
    $template = str_replace("%user_change_theme%",'',$template);
    $theme_lang = ' class="onebutton"';
}

/* load languge list */
if ($settings['user_set_language'] == 1) {
    $template = str_replace("%lang_button%","    <div id=\"langbutton\">
            <button id=\"langlistonoff\" onclick=\"showStyles('#langlistonoff');\" class=\"btn btn-sm btn-outline-danger-dark px-2\" title=\"{$LANGUAGE['setlang']}\">{$lang}</button>
        </div>\n", $template);
    $lhtml = "";
    foreach($langlist as $l) {
        $lhtml .= "         <li><a href=\"?lang={$l}\">{$l}</a></li>\n";
    }
    $template = str_replace("%lang_list%",$lhtml,$template);
} else {
    $template = str_replace("%lang_button%",'',$template);
    $template = str_replace("%lang_list%",'',$template);
    if ($theme_lang == '') {
        $theme_lang = ' class="onebutton"';
    } else {
        $theme_lang = ' class="nobuttons"';
    }
}

/* if we only have a single stream, kill the navigation buttons! */
$stream_button_theme = '';
if (count($settings['streams']) < 2) {
    $stream_button_theme = ' class="singlestream"';
}
$template = str_replace('%no_side_buts%', $stream_button_theme, $template);

/* adblocker warning */
if ($settings['adblock_warning'] == 1) {
    $html = "    <div id=\"blockwarning\" class=\"alert alert-warning\">
            <div id=\"blockwarning_closer\"><button class=\"btn btn-sm btn-white px-2\" onclick=\"clearTimeout(window.blockerWait); $('#blockwarning').slideToggle();\"><i class=\"fas fa-times\"></i></div>
            <p>{$LANGUAGE['blockwarning']}</p>
        </div>\n";
    $js = "  <!-- adblock checker -->
      <script type=\"text/javascript\" src=\"js/ads.js\"></script>\n";
} else {
    $html = '';
    $js = '';
}
$template = str_replace('%block_warning%', $html, $template);
$template = str_replace('%block_checker%', $js, $template);

print($template);

?>
