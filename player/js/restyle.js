/**
 * SolaShout Player Styling JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/*
 * Function: changeStyle
 * Description: applies the selected style to the page
 * Arguments: s = style name
 * Returns: nothing
 */
function changeStyle(s) {
    if (typeof s == "undefined") { s = "black"; }
    var b = $("body");
    var rtl = (b.hasClass("rtl")) ? true : false;
    b.removeClass(b.prop('class'));
    b.addClass(s);
    if (rtl) { b.addClass("rtl"); }
    /* set favicon */
    $("link[rel*='icon']").prop("href","img/player_icons/" + s + "/favicon.ico");
    setStyleSwatches();
}

/*
 * Function: checkForAdblock
 * Description: checks to see if ads.js was added to the page and
 *              displays adblocker warning.
 * Arguments: none
 * Returns: nothing
 */
function checkForAdblock() {
    if (document.getElementById("cQXCWxlWkLKo2hNrOrTs") == null) {
        $("#blockwarning").slideToggle();
        window.blockerWait = setTimeout(function() { $('#blockwarning').slideToggle(); },30000);
    }
}


/*
 *
 * Function: positionLang
 * Description calculates whether or not and where to language selector
 * Arguments: none
 * Retuns: boolean
 */
function positionLang() {
    if ($("#stylebutton").length == 0) {
        $("#langbutton").css("margin-left","-40px");
        $("#langlist").css("margin-left","-40px");
    } else if ($("#radios li").length > 3 && $("#player").css("width") == "320px") {
        $("#langbutton").hide();
    } else if ($("#langbutton").css("display") == "none"){
        $("#langbutton").show();
    }
}

/*
 * Function: setStyleSwatches
 * Description: sets the checkmark in the proper color box
 * Arguments: none
 * Retuns: nothing
 */
function setStyleSwatches() {
    var color = $("body").prop("class");
    /* remove checks */
    $(".btn-style").removeClass("style-checked");
    /* set check */
    console.log("#btn-style-" + color);
    $("#btn-style-" + color).addClass("style-checked");
}

/*
 * Function: showHideLangList
 * Description: function to expand and contract style list
 * Arguments: todo = 'grow' or 'shrink'
 * Returns: nothing
 */
function showHideLangList(todo) {
    if (typeof todo == "undefined") { todo = false; }
    var l = $("#langlist");
    var height = l.height();
    var top = parseInt(l.css("margin-top"));
    var step = 20;

    /* calculate height */
    var totLang = $("#langlist li").length;
    var maxHeight = (totLang * 40) + 40;
    if (maxHeight > 480) { maxHeight = 480; }
    if (!todo) {
        if (l.css("display") == "none") {
            todo= "grow";
            height = 40;
            top = -40;
            l.height(height);
            l.css("display","block");
        } else {
            todo = "shrink";
            height = maxHeight;
            top = maxHeight*(-1);
        }
    }
    switch (todo) {
        case "grow":
            height += step;
            top -= step;
            break;
        case "shrink":
            height -= step;
            top += step;
            break;
    }
    l.height(height);
    l.css("margin-top",top);
    if (height > 40 && height < maxHeight){
        setTimeout(showHideLangList, 20, todo);
    } else if (height <= 40) {
        l.css("display","none");
    }
}

/*
 * Function: showHideStationList
 * Description: function to expand and contract style list
 * Arguments: todo = 'grow' or 'shrink' or 'nothing'
 * Returns: nothing
 */
function showHideStationList(todo) {
    if (typeof todo == "undefined") { todo = false; }
    if (todo == 'nothing') { return false; }
    var l = $("#radios");
    var height = l.height();
    var toHeight = parseInt($("#player").css("height")) - 60;
    var top = parseInt(l.css("margin-top"));
    var step = 20;
    if (!todo){
        if (l.css("display") == 'none') {
            todo = 'grow';
            height = 40;
            top = toHeight - height;
            l.height(height);
            l.show();
        } else {
            todo = 'shrink';
            height = toHeight;
            top = 0;
        }
    }
    switch (todo) {
        case "grow":
            height += step;
            top -= step;
            break;
        case "shrink":
            height -= step;
            top += step;
            break;
    }
    l.height(height);
    l.css("margin-top",top);
    if (height > 40 && height < toHeight){
        setTimeout(showHideStationList, 10, todo);
    } else if (height <= 40) {
        l.hide();
    }
}

/*
 * Function: showHideStyleList
 * Description: function to expand and contract style list
 * Arguments: todo = 'grow' or 'shrink'
 * Returns: nothing
 */
function showHideStyleList(todo) {
    if (typeof todo == "undefined") { todo = false; }
    var l = $("#stylelist");
    var height = l.height();
    var top = parseInt(l.css("margin-top"));
    var step = 20;
    if (!todo){
        if (l.css("display") == 'none') {
            todo = 'grow';
            height = 40;
            top = -40;
            l.height(height);
            l.css("display","block");
        } else {
            todo = 'shrink';
            height = 250;
            top = -250;
        }
    }
    switch (todo) {
        case "grow":
            height += step;
            top -= step;
            break;
        case "shrink":
            height -= step;
            top += step;
            break;
    }
    l.height(height);
    l.css("margin-top",top);
    if (height > 40 && height < 250){
        setTimeout(showHideStyleList, 20, todo);
    } else if (height < 40) {
        l.css("display","none");
    } else {
        setStyleSwatches();
    }
}

/*
 * Function: showStyles
 * Description: button trigger to show styel flyout
 * Arguments: l = layer
 * Returns: nothing
 */
function showStyles(l) {
    if (typeof l == "undefined") { display: none; }
    var s = $(l);
    if (s.hasClass("btn-outline-danger-dark")) {
        s.removeClass("btn-outline-danger-dark px-2");
        s.addClass("btn-danger-dark px-2");
    } else if (s.hasClass("btn-danger-dark")) {
        s.removeClass("btn-danger-dark px-2");
        s.addClass("btn-outline-danger-dark px-2");
    }
    switch (l) {
        case "#langlistonoff":
            showHideLangList();
            break;
        case "#styleonoff":
            showHideStyleList();
            break;
    }
}

$(window).resize(function() {
    positionLang();
});
$(document).ready(function() {
    checkForAdblock();
    positionLang();
});
