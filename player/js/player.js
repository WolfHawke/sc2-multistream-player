/**
 * SolaShout Player JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

/* global items */
window.metaUp = "";
window.showHistory = false;
window.active = 0;

/* Player functions */
/*
 * Function: changeTitle
 * Description: changes text in Title through fading out and in
 * Arguments: t = text; if left as empty string fades in new text
 * Returns: nothing
 */
function changeTitle(t) {
    if (typeof t == "undefined") { return false; }
    clearTimeout(window.aniWait);
    var tObj = $("#trackTitle");
    var opacity = parseFloat(tObj.css("opacity"));
    var wait = 10;
    var goToNext = true;
    if (opacity < 0.1) {
        tObj.html(t);
        document.getElementById("trackTitle").scrollLeft = 0;
        t = "";
    }
    if (t != "") {
        opacity -= 0.1;
    } else if (opacity > 0.9 && t == ""){
        goToNext = false;
    } else {
        opacity += 0.1;
    }
    tObj.css("opacity", opacity);
    if (goToNext) {
        window.aniWait = setTimeout(changeTitle, wait, t);
    } else {
        window.aniWait = setTimeout(walkTitle, 3000, 'forward');
    }
}

/*
 * Function: loadStream
 * Description: loads a new stream to be played
 * Arguments: id = id of stream to load (references window.streams)
 */
function loadStream(id) {
    if (typeof window.streams == "undefined") { return false; }
    if (typeof id == "undefined") { id = 1; }
    window.active = id;
    var i;
    var src = window.streams[id].url;
    var player = document.getElementById('playerObj');
    var playstat = "paused";
    /* reset all tabs */
    for (key in window.streams) {
        $("#s"+key).removeClass('selected');
    }

    /* load tab */
    $("#s"+id).addClass('selected');

    /* stop player and change source */
    if (!player.paused) {
        playPause('playerObj');
        playstat="playing";
    }
    if (window.streams[id].path.indexOf("/") > -1) {
        src += window.streams[id].path;
    } else {
        src += "/" + window.streams[id].path;
    }
    player.src = src;

    $("#trackTitle").html(window.streams[id].name);
    $("#streamSelectActive").html(window.streams[id].name);
    $("#history").html("&nbsp;");
    selectStation("close");

    /* Update content */
    if (playstat != "paused") {
        playPause('playerObj');
    }
    console.log('loaded ' + src);

    /* start walk */
    clearTimeout(window.walkTimeout);
    walkTitle();
}


/*
 * Function: mute
 * Description: mutes the player
 * Arguments: pObj = reference to player Object
 * Returns: nothing
 */
function mute(pObj) {
    if (typeof pObj == "undefined") { return false; }
    var player = document.getElementById(pObj);
    var icon = "<i class=\"fas fa-volume-up\"></i>";
    var muteTxt = window.language.mute;
    if (player.muted) {
        player.muted = false;
    } else {
        player.muted = true;
        icon = "<i class=\"fas fa-volume-mute\"></i>";
        muteTxt = window.language.unmute;
    }
    $("#playMute").html(icon);
    $("#playMute").prop('title',muteTxt);
}

/*
 * Function: playPause
 * Description: plays or pauses the player
 * Arguments: pObj = reference to player Object
 * Returns: nothing
 */
function playPause(pObj) {
    if (typeof pObj == "undefined") { return false; }
    var player = document.getElementById(pObj);
    var icon = "<i class=\"fas fa-play\"></i>";
    var vol = $("#volume").val() * 0.01;
    var playTxt = window.language.play;
    if (player.paused) {
        player.volume = vol;
        player.play();
        icon = "<i class=\"fas fa-pause\"></i>";
        playTxt = window.language.pause;
        updateData();
    } else {
        player.pause();
    }
    $("#playButton").html(icon);
    $("#playButton").prop('title',playTxt);
}

/*
 * Function: selectStation
 * Description: Shows the menu to select the next_station OR moves to the next
 *              station in the list
 * Arguments: dir = 'next' or 'prev'
 * Returns: nothing
 */
function selectStation(dir) {
    if (typeof dir == "undefined") { dir = "select"; }
    var n = 0;
    var s = $("#streamSelectList");
    if (dir == "select" || dir == "close") {
        if (s.hasClass("btn-outline-danger-dark")) {
            s.removeClass("btn-outline-danger-dark px-2");
            s.addClass("btn-danger-dark px-2");
        } else if (s.hasClass("btn-danger-dark")) {
            s.removeClass("btn-danger-dark px-2");
            s.addClass("btn-outline-danger-dark px-2");
        }
        if (($("#radios").css("display") == "none") && (dir == "select")) {
            showHideStationList()
        } else {
            if ($("#radios").css("display") != "none"){ showHideStationList(); }
            $("#streamSelectPrev").prop("disabled",false);
            $("#streamSelectNext").prop("disabled",false);
            if (window.active == 1) {
                $("#streamSelectPrev").prop("disabled",true);
            } else if (window.active == Object.keys(window.streams).length) {
                $("#streamSelectNext").prop("disabled",true);
            }
        }
    } else if (dir == "next") {
        loadStream(window.active + 1);
        selectStation("close");
    } else if (dir == "prev") {
        loadStream(window.active - 1);
        selectStation("close");
    }
}

/*
 * Function: setVolume
 * Description: plays or pauses the player
 * Arguments: pObj = reference to player Object
 * Returns: nothing
 */
 function setVolume(pObj) {
     if (typeof pObj == "undefined") { return false; }
     var player = document.getElementById(pObj);
     var vol = $("#volume").val() * 0.01;
     if (player.muted) { mute(pObj); } /* unmute if muted */
     player.volume = vol;
}

/*
 * Function: showPlayed
 * Description: function to update metadata
 * Arguments: none
 * Returns: nothing
 */
function showPlayed() {
    if ($("#showHistory").val() == "false") {
        $("#showHistory").val("true");
        $("#historyonoff").removeClass("btn-outline-danger-dark px2");
        $("#historyonoff").addClass("btn-danger-dark px2");
        $("#historyonoff").prop("title", window.language.hidePlayedSongs);
        updateData();
        $("#history").slideToggle();
    } else if ($("#showHistory").val() == "true") {
        $("#showHistory").val("false");
        $("#historyonoff").removeClass("btn-danger-dark px2");
        $("#historyonoff").addClass("btn-outline-danger-dark px2");
        $("#historyonoff").prop("title", window.language.showPlayedSongs);
        $("#history").slideToggle();
    }
}

/*
 * Function: updateData
 * Description: function to update metadata
 * Arguments: none
 * Returns: nothing
 */
function updateData() {
    var dataObj = {"stream":document.getElementById("playerObj").src};
    var sArr = document.getElementById('playerObj').src.split('/');
    if ($("#showHistory").val() == "true") {
        dataObj['history'] = "true";
    }
    console.log(dataObj);

    $.ajax({
        url: 'engine.php',
        data: dataObj,
        dataType: "json",
        success: function(r) {
            if (typeof r.error != "undefined") {
                console.log(r.error);
            } else {
                if ($("#trackTitle").html() != r.song) {
                    if (r.status == 'down') {
                        changeTitle(window.language.offline);
                        if (!document.getElementById('playerObj').paused) {
                            playPause('playerObj');
                        }
                    } else if (r.song == ''){
                        changeTitle(streams[window.active].name);
                    } else {
                        changeTitle(r.song);
                    }
                }
                if (typeof r.history != "undefined") {
                    if (r.history.length < 1) {
                        html = "<p>" + window.language.nohistory + "</p>";
                    } else {
                        html = "<ol>\n";
                        for (i=0; i < 10; i++) {
                            if (typeof r.history[i] !="undefined"){
                                html += "<li>" + r.history[i] + "</li>";
                            }
                        }
                        html += "</ol>";
                    }
                    $("#history").html(html);

                }
                window.metaUp = setTimeout(updateData,60000);
            }
        }
    });
}

/*
 * Function: walkTitle
 * Description: walks a long title back and forth
 * Arguments: none
 * Returns: nothing
 */
function walkTitle(direction) {
    if (typeof direction == 'undefined') { direction = 'forward'; }
    var t = document.getElementById("trackTitle");
    var tSize = t.scrollWidth;
    var tVisible = t.clientWidth;
    var tDiff = tSize - tVisible;
    var tFontSize = parseInt($("#trackTitle").css("font-size"));
    var base = 9;
    var interval = 500;
    var left = t.scrollLeft;

    if (tFontSize > 16) {
        base = Math.round(base*(tFontSize/16));
    }

    if (tSize > tVisible) {
        if (direction == 'forward') {
            left += base;
        } else if (direction == 'back') {
            left -= base;
        }
        if (left > tDiff) {
            left = tDiff;
            interval = 3000;
            direction = 'back';
        } else if (left < 0) {
            left = 0;
            interval = 3000;
            direction = 'forward';
        }
        t.scrollLeft = left;
        window.aniWait = setTimeout(walkTitle, interval, direction);
    }
}

/* Hide noscript player */
$(document).ready(function() {
    var p = document.getElementById("playerObj");
    p.controls = false;
    window.active = 1;
    $("#fancyPlayer").show();
    if (p.autoplay) {
        updateData();
        $("#playButton").html("<i class=\"fas fa-pause\"></i>");
    }
    /* safari is too stupid to actually hide the player */
    if (window.navigator.userAgent.indexOf("Safari") > -1) {
      $("#playerObj").css("display", "none");
    }

});
