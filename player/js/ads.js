/**
 * SolaShout Player Adblock Checker
 * JavaScript
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */


/* script to check for advertisement blockers
 * The reason for this is that some adblockers will
 * prevent the audio streams from playing.
 * This way we can display a warning if there an
 * adblocker is active so the user won't be surprised
 * if there are issues.
 */

$(function() {
    var html = "<div id=\"cQXCWxlWkLKo2hNrOrTs\" style=\"display: none\">&nbsp;</div>"
    $("#player").append(html);
});
