<?php
/**
 * SolaShout Player
 * Localization file=> Turkish
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

$LANGUAGE = array(
    'languageCode' => 'tr',
    'languageName' => 'Türkçe',
    'languageIsoName'=> 'Turkish',
    'languageDirection' => 'ltr',
    'pageTitle' => 'SolaShout Player',
    'showPlayedSongs' => 'Son çalınan şarkıları görüntüle',
    'hidePlayedSongs' => 'Son çalınan şarıkları gizle',
    'loading'=> 'Yükleniyor',
    'play' => 'Oynat',
    'pause' => 'Duraklat',
    'mute' => 'Sustur',
    'unmute' => 'Sesi aç',
    'volume' => 'Ses seviyesi',
    'themes' => array(
        'black' => 'Siyah',
        'blue' => 'Mavi',
        'green' => 'Yeşil',
        'red' => 'Kırmızı',
        'yellow' => 'Sarı'
    ),
    'offline' => 'Yayın bağlantısı kesik',
    'showstyles' => 'Biçemi seç',
    'setlang' => 'Arabirim dilini seç',
    'blockwarning' => 'Tarayıcınızda reklamları önleyen bir eklentinin olduğu tespit edildi. Reklam önleyen eklentiler yayın akışını engelleyebilir. Eklentiyi bu sayfada devre dışı bırakırsanız iyi olur. Bu sayfa reklam sunmaz. Üçüncü parti takip yazılmı ve/veya çerez içerebilir. Bu uyarıyı kapatırsanız okuyup kabul ettiğinizi beyan ediyorsunuz.',
    'copyright' => 'Telif Hakları',
    'stations' => array(
        'select' => 'Yayın Seç',
        'prev' => 'Önceki Yayın',
        'next' => 'Sonraki Yayın'
    ),
    'nohistory' => 'Çalınan parça bulunamadı.',
);

 ?>
