<?php
/**
 * SolaShout Player
 * Localization file=> English
 *
 * @author JMW
 * @copyright Hawke AI. All Rights reserved.
 */

$LANGUAGE = array(
    'languageCode' => 'en',
    'languageName' => 'English',
    'languageIsoName'=> 'English',
    'languageDirection' => 'ltr',
    'pageTitle' => 'SolaShout Player',
    'showPlayedSongs' => 'Show last played songs',
    'hidePlayedSongs' => 'Hide last played songs',
    'loading'=> 'Loading',
    'play' => 'Play',
    'pause' => 'Pause',
    'mute' => 'Mute',
    'unmute' => 'Un-mute',
    'volume' => 'Volume',
    'themes' => array(
        'black' => 'Black',
        'blue' => 'Blue',
        'green' => 'Green',
        'red' => 'Red',
        'yellow' => 'Yellow'
    ),
    'offline' => 'Stream is down',
    'showstyles' => 'Select style',
    'setlang' => 'Select interface language',
    'blockwarning' => 'We have detected that you&rsquo;re running an advertisement blocker. Advertisement blockers can interfere with the playing of radio streams, so you may want to disable it. This page will not serve any advertisements, but it may contain third-party tracking scripts. By closing this warning you acknowledge you have read it and accept the risks of the player not working.',
    'copyright' => 'Copyright',
    'stations' => array(
        'select' => 'Select Stream',
        'prev' => 'Previous Stream',
        'next' => 'Next Stream'
    ),
    'nohistory' => 'No previously played songs found.',
);

 ?>
